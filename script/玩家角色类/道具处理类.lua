local 道具处理类 = class()

function 道具处理类:初始化()
	self.数据 = {}
	self.宝图地图 = {1501,1506,1070,1091,1092,1110,1173,1146,1142,1131,1140,1512,1513,1514,1174}
	self.附属性 = {
		"伤害",
		"气血",
		"魔法",
		"灵力"
	}
end

function 道具处理类:加载数据(q, w, e)
	self.数据 = table.loadstring(q)

	for n, v in pairs(self.数据) do
		if self.数据[n] == 0 then
			self.数据[n] = nil
		end

		if self.数据[n] ~= nil and self.数据[n] ~= 0 then
			if self.数据[n].名称 == "神兜兜" and self.数据[n].数量 == nil then
				self.数据[n].数量 = 1
			elseif self.数据[n].名称 == "法宝碎片" and self.数据[n].数量 == nil then
				self.数据[n].数量 = 1
			elseif (self.数据[n].名称 == "蓝色合成旗" or self.数据[n].名称 == "白色合成旗" or self.数据[n].名称 == "绿色合成旗" or self.数据[n].名称 == "黄色合成旗" or self.数据[n].名称 == "红色合成旗") and self.数据[n].地图编号 == nil and self.数据[n].地图 ~= "未定标" then
				for i, w in pairs(地图处理类.地图数据) do
					if 地图处理类.地图数据[i].名称 == self.数据[n].地图 then
						self.数据[n].地图编号 = i
					end
				end
			end
		end
	end
end
function 道具处理类:取随机制造()
	if 取随机数() <= 50 then
		return 制造装备.武器.类型[取随机数(1, 12)]
	else
		return 制造装备.防具[取随机数(1, 7)]
	end
end

function 道具处理类:数据处理(id, 序号, 内容, 参数)
	if 玩家数据[id].战斗 ~= 0 then
		return 0
	end

	if 玩家数据[id].摆摊 ~= nil and 序号 ~= 1 then
		return 0
	end

	if 序号 == 1 then
		self:索要道具(id, 参数)
	elseif 序号 == 2 then
		self:佩戴装备(id, 参数, 内容)
	elseif 序号 == 3 then
		玩家数据[id].道具id = {
			格子 = 参数,
			类型 = 内容
		}
		self.对话内容 = " 丢弃后的道具将被永久删除，请确认你是否要丢弃#R/" .. self.数据[玩家数据[id].角色.数据.道具[内容][参数]].名称 .. "\n\n  " .. "#R/ht|" .. "66" .. [[
/确认丢弃道具
  #R/ht|0/取消


       ]]
		self.发送信息 = {
			名称 = "???",
			对话 = self.对话内容
		}

		发送数据(玩家数据[id].连接id, 20, self.发送信息)
	elseif 序号 == 4 then
		self:出售道具处理(id, 参数, 内容)
	elseif 序号 == 5 then
		self:鉴定道具(id, 参数, 内容)
	elseif 序号 == 6 then
		-- 玩家数据[id].道具:索要打造道具(id, "包裹", "强化打造")
 self.对话内容=[[ 你可以在我这里选择普通打造和强化打造，强化打造的属性范围会高过普通打造，但是消耗的银子会是普通打造的三倍，且还会消耗强化石。等级低于60级的书铁无法进行强化打造，请选择你要打造的方式：

  ]].."#R/ht|普通打造"..[[/普通打造

  ]].."#R/ht|强化打造"..[[/强化打造

  #R/ht|0/取消


       ]]
     self.发送信息={编号=nil,对话=self.对话内容,名称="打造"}
     发送数据(玩家数据[id].连接id,20,self.发送信息)
	elseif 序号 == 7 then
		玩家数据[id].装备:数据处理(id, 4006, 参数, 内容)
	elseif 序号 == 8 then
		self:导标旗定标(id, 参数, 内容)
	elseif 序号 == 9 then
		self:导标旗传送(id, 参数, 内容)
	elseif 序号 == 10 then
		self:鬼谷子处理(id, 参数, 内容)
	elseif 序号 == 11 then
		发送数据(玩家数据[id].连接id, 3009, self:索要道具1(id, 内容))
	elseif 序号 == 12 then
		发送数据(玩家数据[id].连接id, 3010, self:索要仓库道具(id, 参数))
	elseif 序号 == 13 then
		-- Nothing
	elseif 序号 == 14 then
		self:宝石升级处理(id, 参数, 内容)
	elseif 序号 == 15 then
		self:摄妖香处理(id, 参数, 内容)
	elseif 序号 == 16 then
		self:洞冥草处理(id, 参数, 内容)
	elseif 序号 == 17 then
		self:小铲子处理(id, 参数, 内容)
	elseif 序号 == 18 then
		self:月华露处理(id, 参数, 内容)
	elseif 序号 == 19 then
		self:藏宝图处理(id, 参数, 内容)
	elseif 序号 == 20 then
		self:五宝处理(id, 参数, 内容)
	elseif 序号 == 21 then
		self:海马处理(id, 参数, 内容)
	elseif 序号 == 22 then
		self:索要炼妖数据(id)
	elseif 序号 == 23 then
		self:炼妖合宠(id, 参数, 内容)
	elseif 序号 == 24 then
		self:道具合宠(id, 参数, 内容)
	elseif 序号 == 25 then
		self:上古锻造图策处理(id, 参数, 内容)
	elseif 序号 == 26 then
		self:佩戴bb装备处理(id, 参数, 内容)
	elseif 序号 == 27 then
		self:卸下bb装备处理(id, 参数, 内容)
	elseif 序号 == 28 then
		self:炼药处理(id, 参数, 内容)
	elseif 序号 == 29 then
		self:烹饪处理(id, 参数, 内容)
	elseif 序号 == 30 then
		self:使用烹饪处理(id, 参数, 内容)
	elseif 序号 == 31 then
		self:使用药品处理(id, 参数, 内容)
	elseif 序号 == 32 then
		self:召唤兽快捷加血(id)
	elseif 序号 == 33 then
		self:召唤兽快捷加蓝(id)
	elseif 序号 == 34 then
		self:角色快捷加血(id)
	elseif 序号 == 35 then
		self:角色快捷加蓝(id)
	elseif 序号 == 36 then
		self.临时数据 = {id = 内容,格子 = 参数}
		if self.临时数据.id == 0 then
			self:点化装备处理(id, self.临时数据)
		elseif self.临时数据.id==1008601 then
        	self:装备添加特效处理(id,self.临时数据)
        elseif self.临时数据.id==1008602 then
        	self:装备添加特技处理(id,self.临时数据)
		elseif self:删除道具处理(id, self.临时数据) == false then
			-- Nothing
		elseif self.临时数据.id == -701 then
			self.帮派id = 玩家数据[id].角色.数据.帮派

			if self.帮派id ~= nil and 帮派数据[self.帮派id] ~= nil then
				帮派数据[self.帮派id].资金 = 帮派数据[self.帮派id].资金 + 50000

				if 帮派数据[self.帮派id].资金 > 帮派数据[self.帮派id].金库 * 5000000 + 3000000 then
					帮派数据[self.帮派id].资金 = 帮派数据[self.帮派id].金库 * 5000000 + 3000000
				end

				帮派数据[self.帮派id].繁荣 = 帮派数据[self.帮派id].繁荣 + 1
				帮派数据[self.帮派id].成员名单[id].帮贡.获得 = 帮派数据[self.帮派id].成员名单[id].帮贡.获得 + 5
				帮派数据[self.帮派id].成员名单[id].帮贡.当前 = 帮派数据[self.帮派id].成员名单[id].帮贡.当前 + 5
				玩家数据[id].角色.数据.帮贡 = 帮派数据[self.帮派id].成员名单[id].帮贡.当前
				发送数据(玩家数据[id].连接id, 7, "#y/你获得了5点帮贡")
				广播帮派消息(玩家数据[id].角色.数据.帮派, "#bp/#w/" .. 玩家数据[id].角色.数据.名称 .. "#w/在白虎堂总管处上交了金银宝盒，为帮派增加了50000两的资金。")
			end
		elseif self.临时数据.id == -381 then
			self:分解装备(id, 参数)
		elseif self.临时数据.id == -705 then
			self:添加装备临时效果(id, 参数)
		elseif 任务数据[self.临时数据.id].类型 == 6 then
			任务处理类:完成官职物品任务(id, self.临时数据.id)
		elseif 任务数据[self.临时数据.id].类型 == 14 then
			任务处理类:完成任务链(id, self.临时数据.id)
		elseif 任务数据[self.临时数据.id].类型 == 702 then
			任务处理类:完成青龙任务(self.临时数据.id, id)
		elseif 任务数据[self.临时数据.id].类型 == 16 then
			任务处理类:完成师门任务(id)
		elseif 任务数据[self.临时数据.id].类型 == 306 then
			任务处理类:完成铃铛任务(id, self.临时数据.id)
		elseif 任务数据[self.临时数据.id].类型 == 501 then
			if 任务数据[self.临时数据.id].灵饰 then
				self.临时格子 = 玩家数据[id].角色:取可用道具格子("包裹")
				if self.临时格子 == 0 then
					发送数据(玩家数据[id].连接id, 7, "#y/你的包裹空间已满，无法获得新道具#r/")
				end
				self.临时id = 玩家数据[id].道具:取道具编号()
				玩家数据[id].道具:灵饰处理(id, self.临时id, 任务数据[self.临时数据.id].等级, 1, 任务数据[self.临时数据.id].分类)
				玩家数据[id].道具.数据[self.临时id].名称 = 任务数据[self.临时数据.id].名称
				玩家数据[id].道具.数据[self.临时id].制造者 = 玩家数据[id].角色.数据.名称 .. "强化打造"
				玩家数据[id].道具.数据[self.临时id].等级 = 任务数据[self.临时数据.id].等级
				玩家数据[id].道具.数据[self.临时id].类型 = 任务数据[self.临时数据.id].分类
				玩家数据[id].道具.数据[self.临时id].耐久 = 500
				玩家数据[id].角色.数据.道具.包裹[self.临时格子] = self.临时id
				发送数据(玩家数据[id].连接id, 7, "#y/你获得了#r/" .. 任务数据[self.临时数据.id].名称)
			else
				self.临时id = 玩家数据[id].装备:生成装备(任务数据[self.临时数据.id].分类, 任务数据[self.临时数据.id].名称, 任务数据[self.临时数据.id].等级, 1.3, 1.2, false, 1, 玩家数据[id].角色.数据.名称, nil, nil)
				self.临时格子 = 玩家数据[id].角色:取可用道具格子("包裹")
				发送数据(玩家数据[id].连接id, 7, "#y/你获得了#r/" .. 任务数据[self.临时数据.id].名称)
				if self.临时格子 ~= 0 then
					玩家数据[id].角色.数据.道具.包裹[self.临时格子] = self.临时id
				else
					发送数据(玩家数据[id].连接id, 7, "#y/由于你的包裹空间已满，无法获得新道具#r/" .. 任务数据[self.临时数据.id].名称)
				end
			end
            玩家数据[id].角色:取消任务(玩家数据[id].角色:取任务id(501))
			玩家数据[id].角色:取消任务(self.临时数据.id)
			任务数据[self.临时数据.id] = nil
		end
	elseif 序号 == 37 then
		self:元宵处理(id, {格子 = 参数,类型 = 内容})
	elseif 序号 == 38 then
		self:蟠桃处理(id, {格子 = 参数,类型 = 内容})
	elseif 序号 == 39 then
		self:天眼处理(id, {格子 = 参数,类型 = 内容})
	elseif 序号 == 40 then
		self:修炼果处理(id, {格子 = 参数,类型 = 内容})
	elseif 序号 == 41 then
		self:回天丹处理(id, {格子 = 参数,类型 = 内容})
	elseif 序号 == 42 then
		self:人参果处理(id, {格子 = 参数,类型 = 内容})
	elseif 序号 == 43 then
		self:如意棒处理(id, {格子 = 参数,类型 = 内容})
	elseif 序号 == 44 then
		self:飞行卷处理(id, {格子 = 参数,类型 = 内容})
	elseif 序号 == 45 then
		self:神兜兜处理(id, {格子 = 参数,类型 = 内容})
			elseif 序号 == 2000 then
		self:孤儿名册处理(id, {格子 = 参数,类型 = 内容})
	elseif 序号 == 46 then
		self:修理装备处理(id, {格子 = 参数,id = 内容})
	elseif 序号 == 47 then
		self:变身卡处理(id, {格子 = 参数,类型 = 内容})
	elseif 序号 == 48 then
		self:存钱处理(id, 参数, 内容)
	elseif 序号 == 49 then
		self:取钱处理(id, 参数, 内容)
	elseif 序号 == 50 then
		玩家数据[id].商店处理类:取商品组数据(id, 12)
	elseif 序号 == 51 then
		if 系统消息数据[id] == nil then
			发送数据(玩家数据[id].连接id, 7, "#y/你没有新的系统消息了")
		elseif #系统消息数据[id] >= 1 then
			发送数据(玩家数据[id].连接id, 14, 系统消息数据[id][1])
			table.remove(系统消息数据[id], 1)

			if #系统消息数据[id] <= 0 then
				发送数据(玩家数据[id].连接id, 12, "1")
			end
		else
			发送数据(玩家数据[id].连接id, 7, "#y/你没有新的系统消息了")
		end
	elseif 序号 == 52 then
		if 系统消息数据[id] == nil then
			发送数据(玩家数据[id].连接id, 7, "#y/你没有新的系统消息了")
		elseif #系统消息数据[id] >= 1 then
			发送数据(玩家数据[id].连接id, 14, 系统消息数据[id][1])
			table.remove(系统消息数据[id], 1)

			if #系统消息数据[id] <= 0 then
				发送数据(玩家数据[id].连接id, 12, "1")
			end
		else
			发送数据(玩家数据[id].连接id, 7, "#y/你没有新的系统消息了")
		end
	elseif 序号 == 53 then
		发送数据(玩家数据[id].连接id, 2011, 玩家数据[id].角色.数据.称谓)
	elseif 序号 == 54 then
		玩家数据[id].角色:删除称谓(id, 内容)
	elseif 序号 == 55 then
		玩家数据[id].角色:隐藏称谓(id, 内容)
	elseif 序号 == 56 then
		玩家数据[id].角色:改变称谓(id, 内容)
	elseif 序号 == 57 then
		发送数据(玩家数据[id].连接id, 20017, {
			强化技能 = 玩家数据[id].角色.数据.强化技能,
			门派技能 = 玩家数据[id].角色.数据.门派技能,
			辅助技能 = 玩家数据[id].角色.数据.辅助技能,
			生活技能 = 玩家数据[id].角色.数据.生活技能,
			人物修炼 = 玩家数据[id].角色.数据.人物修炼,
			召唤兽修炼 = 玩家数据[id].角色.数据.召唤兽修炼
		})
	elseif 序号 == 58 then
		self:合成旗处理(id, {格子 = 参数,类型 = 内容})
	elseif 序号 == 59 then
		self:合成旗传送处理(id, 参数)
	elseif 序号 == 60 then
		self:摇钱树处理(id, {格子 = 参数,类型 = 内容})
	elseif 序号 == 61 then
		if 玩家数据[id].角色.数据.帮派 == nil then
			发送数据(玩家数据[id].连接id, 7, "#y/你似乎还没有加入一个帮派哟")

			return 0
		else
			self:获取帮派信息(id, 玩家数据[id].角色.数据.帮派)
		end
	elseif 序号 == 62 then
		self:创建帮派(id, 内容)
	elseif 序号 == 63 then
		if 取帮派踢人权限(id, 玩家数据[id].角色.数据.帮派) == false then
			发送数据(玩家数据[id].连接id, 7, "#y/你没有修改权限")

			return 0
		else
			if 帮派数据[玩家数据[id].角色.数据.帮派].资金 < 帮派数据[玩家数据[id].角色.数据.帮派].规模 * 3000000 then
				发送数据(玩家数据[id].连接id, 7, "#y/你帮派的资金不足")

				return 0
			end

			帮派数据[玩家数据[id].角色.数据.帮派].宗旨 = 内容
			self.消耗资金 = 0

			if 帮派数据[玩家数据[id].角色.数据.帮派].修改宗旨 == false then
				帮派数据[玩家数据[id].角色.数据.帮派].修改宗旨 = true
			else
				self.消耗资金 = 500000
			end

			帮派数据[玩家数据[id].角色.数据.帮派].资金 = 帮派数据[玩家数据[id].角色.数据.帮派].资金 - self.消耗资金

			发送数据(玩家数据[id].连接id, 7, "#y/修改帮派宗旨成功！")
			广播帮派消息(玩家数据[id].角色.数据.帮派, "#bp/#g/" .. 玩家数据[id].角色.数据.名称 .. "#y/修改了帮派宗旨。本次操作消耗了#r/" .. self.消耗资金 .. "#y/两帮派资金。")
		end
	elseif 序号 == 64 then
		if 玩家数据[id].角色.数据.帮派 ~= nil then
			发送数据(玩家数据[id].连接id, 7, "#y/请先脱离原有的帮派")

			return 0
		end

		if 帮派数据[参数] == nil then
			发送数据(玩家数据[id].连接id, 7, "#y/这个帮派并不存在")

			return 0
		end

		if 帮派数据[参数].申请名单[id] == nil then
			帮派数据[参数].申请名单[id] = {
				日期 = os.time(),
				-- 日期文本 = 测试时间(os.time()),
				id = id,
				名称 = 玩家数据[id].角色.数据.名称,
				门派 = 玩家数据[id].角色.数据.门派,
				等级 = 玩家数据[id].角色.数据.等级
			}

			广播帮派消息(参数, "#bp/#g/" .. 玩家数据[id].角色.数据.名称 .. "#y/申请加入本帮，请尽快处理")
			发送数据(玩家数据[id].连接id, 7, "#y/提交入帮申请成功")
		else
			发送数据(玩家数据[id].连接id, 7, "#y/你已经在对方的申请名单里了，请耐心等待")
		end
	elseif 序号 == 65 then
		if 玩家数据[id].角色.数据.帮派 == nil then
			发送数据(玩家数据[id].连接id, 7, "#y/你似乎还没有帮派")

			return 0
		elseif 帮派数据[玩家数据[id].角色.数据.帮派] == nil then
			发送数据(玩家数据[id].连接id, 7, "#y/帮派数据异常")

			return 0
		else
			self.发送信息 = {}

			for n, v in pairs(帮派数据[玩家数据[id].角色.数据.帮派].申请名单) do
				if 玩家数据[n] ~= nil then
					self.发送信息[#self.发送信息 + 1] = 帮派数据[玩家数据[id].角色.数据.帮派].申请名单[n]
				end
			end
		end

		发送数据(玩家数据[id].连接id, 20022, self.发送信息)
	elseif 序号 == 66 then
		参数 = 参数 + 0

		if 取帮派踢人权限(id, 玩家数据[id].角色.数据.帮派) == false then
			发送数据(玩家数据[id].连接id, 7, "#y/你没有此项操作权限")

			return 0
		end

		帮派数据[玩家数据[id].角色.数据.帮派].申请名单 = {}

		广播帮派消息(玩家数据[id].角色.数据.帮派, "#bp/#g/" .. 玩家数据[id].角色.数据.名称 .. "#r/清空了帮派申请名单")
		发送数据(玩家数据[id].连接id, 7, "#y/清空申请列表成功")

		self.发送信息 = {}

		for n, v in pairs(帮派数据[玩家数据[id].角色.数据.帮派].申请名单) do
			if 玩家数据[n] ~= nil then
				self.发送信息[#self.发送信息 + 1] = 帮派数据[玩家数据[id].角色.数据.帮派].申请名单[n]
			end
		end

		发送数据(玩家数据[id].连接id, 20022, self.发送信息)
	elseif 序号 == 67 then
		参数 = 参数 + 0

		if 取帮派加人权限(id, 玩家数据[id].角色.数据.帮派) == false then
			发送数据(玩家数据[id].连接id, 7, "#y/你没有此项操作权限")

			return 0
		else
			广播帮派消息(玩家数据[id].角色.数据.帮派, "#bp/#g/" .. 玩家数据[id].角色.数据.名称 .. "#r/拒绝了#g/" .. 帮派数据[玩家数据[id].角色.数据.帮派].申请名单[参数].名称 .. "#r/的入帮申请")

			帮派数据[玩家数据[id].角色.数据.帮派].申请名单[参数] = nil

			发送数据(玩家数据[id].连接id, 7, "#y/你拒绝了对方的入帮申请")

			self.发送信息 = {}

			for n, v in pairs(帮派数据[玩家数据[id].角色.数据.帮派].申请名单) do
				if 玩家数据[n] ~= nil then
					self.发送信息[#self.发送信息 + 1] = 帮派数据[玩家数据[id].角色.数据.帮派].申请名单[n]
				end
			end

			发送数据(玩家数据[id].连接id, 20022, self.发送信息)
		end
	elseif 序号 == 68 then
		参数 = 参数 + 0

		if 取帮派加人权限(id, 玩家数据[id].角色.数据.帮派) == false then
			发送数据(玩家数据[id].连接id, 7, "#y/你没有此项操作权限")

			return 0
		elseif 玩家数据[参数] == nil then
			发送数据(玩家数据[id].连接id, 7, "#y/对方不在线，无法进行此操作")

			return 0
		elseif 玩家数据[参数].角色.数据.帮派 ~= nil then
			发送数据(玩家数据[id].连接id, 7, "#y/对方已经加入了其它帮派")

			帮派数据[玩家数据[id].角色.数据.帮派].申请名单[参数] = nil
			self.发送信息 = {}

			for n, v in pairs(帮派数据[玩家数据[id].角色.数据.帮派].申请名单) do
				if 玩家数据[n] ~= nil then
					self.发送信息[#self.发送信息 + 1] = 帮派数据[玩家数据[id].角色.数据.帮派].申请名单[n]
				end
			end

			发送数据(玩家数据[id].连接id, 20022, self.发送信息)

			return 0
		elseif 帮派数据[玩家数据[id].角色.数据.帮派].厢房 * 20 + 20 <= self:取帮派人数(玩家数据[id].角色.数据.帮派) then
			发送数据(玩家数据[id].连接id, 7, "#y/本帮人数已达上限，无法接收新的帮众")

			return 0
		else
			广播帮派消息(玩家数据[id].角色.数据.帮派, "#bp/#g/" .. 玩家数据[id].角色.数据.名称 .. "#r/批准了#g/" .. 帮派数据[玩家数据[id].角色.数据.帮派].申请名单[参数].名称 .. "#r/的入帮申请")

			帮派数据[玩家数据[id].角色.数据.帮派].申请名单[参数] = nil

			发送数据(玩家数据[id].连接id, 7, "#y/你批准了对方的入帮申请")

			玩家数据[参数].角色.数据.帮派 = 玩家数据[id].角色.数据.帮派
			帮派数据[玩家数据[id].角色.数据.帮派].成员名单[参数] = {
				职务 = "帮众",
				id = 参数,
				名称 = 玩家数据[参数].角色.数据.名称,
				帮贡 = {
					当前 = 玩家数据[参数].角色.数据.帮贡,
					获得 = 0
				},
				入帮时间 = os.time(),
				离线时间 = os.time()
			}

			发送数据(玩家数据[参数].连接id, 7, "#y/你成功加入了#r/" .. 帮派数据[玩家数据[id].角色.数据.帮派].名称 .. "#y/帮派")

			self.发送信息 = {}

			for n, v in pairs(帮派数据[玩家数据[id].角色.数据.帮派].申请名单) do
				if 玩家数据[n] ~= nil then
					self.发送信息[#self.发送信息 + 1] = 帮派数据[玩家数据[id].角色.数据.帮派].申请名单[n]
				end
			end

			发送数据(玩家数据[id].连接id, 20022, self.发送信息)
		end
	elseif 序号 == 69 then
		if 玩家数据[id].角色.数据.帮派 == nil then
			发送数据(玩家数据[id].连接id, 7, "#y/你似乎还没有加入一个帮派哟")

			return 0
		elseif 帮派数据[玩家数据[id].角色.数据.帮派].成员名单[id].职务 ~= "帮众" then
			发送数据(玩家数据[id].连接id, 7, "#y/请先解除您的帮派职务")

			return 0
		else
			帮派数据[玩家数据[id].角色.数据.帮派].成员名单[id] = nil

			广播帮派消息(玩家数据[id].角色.数据.帮派, "#bp/#g/" .. 玩家数据[id].角色.数据.名称 .. "#w/退出了本帮")

			玩家数据[id].角色.数据.帮派 = nil

			发送数据(玩家数据[id].连接id, 20023, "1")
			发送数据(玩家数据[id].连接id, 7, "#y/脱离帮派成功")
		end
	elseif 序号 == 70 then
		if 玩家数据[id].角色.数据.帮派 == nil then
			发送数据(玩家数据[id].连接id, 7, "#y/你似乎还没有加入一个帮派哟")

			return 0
		elseif 取帮派踢人权限(id, 玩家数据[id].角色.数据.帮派) == false then
			发送数据(玩家数据[id].连接id, 7, "#y/你没有此种操作的权限")

			return 0
		elseif 帮派数据[玩家数据[id].角色.数据.帮派].成员名单[参数].职务 ~= "帮众" then
			发送数据(玩家数据[id].连接id, 7, "#y/请先解除对方的帮派职务")

			return 0
		else
			参数 = 参数 + 0

			广播帮派消息(玩家数据[id].角色.数据.帮派, "#bp/#g/" .. 玩家数据[id].角色.数据.名称 .. "#y/动用权限将#r/" .. 帮派数据[玩家数据[id].角色.数据.帮派].成员名单[参数].名称 .. "#y/踢出了本帮")

			帮派数据[玩家数据[id].角色.数据.帮派].成员名单[参数] = nil

			self:获取帮派信息(id, 玩家数据[id].角色.数据.帮派)

			if 玩家数据[参数] ~= nil then
				玩家数据[参数].角色.数据.帮派 = nil

				发送数据(玩家数据[参数].连接id, 20023, "1")
				发送数据(玩家数据[参数].连接id, 7, "#y/你已经被请离了本帮！")
			end

			发送数据(玩家数据[id].连接id, 7, "#y/请离帮派成员成功！")
		end
	elseif 序号 == 71 then
		玩家数据[id].帮派操作id = 参数 + 0
		self.连接id内容 = [[
 请选择您要任命的职务：

  #R/ht|296-3-4/副帮主
  #R/ht|296-3-5/左护法
  #R/ht|296-3-6/右护法
  #R/ht|296-3-7/长老
  #R/ht|296-3-8/帮众

     ]]
		self.发送信息 = {
			名称 = "???",
			对话 = self.连接id内容
		}

		发送数据(玩家数据[id].连接id,20,self.发送信息)
	elseif 序号 == 72 then
		self:设置内政(id, 内容)
	elseif 序号 == 74 then
		self:索要灵饰(id)
	elseif 序号 == 75 then
		self:佩戴灵饰(id, 参数, 内容)
	elseif 序号 == 76 then
		self:超级回天丹处理(id, {
			格子 = 参数,
			类型 = 内容
		})
	  elseif 序号==77 then
    self:易经丹使用(id,{格子=参数,类型=内容})
  elseif 序号==78 then
     self:清灵仙露使用(id,{格子=参数,类型=内容})
  elseif 序号==79 then
    self:清灵净瓶使用(id,{格子=参数,类型=内容})
  elseif 序号==80 then
  self:玉葫灵髓使用(id,{格子=参数,类型=内容})
  elseif 序号==81 then
  	玩家数据[id].商店处理类:取商城数据(id, 18)
  elseif 序号==82 then
  	玩家数据[id].商店处理类:切换商城数据(id, 18)
  elseif 序号==83 then
  	玩家数据[id].商店处理类:切换商城数据(id, 19)
  elseif 序号==84 then
  	玩家数据[id].商店处理类:切换商城数据(id, 20)
  elseif 序号==85 then
  	玩家数据[id].商店处理类:取神兽商城数据(id, 99)
  elseif 序号==86 then
  	玩家数据[id].商店处理类:取神兽商城数据(id,100)
  elseif 序号 == 87 then
	self:金银宝盒处理(id, {格子 = 参数,类型 = 内容})
  elseif 序号 == 88 then
	self:小雪块处理(id, {格子 = 参数,类型 = 内容})
  elseif 序号 == 89 then
	self:大雪块处理(id, {格子 = 参数,类型 = 内容})
   elseif 序号==185 then
     self:符石升级处理(id,内容)
   elseif 序号==186 then
     self:符石激活处理(id,{格子=参数,类型=内容})
   elseif 序号==187 then
   	self:新玩家礼包处理(id,{格子=参数,类型=内容})
   elseif 序号==188 then
   	self:双倍经验丹处理(id,{格子=参数,类型=内容})
   elseif 序号==189 then
   	self:级120礼包处理(id,{格子=参数,类型=内容})
   elseif 序号==190 then
   	self:级150礼包处理(id,{格子=参数,类型=内容})
   elseif 序号==191 then
   	self:银币礼包处理(id,{格子=参数,类型=内容})
    elseif 序号==192 then
   	self:储备礼包处理(id,{格子=参数,类型=内容})
   	elseif 序号==193 then
   	self:玄天残卷处理(id,{格子=参数,类型=内容})
     elseif 序号==194 then
   	self:召唤兽饰品处理(id,{格子=参数,类型=内容})
   	 elseif 序号==195 then
   	self:级160礼包处理(id,{格子=参数,类型=内容})
   	 elseif 序号==196 then
   	self:元30礼包处理(id,{格子=参数,类型=内容})
   	 elseif 序号==197 then
   	self:元88礼包处理(id,{格子=参数,类型=内容})
   	 elseif 序号==198 then
   	self:元188礼包处理(id,{格子=参数,类型=内容})
   	 elseif 序号==199 then
   	self:星辉石礼包处理(id,{格子=参数,类型=内容})
   	 elseif 序号==200 then
   	self:太阳石礼包处理(id,{格子=参数,类型=内容})
   	 elseif 序号==201 then
   	self:图册礼包处理(id,{格子=参数,类型=内容})
	elseif 序号 == 202 then
		self:索要锦衣(id)
	elseif 序号 == 203 then
		self:佩戴锦衣(id, 参数, 内容)
	elseif 序号 == 204 then
		玩家数据[id].删除内容={格子 = 参数,类型 = 内容}
		发送数据(玩家数据[id].连接id, 3011, {id = -381,道具 = 玩家数据[id].道具:索要道具2(id, "包裹"),格子 = 参数,类型 = 内容})
	elseif 序号 == 205 then
		self:九转金丹处理(id, {格子 = 参数,类型 = 内容})
	elseif 序号 == 206 then
		self:佩戴法宝(id, 参数, 内容)
	elseif 序号 == 207 then
		self:法宝碎片处理(id,{格子 = 参数,类型 = 内容})
	elseif 序号 == 208 then
		self:法宝修炼处理(id,{格子 = 参数,类型 = 内容})
	elseif 序号 == 209 then
		self:法宝定位处理(id,{格子 = 参数,类型 = 内容})
	elseif 序号 == 210 then
		self:法宝传送处理(id, {格子 = 参数,类型 = 内容})
    elseif 序号==211 then
  	玩家数据[id].商店处理类:取积分商城数据(id, 101)
    elseif 序号==212 then
  	玩家数据[id].商店处理类:取积分商城数据2(id, 102)
    elseif 序号==213 then
  	玩家数据[id].商店处理类:取积分商城数据3(id, 103)
    elseif 序号==214 then
  	玩家数据[id].商店处理类:取积分商城数据4(id, 104)
    elseif 序号==215 then
  	玩家数据[id].商店处理类:取积分商城数据5(id, 105)
   	 elseif 序号==216 then
   	self:月亮石礼包处理(id,{格子=参数,类型=内容})
   	 elseif 序号==217 then
   	self:舍利子礼包处理(id,{格子=参数,类型=内容})
   	 elseif 序号==218 then
   	self:黑宝石礼包处理(id,{格子=参数,类型=内容})
   	 elseif 序号==219 then
   	self:光芒石礼包处理(id,{格子=参数,类型=内容})
   	 elseif 序号==220 then
   	self:红玛瑙礼包处理(id,{格子=参数,类型=内容})
   	elseif 序号 == 888 then
    任务处理类:统计实力榜(id)
  end
end
function 道具处理类:法宝碎片处理(id, 数据)
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]

	if self.数据[self.临时id1] == nil or self.数据[self.临时id1] == 0 then
		发送数据(玩家数据[id].连接did, 7, "#y/你似乎并没有这样的道具")

		return 0
	end

	if self.数据[self.临时id1].数量 ~= nil and self.数据[self.临时id1].数量 >= 99 then

		self.随机法宝 = {  --输入法宝名字
			"碧玉葫芦",
			"飞剑",
			"拭剑石",
			"金甲仙衣",
			"嗜血幡",
			"风袋",
			"九黎战鼓",
			"盘龙壁",
			"神行飞剑",
			"汇灵盏",
			"织女扇",
			"五彩娃娃",
			"普渡",
			"混元伞",
			"金刚杵",
			"兽王令",
			"五火神焰印",
			"附灵玉",
			"月光宝盒",
			"通灵宝玉",
			"重明战鼓",
			"梦云幻甲",
			"蟠龙玉璧",
			"落星飞鸿",
			"流影云笛",
			"月影",
			"斩魔",
			"金蟾",
			"千斗金樽",
			"宿幕星河",
			"分水",
			"赤焰",
			"神木宝鼎",
			"宝烛",
			"救命毫毛",
			"镇海珠",
			--"聚宝盆",
			"凌波仙符",
			"河图洛书",
			"归元圣印"

		}  --那些是3级  那些是2级

		self.临时格子 = 玩家数据[id].角色:取可用道具格子("包裹")

		if self.临时格子 == 0 then
			发送数据(玩家数据[id].连接did, 7, "#y/您身上似乎没有多余的道具存放空间")
			return 0
		else
			self.道具编号 = 玩家数据[id].道具:取道具编号()
			玩家数据[id].角色.数据.道具.包裹[self.临时格子] = self.道具编号
			self.物品名称 = self.随机法宝[取随机数(1,#self.随机法宝)]
			self.物品等级 = 法宝名称算等级(self.物品名称)
			if self.物品等级 == 4 then
			   self.灵气上限 = 500
			elseif self.物品等级 == 3 then
			   self.灵气上限 = 400
			else
			   self.灵气上限 = 300
			end

			玩家数据[id].道具.数据[self.道具编号] = {类型 = "法宝",名称 = self.物品名称,等级 = self.物品等级,灵气 = 200,灵气上限 = self.灵气上限,层数 = 0,五行 = 生成五行(),修炼经验 = 0,升级经验 = 法宝消耗[1]}
			玩家数据[id].道具:增加识别码(id, self.道具编号)
			self.数据[self.临时id1] = nil
			玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil
			发送数据(玩家数据[id].连接did, 3006, "66")
		end

	else
		发送数据(玩家数据[id].连接did, 7, "#y/只有集齐99个法宝碎片才可兑换法宝哦")
	end
end
function 道具处理类:法宝定位处理(id, 数据)
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
	if self.数据[self.临时id1] == nil or self.数据[self.临时id1] == 0 then
		发送数据(玩家数据[id].连接did, 7, "#y/你似乎并没有这样的道具")
		return 0
	end
	local mapname = 地图处理类.地图数据[玩家数据[id].地图].名称
	if mapname == '观星台' or mapname == '乾坤九曜图' or mapname == '群雄逐鹿' or 
		string.find(mapname, '副本')  ~= nil or 
		string.find(mapname, '幻域')  ~= nil or 
		string.find(mapname, '比武')  ~= nil then
		发送数据(玩家数据[id].连接did, 7, "#y/这里不可以定位")
		return 0
	end
	self.数据[self.临时id1].地图数据 = {编号 = 玩家数据[id].地图,名称= name ,坐标x=玩家数据[id].角色.数据.地图数据.x,坐标y=玩家数据[id].角色.数据.地图数据.y}
	发送数据(玩家数据[id].连接did, 7, "#y/定位成功")
	发送数据(玩家数据[id].连接did, 3006, "66")
end

function 道具处理类:法宝修炼处理(id, 数据)
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
	if self.数据[self.临时id1] == nil or self.数据[self.临时id1] == 0 then
		发送数据(玩家数据[id].连接did, 7, "#y/你似乎并没有这样的道具")
		return 0
	end

	if 玩家数据[id].角色.数据.当前经验 < 1000000 then
		发送数据(玩家数据[id].连接did, 7, "#y/你经验不够")

		return 0
	elseif self.数据[self.临时id1].层数 >=9 then
		发送数据(玩家数据[id].连接did, 7, "#y/到顶了")

		return 0
	else
		self.数据[self.临时id1].修炼经验 = self.数据[self.临时id1].修炼经验 + 1000000
		玩家数据[id].角色:扣除经验(id, 1000000)
		发送数据(玩家数据[id].连接did, 7, "#y/修炼成功，您获得了法宝经验！")
		if self.数据[self.临时id1].修炼经验 >= self.数据[self.临时id1].升级经验 then
			self.数据[self.临时id1].层数 = self.数据[self.临时id1].层数 +1
			self.数据[self.临时id1].修炼经验 = self.数据[self.临时id1].修炼经验 - self.数据[self.临时id1].升级经验
			self.数据[self.临时id1].升级经验 = 法宝消耗[self.数据[self.临时id1].层数+1]
			发送数据(玩家数据[id].连接did, 7, "#y/你的法宝提升了")
		end
		发送数据(玩家数据[id].连接did, 3006, "66")
	end
end
function 道具处理类:佩戴法宝(id, 格子, 类型)
	self.临时数据 = {
		格子 = 格子,
		当前类型 = 包裹转换(类型)
	}
	self.法宝满 = 0
	if 玩家数据[id].角色.数据.法宝 == nil then
		玩家数据[id].角色.数据.法宝 = {}
	end
	if 玩家数据[id].角色.数据.道具.法宝 == nil then
		玩家数据[id].角色.数据.道具.法宝 = {}
	end
	if self.临时数据.格子 >= 35 then
		self.临时格子 = 玩家数据[id].角色:取可用道具格子(self.临时数据.当前类型)
		if self.临时格子 == 0 then
			发送数据(玩家数据[id].连接did, 7, "#y/您的" .. self.临时数据.当前类型 .. "已经无法存储更多的道具了")

			return 0
		else
			玩家数据[id].角色.数据.道具["法宝"][self.临时格子] = 玩家数据[id].角色.数据.法宝[self.临时数据.格子]
			玩家数据[id].角色.数据.法宝[self.临时数据.格子] = 0
			self.道具编号 = 玩家数据[id].角色.数据.道具["法宝"][self.临时格子]
			if 玩家数据[id].道具.数据[self.道具编号].名称 == "通灵宝玉" then
				玩家数据[id].角色.数据.通灵宝玉 = nil
			elseif 玩家数据[id].道具.数据[self.道具编号].名称 == "聚宝盆" then
				玩家数据[id].角色.数据.聚宝盆 = nil
			end
			发送数据(玩家数据[id].连接did, 3006, "66")
			return 0
		end
	else
		self.道具编号 = 玩家数据[id].角色.数据.道具["法宝"][self.临时数据.格子]
		-- if 玩家数据[id].道具.数据[self.道具编号].等级 >= 4 and 玩家数据[id].角色.数据.等级 < 120 then
		-- 	发送数据(玩家数据[id].连接did, 7, "#y/您的等级不足以佩戴这种法宝")

		-- 	return 0
		-- elseif 玩家数据[id].道具.数据[self.道具编号].等级 >= 3 and 玩家数据[id].角色.数据.等级 < 100 then
		-- 	发送数据(玩家数据[id].连接did, 7, "#y/您的等级不足以佩戴这种法宝")

		-- 	return 0
		-- elseif 玩家数据[id].道具.数据[self.道具编号].等级 >= 2 and 玩家数据[id].角色.数据.等级 < 80 then
		-- 	发送数据(玩家数据[id].连接did, 7, "#y/您的等级不足以佩戴这种法宝")

		-- 	return 0
		-- elseif 玩家数据[id].道具.数据[self.道具编号].等级 >= 1 and 玩家数据[id].角色.数据.等级 < 60 then
		-- 	发送数据(玩家数据[id].连接did, 7, "#y/您的等级不足以佩戴这种法宝")

		-- 	return 0
		-- end

		for i=35,38 do
			if 玩家数据[id].角色.数据.法宝[i] == nil or 玩家数据[id].角色.数据.法宝[i] == 0 then --判断已经佩带法宝是否剩余位置
				玩家数据[id].角色.数据.道具["法宝"][self.临时数据.格子] = nil  --如果有剩余位置 把法宝背包里面的物品NIL
				self.法宝满 = i
				break
			end
		end
		if self.法宝满 ~= 0 then
			玩家数据[id].角色.数据.法宝[self.法宝满] = self.道具编号  -- 将空余的位置  放入 佩带的法宝
			if 玩家数据[id].道具.数据[self.道具编号].名称 == "通灵宝玉" then
				玩家数据[id].角色.数据.通灵宝玉 = 玩家数据[id].道具.数据[self.道具编号].层数+1
			elseif 玩家数据[id].道具.数据[self.道具编号].名称 == "聚宝盆" then
				玩家数据[id].角色.数据.聚宝盆 = 玩家数据[id].道具.数据[self.道具编号].层数+1
			end
		else
			玩家数据[id].角色.数据.道具["法宝"][self.临时数据.格子] = 玩家数据[id].角色.数据.法宝[35]  -- 如果没有空余位置 将35号位置的法宝放到需要穿戴的法宝
			玩家数据[id].角色.数据.法宝[35] = self.道具编号 --将佩带的法宝35好位置 放入穿戴的法宝
			if 玩家数据[id].道具.数据[self.道具编号].名称 == "通灵宝玉" then
				玩家数据[id].角色.数据.通灵宝玉 = 玩家数据[id].道具.数据[self.道具编号].层数+1
			elseif 玩家数据[id].道具.数据[self.道具编号].名称 == "聚宝盆" then
				玩家数据[id].角色.数据.聚宝盆 = 玩家数据[id].道具.数据[self.道具编号].层数+1
			end
		end
		发送数据(玩家数据[id].连接did, 3006, "66")

	end
end


function 道具处理类:符石激活处理(id,数据)---------------------
  local fsgz=玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
 if 玩家数据[id].角色.数据.道具[数据.类型][数据.格子] ==nil or 玩家数据[id].角色.数据.道具[数据.类型][数据.格子] == 0 then
    发送数据(玩家数据[id].连接id,7,"#y/物品不存在")
  return
 else
   local  jhjl = 0
   local  jhjq = self.数据[fsgz].等级*50000
   local  jhjy = 玩家数据[id].角色.数据.等级*self.数据[fsgz].等级*20000
    if  self.数据[fsgz].等级 ==1 then
       jhjl= 100
   elseif  self.数据[fsgz].等级 ==2 then
     jhjl=90
   elseif  self.数据[fsgz].等级 ==3 then
     jhjl= 80
   end

        if 银子检查(id,jhjq)==false then
        发送数据(玩家数据[id].连接id,7,"#y/你没那么多的银子")
        return 0
        elseif 玩家数据[id].角色.数据.当前经验<jhjy then
        发送数据(玩家数据[id].连接id,7,"#y/你没那么多的经验")
        return 0
        end
   if 取随机数()<=jhjl then
          玩家数据[id].角色:扣除银子(id,jhjq,"符石激活")
          玩家数据[id].角色.数据.当前经验=玩家数据[id].角色.数据.当前经验-jhjy
          发送数据(玩家数据[id].连接id,7,"#y/你消耗了".."#r/"..jhjq.."金钱"..jhjy.."经验")
          发送数据(玩家数据[id].连接id,7,"#y/符石激活成功")
          self:新加给予道具(id,取符石(self.数据[fsgz].等级))
          self.数据[fsgz]  = nil
          玩家数据[id].角色.数据.道具[数据.类型][数据.格子] =nil
   else
        玩家数据[id].角色:扣除银子(id,jhjq,"符石激活")
         玩家数据[id].角色.数据.当前经验=玩家数据[id].角色.数据.当前经验-jhjy
         发送数据(玩家数据[id].连接id,7,"#y/你消耗了".."#r/"..jhjq.."金钱"..jhjy.."经验")
        发送数据(玩家数据[id].连接id,7,"#y/符石激活失败")
   end
     发送数据(玩家数据[id].连接id,3006,"66")

 end
 end
function 道具处理类:符石升级处理(id,格子)--------------------
  local 临时格子=分割文本(格子,"*-*")
  local 符石id = {}
  self.符石={}
  local 序号 = 0
	local 等级 ={}
	self.找到卷轴= false
	 for i=1,4 do
	  临时格子[i]=临时格子[i]+0
	   符石id[i]=玩家数据[id].角色.数据.道具.包裹[临时格子[i]+0]
	  self.符石[i]= false
	    if 玩家数据[id].角色.数据.道具.包裹[临时格子[i]]==nil or  玩家数据[id].道具.数据[符石id[i]]==nil  then
	      发送数据(玩家数据[id].连接id,7,"#y/数据错误，道具不存在")
	     return 0
	    end
	    if 玩家数据[id].道具.数据[符石id[i]].名称 ~= "符石卷轴" and  玩家数据[id].道具.数据[符石id[i]].类型 == "符石" then
	         序号= 序号+1
	       等级[#等级+1] = 符石id[i]
	    end
	 end

	  if 玩家数据[id].道具.数据[符石id[1]].名称=="符石卷轴" or 玩家数据[id].道具.数据[符石id[2]].名称=="符石卷轴" or 玩家数据[id].道具.数据[符石id[3]].名称=="符石卷轴"or 玩家数据[id].道具.数据[符石id[4]].名称=="符石卷轴" then
	    self.找到卷轴 =true
	  else
	    发送数据(玩家数据[id].连接id,7,"#y/未找到符石卷轴")
	     return  0
	  end
	 if 序号 >= 3 and 玩家数据[id].道具.数据[等级[1]].等级 == 玩家数据[id].道具.数据[等级[2]].等级 and 玩家数据[id].道具.数据[等级[1]].等级 == 玩家数据[id].道具.数据[等级[3]].等级 then
	     local 合成几率
	   if  玩家数据[id].道具.数据[等级[1]].等级==1 then ---合成几率 90代表90% 成功
	     合成几率=90
	   elseif 玩家数据[id].道具.数据[等级[1]].等级==2 then
	     合成几率=80
	  elseif 玩家数据[id].道具.数据[等级[1]].等级==3 then ---最高3级 以后有4级可以在这里加
	     合成几率=70
	   end
	   if  取随机数()>=合成几率 then

	         发送数据(玩家数据[id].连接id,7,"#y/合成失败，你损失了一个符石卷轴和3块符石")
	   else
	     self:新加给予道具(id,取符石(玩家数据[id].道具.数据[等级[1]].等级+1))

	    end
	         for i=1,4 do
	       玩家数据[id].道具.数据[符石id[i]] =nil
	       玩家数据[id].角色.数据.道具.包裹[临时格子[i]] =nil
	     end
	      发送数据(玩家数据[id].连接id,3006,"66")
	 else
	   发送数据(玩家数据[id].连接id,7,"#y/合成符石需要3块同等级符石合成")
	   return
	 end
 end
function 道具处理类:染色处理(id, 参数, 序号, 内容)----------------
	self.染色参数 = {参数,序号,内容 + 0}
	self.花豆消耗 = 0
	self.彩果消耗 = 0
	if self.染色参数 == nil or #self.染色参数 < 3 then
		发送数据(玩家数据[id].连接id, 7, "#y/染色参数异常，请重新选择染色方案")
		return 0
	end
 if self.染色参数[1] ~= 玩家数据[id].角色.数据.染色.a then
      if self.染色参数[1]<3 then
      self.花豆消耗=self.花豆消耗+10
      else
      self.彩果消耗=self.彩果消耗+10
      end
  end
  if self.染色参数[2] ~= 玩家数据[id].角色.数据.染色.b then
      if self.染色参数[2]<3 then
      self.花豆消耗=self.花豆消耗+10
      else
      self.彩果消耗=self.彩果消耗+10
      end
  end
    if self.染色参数[3] ~= 玩家数据[id].角色.数据.染色.c then
      if self.染色参数[3]<3 then
      self.花豆消耗=self.花豆消耗+10
      else
      self.彩果消耗=self.彩果消耗+10
      end
  end
	self.花豆计算 = {}
	self.花豆满足 = false
	self.彩果计算 = {}
	self.彩果满足 = false
	 if self.花豆消耗>0 then
	   for n=1,20 do
	     if self.花豆消耗>0 then
	       if self.花豆消耗>0 and  玩家数据[id].角色.数据.道具.包裹[n]~=nil and 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[n]]~=nil and  玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[n]].名称=="花豆" then
	         if 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[n]].数量==nil then
	            self.花豆消耗=self.花豆消耗-1
	            self.花豆计算[#self.花豆计算+1]={格子=n,数量=1}
	          elseif 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[n]].数量>=self.花豆消耗 then
	            self.花豆计算[#self.花豆计算+1]={格子=n,数量=self.花豆消耗}
	            self.花豆消耗=0
	          else
	           self.花豆计算[#self.花豆计算+1]={格子=n,数量=玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[n]].数量}
	           self.花豆消耗=self.花豆消耗-玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[n]].数量
	           end
	         if self.花豆消耗==0 then
	            self.花豆满足=true
	           end
	         end
	       end
	     end
	   end
	 if self.彩果消耗>0 then
	   for n=1,20 do
	     if self.彩果消耗>0 then
	       if self.彩果消耗>0 and  玩家数据[id].角色.数据.道具.包裹[n]~=nil and 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[n]].名称=="彩果" then
	         if 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[n]].数量==nil then
	            self.彩果消耗=self.彩果消耗-1
	            self.彩果计算[#self.彩果计算+1]={格子=n,数量=1}
	          elseif 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[n]].数量>=self.彩果消耗 then
	            self.彩果计算[#self.彩果计算+1]={格子=n,数量=self.彩果消耗}
	            self.彩果消耗=0
	          else
	           self.彩果计算[#self.彩果计算+1]={格子=n,数量=玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[n]].数量}
	           self.彩果消耗=self.彩果消耗-玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[n]].数量
	           end
	         if self.彩果消耗==0 then
	            self.彩果满足=true
	           end
	         end
	       end
	     end
	   end

   if self.彩果消耗>0  and self.花豆消耗>0  then
    发送数据(玩家数据[id].连接id,7,"#Y/您还需要#R/"..self.彩果消耗.."#Y/个彩果和#R/"..self.花豆消耗.."#Y/个花豆才能染色")
   return 0
   elseif self.彩果消耗>0 then
   发送数据(玩家数据[id].连接id,7,"#Y/您还需要#R/"..self.彩果消耗.."#Y/个彩果才能染色")
   return 0
   elseif self.花豆消耗>0 then
    发送数据(玩家数据[id].连接id,7,"#Y/您还需要#R/"..self.花豆消耗.."#Y/个花豆才能染色")
    return 0
  else
    if #self.花豆计算>0 then
      for n=1,#self.花豆计算 do
       self.临时格子=self.花豆计算[n].格子
       self.临时id=玩家数据[id].角色.数据.道具.包裹[self.临时格子]
       if 玩家数据[id].道具.数据[self.临时id].数量==nil then
          玩家数据[id].道具.数据[self.临时id]=0
          玩家数据[id].角色.数据.道具.包裹[self.临时格子]=nil
        else
          玩家数据[id].道具.数据[self.临时id].数量=玩家数据[id].道具.数据[self.临时id].数量-self.花豆计算[n].数量
          if 玩家数据[id].道具.数据[self.临时id].数量<=0 then

           玩家数据[id].道具.数据[self.临时id]=0
           玩家数据[id].角色.数据.道具.包裹[self.临时格子]=nil
            end
         end
        end
      end
    if #self.彩果计算>0 then
      for n=1,#self.彩果计算 do
       self.临时格子=self.彩果计算[n].格子
       self.临时id=玩家数据[id].角色.数据.道具.包裹[self.临时格子]
       if 玩家数据[id].道具.数据[self.临时id].数量==nil then
          玩家数据[id].道具.数据[self.临时id]=0
          玩家数据[id].角色.数据.道具.包裹[self.临时格子]=nil
        else
          玩家数据[id].道具.数据[self.临时id].数量=玩家数据[id].道具.数据[self.临时id].数量-self.彩果计算[n].数量
          if 玩家数据[id].道具.数据[self.临时id].数量<=0 then
           玩家数据[id].道具.数据[self.临时id]=0
           玩家数据[id].角色.数据.道具.包裹[self.临时格子]=nil
            end
         end
        end
      end
   end
	玩家数据[id].角色.数据.染色.a = self.染色参数[1]
	玩家数据[id].角色.数据.染色.b = self.染色参数[2]
	玩家数据[id].角色.数据.染色.c = self.染色参数[3]
    地图处理类:发送数据(id,1016,玩家数据[id].角色:获取角色数据(),玩家数据[id].地图)
	发送数据(玩家数据[id].连接id, 2020, table.tostring(玩家数据[id].角色.数据.染色))
	发送数据(玩家数据[id].连接id, 3006, "66")
	发送数据(玩家数据[id].连接id, 7, "#y/染色成功")
 end
function 道具处理类:洞冥草处理(id, 格子, 类型)------------
	local 数据 = {
		类型 = 类型,
		格子 = 格子
	}

	if 玩家数据[id].角色:取任务id(1) ~= 0 then
		玩家数据[id].角色:取消任务(玩家数据[id].角色:取任务id(1))
		任务数据[玩家数据[id].角色:取任务id(1)] = nil
		发送数据(玩家数据[id].连接id, 7, "#y/你取消了摄妖香效果")
	else
		发送数据(玩家数据[id].连接id, 7, "#y/你似乎并没有使用过摄妖香")
		return 0
	end

	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]

	if self.数据[self.临时id1].数量 == nil then
		self.数据[self.临时id1].数量 = 1
	end

	self.数据[self.临时id1].数量 = self.数据[self.临时id1].数量 - 1

	if self.数据[self.临时id1].数量 <= 0 then
		self.数据[self.临时id1] = nil
		玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil
	end

	发送数据(玩家数据[id].连接id, 3006, "66")
 end
function 道具处理类:摄妖香处理(id, 格子, 类型)----------------
	local 数据 = {类型 = 类型,格子 = 格子}
	if 玩家数据[id].角色:取任务id(1) ~= 0 then
		任务数据[玩家数据[id].角色:取任务id(1)].起始 = os.time()
     任务处理类:刷新追踪任务信息(id)
		发送数据(玩家数据[id].连接id, 7, "#y/你使用了摄妖香")
	else
		任务处理类:添加摄妖香(id)
		发送数据(玩家数据[id].连接id, 7, "#y/你使用了摄妖香")
	end
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
	if self.数据[self.临时id1].数量 == nil then
		self.数据[self.临时id1].数量 = 1
	end
	self.数据[self.临时id1].数量 = self.数据[self.临时id1].数量 - 1
	if self.数据[self.临时id1].数量 <= 0 then
		self.数据[self.临时id1] = nil
		玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil
	end
	发送数据(玩家数据[id].连接id, 3006, "66")
 end
function 道具处理类:新玩家礼包处理(id,数据)-----------
 self.临时id1=玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
 if self.数据[self.临时id1]==nil or self.数据[self.临时id1]==0 then
   发送数据(玩家数据[id].连接id,7,"#y/你似乎并没有这样的道具")
    return 0
 elseif self.数据[self.临时id1].绑定id==nil then
    发送数据(玩家数据[id].连接id,7,"#y/该礼包无法使用")
   return 0
 elseif self.数据[self.临时id1].绑定id~=玩家数据[id].角色.数据.id then
  发送数据(玩家数据[id].连接id,7,"#y/该礼包只能由id为"..self.数据[self.临时id1].绑定id.."的玩家使用")
   return 0
 end
  self.礼包等级=self.数据[self.临时id1].等级
 if self.礼包等级>玩家数据[id].角色.数据.等级 then
   发送数据(玩家数据[id].连接id,7,"#y/你当前的等级不足以打开此礼包")
   return 0
 else
    if self.礼包等级==50 then
		if 玩家数据[id].角色:取可用格子数量("包裹") < 6 then
			发送数据(玩家数据[id].连接id, 7, "#y/请先预留出6个道具空间")
			return 0
		elseif #玩家数据[id].召唤兽.数据 >= 8 then
			发送数据(玩家数据[id].连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
			return 0
		else
            玩家数据[id].装备:取100级装备礼包(id, 100)
			玩家数据[id].角色:购买神兽(id,"泡泡",0)
		end
	elseif self.礼包等级==60 then
		if 玩家数据[id].角色:取可用格子数量("包裹") < 3 then
			发送数据(玩家数据[id].连接id, 7, "#y/请先预留出3个道具空间")
			return 0
		else
			-- 发送数据(玩家数据[id].连接id, 7, "#y/你获得了一只泡泡")
			self:给予道具(id,"摄妖香","功能",10)
			-- 发送数据(玩家数据[id].连接id, 7, "#y/你获得了#r/摄妖香*30")
			self:给予道具(id,"飞行符","杂货",99)
			self:给予道具(id,"回天丹","功能",1)
			发送数据(玩家数据[id].连接id, 7, "#y/你获得了#r/飞行符*99 摄妖香*10 回天丹*1")
			--玩家数据[id].角色:添加仙玉(3000000000000,id,"新手礼包")


			--local gh ={"烈焰澜翻","水墨游龙","星光熠熠","双鲤寄情","凌波微步","浩瀚星河"}
			--local gh1=gh[取随机数(1,#gh)]
			--玩家数据[id].角色.数据.光环=gh1
			--发送数据(玩家数据[id].连接id, 7, "#y/你获得了内测奖励#r/"..gh1.."#y/光环（水墨游龙能增加人物10%伤害）")
		end
    elseif self.礼包等级==70 then
          玩家数据[id].角色:添加经验(500000,id,"新手礼包")
          玩家数据[id].角色:添加储备(id,300000,"新手礼包")
    elseif self.礼包等级==80 then
           玩家数据[id].角色:添加经验(1000000,id,"新手礼包")
          玩家数据[id].角色:添加储备(id,500000,"新手礼包")
    elseif self.礼包等级==90 then
          玩家数据[id].角色:添加经验(1500000,id,"新手礼包")
          玩家数据[id].角色:添加储备(id,700000,"新手礼包")
    elseif self.礼包等级==100 then
            玩家数据[id].角色:添加经验(2000000,id,"新手礼包")
           -- 玩家数据[id].角色:添加仙玉(300,id,"新手礼包")
          玩家数据[id].角色:添加储备(id,1000000,"新手礼包")
      end
   end
 self.数据[self.临时id1].等级=self.数据[self.临时id1].等级+10
 if self.数据[self.临时id1].等级>=100 then
   发送数据(玩家数据[id].连接id,7,"#y/你的新玩家礼包消失了")
   self.数据[self.临时id1]=nil
   玩家数据[id].角色.数据.道具[数据.类型][数据.格子]=nil
   end
 发送数据(玩家数据[id].连接id, 3006, "66")
 end
function 道具处理类:存钱处理(id, 参数, 内容)---------------
	self.存入数额 = tonumber(参数)

	if self.存入数额 == nil then
		return 0
	elseif self.存入数额 <= 0 then
		return 0
	elseif 玩家数据[id].角色.数据.道具.货币.银子 < self.存入数额 then
		发送数据(玩家数据[id].连接id, 7, "#y/你没有那么多的现金")

		return 0
	end
	玩家数据[id].角色:添加消费日志("存钱-存入数额:" .. self.存入数额)
	玩家数据[id].角色:添加消费日志("存钱-银子-之前金额:" .. 玩家数据[id].角色.数据.道具.货币.银子)
	玩家数据[id].角色:添加消费日志("存钱-存银-之前金额:" .. 玩家数据[id].角色.数据.道具.货币.存银)
	玩家数据[id].角色.数据.道具.货币.存银 = 玩家数据[id].角色.数据.道具.货币.存银 + self.存入数额
	玩家数据[id].角色.数据.道具.货币.银子 = 玩家数据[id].角色.数据.道具.货币.银子 - self.存入数额
	玩家数据[id].角色:添加消费日志("存钱-银子-存后金额:" .. 玩家数据[id].角色.数据.道具.货币.银子)
	玩家数据[id].角色:添加消费日志("存钱-存银-存后金额:" .. 玩家数据[id].角色.数据.道具.货币.存银)
	发送数据(玩家数据[id].连接id, 7, "#y/你存入了" .. self.存入数额 .. "两银子")
	发送数据(玩家数据[id].连接id, 11011, 玩家数据[id].角色.数据.道具.货币.银子 .. "*-*" .. 玩家数据[id].角色.数据.道具.货币.存银)
 end
function 道具处理类:取钱处理(id, 参数, 内容)-----------------
	self.存入数额 = tonumber(参数)

	if self.存入数额 == nil then
		return 0
	elseif self.存入数额 <= 0 then
		return 0
	elseif 玩家数据[id].角色.数据.道具.货币.存银 < self.存入数额 then
		发送数据(玩家数据[id].连接id, 7, "#y/你没有那么多的存款")

		return 0
	end
	玩家数据[id].角色:添加消费日志("取钱-取出数额:" .. self.存入数额)
	玩家数据[id].角色:添加消费日志("取钱-银子-之前金额:" .. 玩家数据[id].角色.数据.道具.货币.银子)
	玩家数据[id].角色:添加消费日志("取钱-存银-之前金额:" .. 玩家数据[id].角色.数据.道具.货币.存银)
	玩家数据[id].角色.数据.道具.货币.存银 = 玩家数据[id].角色.数据.道具.货币.存银 - self.存入数额
	玩家数据[id].角色.数据.道具.货币.银子 = 玩家数据[id].角色.数据.道具.货币.银子 + self.存入数额
	玩家数据[id].角色:添加消费日志("取钱-银子-取后金额:" .. 玩家数据[id].角色.数据.道具.货币.银子)
	玩家数据[id].角色:添加消费日志("取钱-存银-取后金额:" .. 玩家数据[id].角色.数据.道具.货币.存银)
	发送数据(玩家数据[id].连接id, 7, "#y/你取出了" .. self.存入数额 .. "两银子")
	发送数据(玩家数据[id].连接id, 11012, 玩家数据[id].角色.数据.道具.货币.银子 .. "*-*" .. 玩家数据[id].角色.数据.道具.货币.存银)
 end
function 道具处理类:道具合宠(id,参数,内容)
 local lysj={参数,内容+0}
  if 玩家数据[id].角色.数据.召唤兽数据.参战~=0 then
     发送数据(玩家数据[id].连接id,7,"#y/请先将所有召唤兽设置为休息状态")
     return 0
  elseif 玩家数据[id].角色.数据.道具.包裹[lysj[2]]==nil then
     发送数据(玩家数据[id].连接id,7,"#y/你没有这样的道具")
     return 0
  end

 local djid=玩家数据[id].角色.数据.道具.包裹[lysj[2]]
 if self.数据[djid].名称=="金柳露" then
   if 召唤兽[玩家数据[id].召唤兽.数据[lysj[1]].造型].参战等级>=95 then
         发送数据(玩家数据[id].连接id,7,"#y/金柳露无法对携带等级超过95级的召唤兽使用")
         return 0
     end
     if 玩家数据[id].召唤兽.数据[lysj[1]].内丹.总数 ~= 玩家数据[id].召唤兽.数据[lysj[1]].内丹.可用数量 then
         发送数据(玩家数据[id].连接id,7,"#y/有内丹的宝宝不能使用")
         return 0
     end
	local 临时召唤兽=召唤兽处理类.创建()
	临时召唤兽:创建召唤兽(玩家数据[id].召唤兽.数据[lysj[1]].造型,true,false,1,0)
	玩家数据[id].召唤兽.数据[lysj[1]]=table.loadstring(临时召唤兽:获取指定数据(1))
	临时召唤兽=nil
	self.数据[djid]=nil
	玩家数据[id].角色.数据.道具.包裹[lysj[2]]=nil
	发送数据(玩家数据[id].连接id,7,"#y/洗宠成功")
	self:索要炼妖数据(id)
	发送数据(玩家数据[id].连接id,3006,"66")
	发送数据(玩家数据[id].连接id,3040,"6")
  elseif self.数据[djid].名称=="超级金柳露" then
     local 临时召唤兽=召唤兽处理类.创建()
     临时召唤兽:创建召唤兽(玩家数据[id].召唤兽.数据[lysj[1]].造型,true,true,1,0)
        if 玩家数据[id].召唤兽.数据[lysj[1]].内丹.总数 ~= 玩家数据[id].召唤兽.数据[lysj[1]].内丹.可用数量 then
         发送数据(玩家数据[id].连接id,7,"#y/有内丹的宝宝不能使用")
         return 0
     end
		玩家数据[id].召唤兽.数据[lysj[1]]=table.loadstring(临时召唤兽:获取指定数据(1))
		临时召唤兽=nil
		self.数据[djid]=nil
		玩家数据[id].角色.数据.道具.包裹[lysj[2]]=nil
		发送数据(玩家数据[id].连接id,7,"#y/洗宠成功")
		self:索要炼妖数据(id)
		发送数据(玩家数据[id].连接id,3006,"66")
		发送数据(玩家数据[id].连接id,3040,"6")
  elseif self.数据[djid].名称=="召唤兽内丹" or self.数据[djid].名称=="高级召唤兽内丹"  and self.数据[djid].技能~=nil then
        local ndsj =  玩家数据[id].召唤兽.数据[lysj[1]].内丹
       if #ndsj >= 1 then
            for i=1,#ndsj do
              if ndsj[i].技能 == self.数据[djid].技能 then
                    if ndsj[i].等级 == 5 then
                      发送数据(玩家数据[id].连接id,7,"#y/该内丹已经学满")
                       return 0
                    else
						玩家数据[id].召唤兽.数据[lysj[1]].内丹[i].等级 = 玩家数据[id].召唤兽.数据[lysj[1]].内丹[i].等级 + 1
						发送数据(玩家数据[id].连接id,7,"#y/恭喜你的"..玩家数据[id].召唤兽.数据[lysj[1]].名称.."#r/"..self.数据[djid].技能.."#y/升到第#r/"..玩家数据[id].召唤兽.数据[lysj[1]].内丹[i].等级.."#y/层")
						self.数据[djid]=nil
						玩家数据[id].召唤兽:刷新属性(lysj[1])
						玩家数据[id].角色.数据.道具.包裹[lysj[2]]=nil
						self:索要炼妖数据(id)
						发送数据(玩家数据[id].连接id,3006,"66")
                     end
               elseif ndsj[i+1] == nil then
					if ndsj.可用数量 > 0 then
						玩家数据[id].召唤兽.数据[lysj[1]].内丹[i+1] = {}
						玩家数据[id].召唤兽.数据[lysj[1]].内丹[i+1].技能= self.数据[djid].技能
						玩家数据[id].召唤兽.数据[lysj[1]].内丹[i+1].等级 = 1
						玩家数据[id].召唤兽.数据[lysj[1]].内丹.可用数量=玩家数据[id].召唤兽.数据[lysj[1]].内丹.可用数量 - 1
						发送数据(玩家数据[id].连接id,7,"#y/恭喜你的"..玩家数据[id].召唤兽.数据[lysj[1]].名称.."学会#r/"..self.数据[djid].技能)
						self.数据[djid]=nil
						玩家数据[id].角色.数据.道具.包裹[lysj[2]] =nil
						玩家数据[id].召唤兽:刷新属性(lysj[1])
						self:索要炼妖数据(id)
						发送数据(玩家数据[id].连接id,3006,"66")
					else
						发送数据(玩家数据[id].连接id,7,"#y/召唤兽没有空余的内丹格子")
					end
                end
            end
      else
			玩家数据[id].召唤兽.数据[lysj[1]].内丹[1] = {}
			玩家数据[id].召唤兽.数据[lysj[1]].内丹[1].技能= self.数据[djid].技能
			玩家数据[id].召唤兽.数据[lysj[1]].内丹[1].等级 = 1
			玩家数据[id].召唤兽.数据[lysj[1]].内丹.可用数量=玩家数据[id].召唤兽.数据[lysj[1]].内丹.可用数量 - 1
			self.数据[djid]=nil
			玩家数据[id].角色.数据.道具.包裹[lysj[2]]=nil
			发送数据(玩家数据[id].连接id,7,"#y/恭喜你的"..玩家数据[id].召唤兽.数据[lysj[1]].名称.."学会#r/"..玩家数据[id].召唤兽.数据[lysj[1]].内丹[1].技能)
			玩家数据[id].召唤兽:刷新属性(lysj[1])
			self:索要炼妖数据(id)
			发送数据(玩家数据[id].连接id,3006,"45147")
      end

 elseif self.数据[djid].名称=="圣兽丹" then
	local zxsj =  玩家数据[id].召唤兽.数据[lysj[1]].造型
	if 玩家数据[id].角色:取可用格子数量("包裹") < 1 then
	发送数据(玩家数据[id].连接id, 7, "#y/请先预留出1个道具空间")
	return 0
	end


	if zxsj == "海星" or zxsj == "章鱼" or zxsj == "泡泡" or zxsj == "山贼" or zxsj == "蚌精" or zxsj == "进阶蝴蝶仙子" or zxsj == "进阶黑山老妖" or zxsj == "古代瑞兽" or zxsj == "进阶雷鸟人" or zxsj == "白熊" or
		zxsj == "风伯" or zxsj == "进阶地狱战神" or zxsj == "进阶天兵" or zxsj == "机关兽" or zxsj == "百足将军" or zxsj == "金身罗汉" or zxsj == "进阶巨力神猿" or zxsj == "幽灵" or zxsj == "雾中仙" or zxsj == "进阶灵符女娲"
		or zxsj == "进阶芙蓉仙子" or zxsj == "长眉灵猴" or zxsj == "猪八戒" or zxsj == "机关鸟" or zxsj == "画魂" or zxsj == "进阶百足将军" or zxsj == "进阶炎魔神" or zxsj == "碧海夜叉" or zxsj == "蝎子精" or
		zxsj == "进阶毗舍童子" or zxsj == "进阶噬天虎" or zxsj == "进阶雨师" or zxsj == "巡游天神" or zxsj == "进阶巡游天神" or zxsj == "如意仙子" or zxsj == "进阶如意仙子" or zxsj == "机关人战车" or zxsj == "进阶蝎子精" or
		zxsj == "进阶犀牛将军兽" or zxsj == "进阶凤凰" or zxsj == "进阶机关人" or zxsj == "鼠先锋" or zxsj == "进阶修罗傀儡妖" or zxsj == "持国巡守" or zxsj == "进阶鼠先锋" or zxsj == "百足将军" or zxsj == "真陀护法" or
		zxsj == "星灵仙子" or zxsj == "吸血鬼" or zxsj == "灵符女娲" or zxsj == "进阶阴阳伞" or zxsj == "进阶律法女娲" or zxsj == "进阶鲛人" or zxsj == "进阶蛟龙" or zxsj == "进阶雨师" or zxsj == "进阶碧水夜叉" or
		zxsj == "碧水夜叉" or zxsj == "阴阳伞" or zxsj == "天兵" or zxsj == "进阶天兵" or zxsj == "蚌精" or zxsj == "狂豹人形" or zxsj == "进阶连弩车" or zxsj == "巴蛇" or zxsj == "狂豹兽形" or zxsj == "进阶龙龟" or
		zxsj == "红萼仙子" or zxsj == "炎魔神" or zxsj == "幽萤娃娃" or zxsj == "进阶画魂" or zxsj == "画魂" or zxsj == "鬼将" or zxsj == "进阶古代瑞兽" or zxsj == "进阶大力金刚"   then

      self:新加给予道具(id,zxsj.."饰品")
      self.数据[djid]=nil
      玩家数据[id].角色.数据.道具.包裹[lysj[2]] =nil
      发送数据(玩家数据[id].连接id,7,"#y/炼化成功你的"..玩家数据[id].召唤兽.数据[lysj[1]].名称.."消失的无影无踪,恭喜你获得了#r/"..zxsj.."饰品")
      table.remove(玩家数据[id].召唤兽.数据,lysj[1])
      self:索要炼妖数据(id)
      发送数据(玩家数据[id].连接id,3006,"sxsj")
   	else
   	发送数据(玩家数据[id].连接id,7,"#y/该召唤兽目前没有饰品")
   	return
   end
  elseif self.数据[djid].名称=="吸附石" then
 	if #玩家数据[id].召唤兽.数据[lysj[1]].技能 <=0 then
        	发送数据(玩家数据[id].连接id,7,"#y/该召唤兽目前没有技能无法提取!")
      	return
      else
         if 取随机数() <=30 then
            local sj = 玩家数据[id].召唤兽.数据[lysj[1]].技能[取随机数(1,#玩家数据[id].召唤兽.数据[lysj[1]].技能)]
         	 self:给予道具(id,"点化石","功能",sj)
         	 发送数据(玩家数据[id].连接id,7,"#y/你成功的从召唤兽身上吸取到了#r"..sj.."#y/技能")
         else
            发送数据(玩家数据[id].连接id,7,"#y/召唤兽技能提取失败,你的召唤兽消失的无影无踪")
         end
         if self.数据[djid].数量 == nil then self.数据[djid].数量 = 1 end
          self.数据[djid].数量= self.数据[djid].数量-1
         if self.数据[djid].数量 <1 then
             self.数据[djid]=nil
            玩家数据[id].角色.数据.道具.包裹[lysj[2]] = nil
         end
         table.remove(玩家数据[id].召唤兽.数据,lysj[1])
      玩家数据[id].召唤兽.数据.参战=0
      self:索要炼妖数据(id,类型)
      发送数据(玩家数据[id].连接id,3006,"sxsj")
    end
 elseif self.数据[djid].名称=="魔兽要诀" or self.数据[djid].名称=="高级魔兽要诀" then
      local 临时技能=self.数据[djid].参数
      local 技能格子=0
      self.基础几率=6
      if 玩家数据[id].角色.数据.祈福>0 then
         玩家数据[id].角色.数据.祈福=玩家数据[id].角色.数据.祈福-1
         self.基础几率=self.基础几率+2
         发送数据(玩家数据[id].连接id,7,"#y/你消耗了1次祈福次数，当前可用祈福次数为#r/"..玩家数据[id].角色.数据.祈福.."#y/次")
         end
      if 取随机数()<=self.基础几率 and #玩家数据[id].召唤兽.数据[lysj[1]].技能< 13 then
         技能格子=#玩家数据[id].召唤兽.数据[lysj[1]].技能+1
        else

          技能格子=取随机数(1,#玩家数据[id].召唤兽.数据[lysj[1]].技能)
         end
      local 重复技能=0
      for n=1,#玩家数据[id].召唤兽.数据[lysj[1]].技能 do
       if 玩家数据[id].召唤兽.数据[lysj[1]].技能[n]==临时技能 then 重复技能=n end
        end
     if 重复技能==0 then
       玩家数据[id].召唤兽.数据[lysj[1]].技能[技能格子]=临时技能
       end
      发送数据(玩家数据[id].连接id,7,"#y/你的这只召唤兽学会了新技能#r/"..临时技能)
      self.数据[djid]=nil
      玩家数据[id].角色.数据.道具.包裹[lysj[2]]=nil
      玩家数据[id].召唤兽.数据[lysj[1]].默认法术=nil
      self:索要炼妖数据(id)
      发送数据(玩家数据[id].连接id,3006,"6sxsj6")
      发送数据(玩家数据[id].连接id,3040,"61sxsj")
   end
  end
function 道具处理类:炼妖合宠(id,参数,内容)
 if 玩家数据[id].召唤兽.数据.参战~=0 then
     发送数据(玩家数据[id].连接id,7,"#y/请先将所有召唤兽设置为休息状态")
     return 0
   end
 self.临时炼妖={参数,内容+0}
 if self.临时炼妖==nil then

     发送数据(玩家数据[id].连接id,7,"#y/炼妖数据异常")
     return 0
  elseif 玩家数据[id].召唤兽.数据[self.临时炼妖[1]].神兽 or 玩家数据[id].召唤兽.数据[self.临时炼妖[2]].神兽 then

   发送数据(玩家数据[id].连接id,7,"#y/神兽不允许进行此种操作")
     return 0
  elseif 调试模式==false and (玩家数据[id].召唤兽.数据[self.临时炼妖[1]].等级<30 or 玩家数据[id].召唤兽.数据[self.临时炼妖[2]].等级<30) then
    发送数据(玩家数据[id].连接id,7,"#y/等级小于30级的召唤兽无法进行炼妖操作")
    return 0

   end

 self.禁止名称=""
 for n=1,3 do

   if 玩家数据[id].召唤兽.数据[self.临时炼妖[1]].装备[n]~=nil then

      self.禁止名称=玩家数据[id].召唤兽.数据[self.临时炼妖[1]].名称

     end

   end

  for n=1,3 do

   if 玩家数据[id].召唤兽.数据[self.临时炼妖[2]].装备[n]~=nil then

      self.禁止名称=玩家数据[id].召唤兽.数据[self.临时炼妖[2]].名称

     end

   end

 if self.禁止名称~="" then
   发送数据(玩家数据[id].连接id,7,"#y/请先卸下"..self.禁止名称.."所佩戴的装备")
    return 0

   end

 self.造型1=玩家数据[id].召唤兽.数据[self.临时炼妖[1]].造型
 self.造型2=玩家数据[id].召唤兽.数据[self.临时炼妖[2]].造型
 self.造型3="大海龟"
 self.重置技能=true

 local 临时随机数=取随机数()
 local 临时造型=""
 if  临时随机数<=65 then
     临时造型=self.造型1
	elseif 临时随机数>65 and 临时随机数<=85 then

	  临时造型=self.造型2
	elseif 临时随机数>99 and 临时随机数<=100 then

	  临时造型=self.造型3
	  self.重置技能=false
	end
  if 临时造型==nil then 临时造型="大海龟" end
  local 临时技能={}
  for n=1,#玩家数据[id].召唤兽.数据[self.临时炼妖[1]].技能 do

      临时技能[#临时技能+1]=玩家数据[id].召唤兽.数据[self.临时炼妖[1]].技能[n]

    end
  for n=1,#玩家数据[id].召唤兽.数据[self.临时炼妖[2]].技能 do

     临时技能[#临时技能+1]=玩家数据[id].召唤兽.数据[self.临时炼妖[2]].技能[n]

   end

 local 是否宝宝=false

  if 玩家数据[id].召唤兽.数据[self.临时炼妖[1]].宝宝 and 玩家数据[id].召唤兽.数据[self.临时炼妖[2]].宝宝 then

     是否宝宝=true

     end
 self.临时等级=玩家数据[id].召唤兽.数据[self.临时炼妖[1]].等级+玩家数据[id].召唤兽.数据[self.临时炼妖[2]].等级
 self.临时等级=math.floor(self.临时等级/2*0.75)
 if self.临时等级<0 then self.临时等级=0 end

 self.临时资质数据={[1]={},[2]={}}
 for n, v in pairs(全局变量.资质标识) do

     if 玩家数据[id].召唤兽.数据[self.临时炼妖[1]][全局变量.资质标识[n]]<玩家数据[id].召唤兽.数据[self.临时炼妖[2]][全局变量.资质标识[n]] then
         self.临时资质数据[1][全局变量.资质标识[n]]=玩家数据[id].召唤兽.数据[self.临时炼妖[1]][全局变量.资质标识[n]]
         self.临时资质数据[2][全局变量.资质标识[n]]=玩家数据[id].召唤兽.数据[self.临时炼妖[2]][全局变量.资质标识[n]]

        else

         self.临时资质数据[2][全局变量.资质标识[n]]=玩家数据[id].召唤兽.数据[self.临时炼妖[1]][全局变量.资质标识[n]]
         self.临时资质数据[1][全局变量.资质标识[n]]=玩家数据[id].召唤兽.数据[self.临时炼妖[2]][全局变量.资质标识[n]]


       end



   end

 self.临时成长数据={[1]=玩家数据[id].召唤兽.数据[self.临时炼妖[1]].成长,[2]=玩家数据[id].召唤兽.数据[self.临时炼妖[2]].成长}


 if self.临时成长数据[1]<self.临时成长数据[2] then


    else
     self.临时参数11=self.临时成长数据[1]
     self.临时成长数据[1]=self.临时成长数据[2]
     self.临时成长数据[2]=self.临时参数11
   end

 table.remove(玩家数据[id].召唤兽.数据,self.临时炼妖[2])
 self.临时召唤兽=召唤兽处理类.创建()
 self.临时召唤兽:创建召唤兽(临时造型,是否宝宝,false,1,self.临时等级)
 if self.临时炼妖[1]>#玩家数据[id].召唤兽.数据 then

   self.临时炼妖[1]=#玩家数据[id].召唤兽.数据
   end

 玩家数据[id].召唤兽.数据[self.临时炼妖[1]]=table.loadstring(self.临时召唤兽:获取指定数据(1))

 for n, v in pairs(全局变量.资质标识) do

       玩家数据[id].召唤兽.数据[self.临时炼妖[1]][全局变量.资质标识[n]]=取随机数(self.临时资质数据[1][全局变量.资质标识[n]],self.临时资质数据[2][全局变量.资质标识[n]])

   end

  玩家数据[id].召唤兽.数据[self.临时炼妖[1]].成长=玩家数据[id].召唤兽:保留成长(取随机数(self.临时成长数据[1]*1000,self.临时成长数据[2]*1000)/1000)
 self.临时召唤兽=nil

 if self.重置技能 then

      玩家数据[id].召唤兽.数据[self.临时炼妖[1]].技能={}
      for n=1,#临时技能 do
		-- 取随机数(25,200)<=45+(12-n*1)
         if 取随机数() < 70 and #玩家数据[id].召唤兽.数据[self.临时炼妖[1]].技能 < 18 then
            玩家数据[id].召唤兽.数据[self.临时炼妖[1]].技能[#玩家数据[id].召唤兽.数据[self.临时炼妖[1]].技能+1]=临时技能[n]
           end
         end
   end

 玩家数据[id].召唤兽.数据[self.临时炼妖[1]].默认法术=nil

 发送数据(玩家数据[id].连接id,7,"#y/恭喜你合出了#r/"..临时造型)

 发送数据(玩家数据[id].连接id,3002,玩家数据[id].召唤兽:获取数据())
 发送数据(玩家数据[id].连接id,3040,"6")



 end
function 道具处理类:玉葫灵髓使用(id,数据)-------------------------------------完成---------
 local 临时=玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
 if self.数据[临时]==nil or self.数据[临时]==0 then
   发送数据(玩家数据[id].连接id,7,"#y/你似乎并没有这样的道具")
   return 0
   end
    if 玩家数据[id].召唤兽.数据.参战==0 then
     发送数据(玩家数据[id].连接id,7,"#y/请先将召唤兽设置为参战状态")
     return 0
   end
 if 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].内丹.可用数量 ~= 0 then
   发送数据(玩家数据[id].连接id,7,"#y/你召唤兽内丹还有空余格子无法使用")
   return 0
   end
   if 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].灵性 > 50 and (玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].参战等级 >=45 or 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].神兽) then
      玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].灵性 = 50
      玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].进阶属性.力量 = 0
      玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].进阶属性.耐力 = 0
      玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].进阶属性.敏捷 = 0
      玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].进阶属性.体质 = 0
      玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].进阶属性.魔力 = 0
      玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].最高灵性显示 = true
      玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].特性 ="无"
 发送数据(玩家数据[id].连接id,7,"#y/你的召唤兽"..玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].名称.."服用一个玉葫灵髓后,灵性已回归原始！")
   if self.数据[临时].数量==1 then
    self.数据[临时]=nil
    玩家数据[id].角色.数据.道具[数据.类型][数据.格子]=nil
  else
    self.数据[临时].数量=self.数据[临时].数量 - 1
  end
 self:索要道具(id,数据.类型)

 玩家数据[id].召唤兽:刷新属性(玩家数据[id].召唤兽.数据.参战)
 发送数据(玩家数据[id].连接id,2009,玩家数据[id].召唤兽:获取数据())
  else
       发送数据(玩家数据[id].连接id,7,"#y/你的召唤兽灵性必须大于50才能使用")
       self:索要道具(id,数据.类型)

   end
 end
function 道具处理类:清灵仙露使用(id,数据)-------------------------------------完成---------
 local 临时=玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
 local 临时灵性
 if self.数据[临时]==nil or self.数据[临时]==0 then
   发送数据(玩家数据[id].连接id,7,"#y/你似乎并没有这样的道具")
   return 0
   end
       if 玩家数据[id].召唤兽.数据.参战==0 then
     发送数据(玩家数据[id].连接id,7,"#y/请先将召唤兽设置为参战状态")
     return 0
   end
    if 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].内丹.可用数量 ~= 0 then
   发送数据(玩家数据[id].连接id,7,"#y/你召唤兽内丹还有空余格子无法使用")
   return 0
   end
    if 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].灵性> 100  then
   发送数据(玩家数据[id].连接id,7,"#y/你召唤兽已经无法使用清灵仙露")
   return 0
   end
   if 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].灵性 >= 50 then

      if self.数据[临时].灵气 == 8 then
      if  取随机数() < 10 then
      临时灵性 = 取随机数(9,10)
      else
      临时灵性 = self.数据[临时].灵气
      end
      else
      临时灵性 = self.数据[临时].灵气
      end
      玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].灵性 = 临时灵性+ 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].灵性
      if 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].灵性> 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].最高灵性 then
       玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].潜能 =玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].潜能+((玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].灵性-玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].最高灵性)*2)
       if 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].灵性 < 临时灵性+  玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].最高灵性 then
         玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].最高灵性 = 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].灵性
       else
           玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].最高灵性=临时灵性+  玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].最高灵性
       end
      end
      发送数据(玩家数据[id].连接id,7,"#y/你的召唤兽"..玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].名称.."增加了#r/"..self.数据[临时].灵气.."#y/点灵性")
      if 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].灵性 >= 110  then
      玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].进阶属性.力量 =取随机数(20,100)
      玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].进阶属性.魔力 =取随机数(20,100)
      玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].进阶属性.体质 =取随机数(20,100)
      玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].进阶属性.敏捷 =取随机数(20,100)
      玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].进阶属性.耐力 =取随机数(20,100)
      发送数据(玩家数据[id].连接id,7,"#y/增加进阶属性"..玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].进阶属性.力量.."#y/点")
      发送数据(玩家数据[id].连接id,7,"#y/增加进阶属性"..玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].进阶属性.魔力.."#y/点")
      发送数据(玩家数据[id].连接id,7,"#y/增加进阶属性"..玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].进阶属性.体质.."#y/点")
      发送数据(玩家数据[id].连接id,7,"#y/增加进阶属性"..玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].进阶属性.敏捷.."#y/点")
      发送数据(玩家数据[id].连接id,7,"#y/增加进阶属性"..玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].进阶属性.耐力.."#y/点")
  end
  self.数据[临时].数量  = self.数据[临时].数量  -1
  if self.数据[临时].数量 < 1  then
      self.数据[临时]=nil
		 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]=nil
  end

 self:索要道具(id,数据.类型)
  玩家数据[id].召唤兽:刷新属性(玩家数据[id].召唤兽.数据.参战)
 发送数据(玩家数据[id].连接id,2009,玩家数据[id].召唤兽:获取数据())
  else
       发送数据(玩家数据[id].连接id,7,"#y/你的召唤兽灵性必须达到50以上才能使用")
       self:索要道具(id,数据.类型)

       发送数据(玩家数据[id].连接id,2009,玩家数据[id].召唤兽:获取数据())
   end
 end
function 道具处理类:易经丹使用(id,数据)-------------------------------------完成---------
 local 临时=玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
 if self.数据[临时]==nil or self.数据[临时]==0 then
   发送数据(玩家数据[id].连接id,7,"#y/你似乎并没有这样的道具")
   return 0
   end
 if 玩家数据[id].召唤兽.数据.参战==0 then
     发送数据(玩家数据[id].连接id,7,"#y/请先将召唤兽设置为参战状态")
     return 0
   end
    if 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].内丹.可用数量 ~= 0 then
   发送数据(玩家数据[id].连接id,7,"#y/你召唤兽内丹还有空余格子无法使用")
   return 0
   end
   if 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].灵性 < 50 and  (玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].参战等级 >=45 or 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].神兽) and 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].内丹.可用数量 == 0 then
       玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].灵性 = 10+ 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].灵性
             if 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].灵性>= 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].最高灵性 then
         玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].潜能 =玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].潜能+((玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].灵性-玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].最高灵性)*2)
        玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].最高灵性=10+ 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].最高灵性
      end
       发送数据(玩家数据[id].连接id,7,"#y/你的召唤兽"..玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].名称.."服用一个易经丹后,神清气爽,仙气缭绕,修为增加！")
          if 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].灵性 >= 50 then
            玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].进阶 = true
            发送数据(玩家数据[id].连接id,7,"#y/"..玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].造型.."造型已变为#r/进阶"..玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].造型)
           -- 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].造型 ="进阶"..玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].造型
          end
         if self.数据[临时].数量==1 then
          self.数据[临时]=nil
          玩家数据[id].角色.数据.道具[数据.类型][数据.格子]=nil
        else
          self.数据[临时].数量=self.数据[临时].数量 - 1
        end
       self:索要道具(id,数据.类型)
        玩家数据[id].召唤兽:刷新属性(玩家数据[id].召唤兽.数据.参战)
       发送数据(玩家数据[id].连接id,2009,玩家数据[id].召唤兽:获取数据())
  elseif 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].灵性 > 79  then
      local 随机特性={"复仇","自恋","灵刃","灵法","预知","灵动","瞬击","瞬法","抗法","抗物","阳护","识物","护佑","洞察","弑神","御风","顺势","怒吼","逆境","乖巧","力破","识药","吮魔","争锋","灵断"}
      local a =随机特性[取随机数(1,#随机特性)]
      玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].特性 = a
      玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].特性几率 =取随机数(1,5)
      发送数据(玩家数据[id].连接id,7,"#y/你的召唤兽"..玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].名称.."领悟了#r/"..a)
      if self.数据[临时].数量==1 then
          self.数据[临时]=nil
          玩家数据[id].角色.数据.道具[数据.类型][数据.格子]=nil
        else
          self.数据[临时].数量=self.数据[临时].数量 - 1
        end
       self:索要道具(id,数据.类型)
       发送数据(玩家数据[id].连接id,2009,玩家数据[id].召唤兽:获取数据())
     else
       发送数据(玩家数据[id].连接id,7,"#y/你的召唤兽不符合使用条件")
       self:索要道具(id,数据.类型)

   end
 end
function 道具处理类:清灵净瓶使用(id,数据)-------------------------------------完成---------
 local 临时=玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
 if self.数据[临时]==nil or self.数据[临时]==0 then
   发送数据(玩家数据[id].连接id,7,"#y/你似乎并没有这样的道具")
   return 0
   end
  self.可用格子=玩家数据[id].角色:取可用道具格子("包裹")
 if self.可用格子==0 then
   发送数据(玩家数据[id].连接id,7,"#y/你的包裹已满,无法使用")
   return 0
   end

  if self.数据[临时].数量==1 then
    self.数据[临时]=nil
    玩家数据[id].角色.数据.道具[数据.类型][数据.格子]=nil
  else
    self.数据[临时].数量=self.数据[临时].数量 - 1
  end
    local 随机瓶子={"高级清灵仙露","中级清灵仙露","初级清灵仙露"}
    local a =随机瓶子[取随机数(1,#随机瓶子)]
    self:新加给予道具(id,a)
    self:索要道具(id,数据.类型)
 end
function 道具处理类:新加给予道具(id,名称,等级技能,类型,数量)------------
 self.临时格子=玩家数据[id].角色:取可用道具格子("包裹")
    if self.临时格子==0 then
      发送数据(玩家数据[id].连接id,7,"#y/您当前的包裹空间已满，无法获得新道具")
    else
      self.临时编号=self:取道具编号()
      self.数据[self.临时编号]={}
      玩家数据[id].角色.数据.道具.包裹[self.临时格子]=self.临时编号
      self.数据[self.临时编号]=置物品(名称,等级技能,类型,数量,id)
      if self.数据[self.临时编号].数量 ==nil then
      发送数据(玩家数据[id].连接id,7,"#y/你获得了:#r/"..self.数据[self.临时编号].名称)
      else
      发送数据(玩家数据[id].连接id,7,"#y/你获得了:#r/"..self.数据[self.临时编号].名称..self.数据[self.临时编号].数量.."#y/个")
    end
   end
 end
function 道具处理类:给予道具(id, 名称, 类型, 参数, 数量, 技能)----------------
	if 名称 == "神兜兜" or 名称 == "修炼果"or 名称 == "飞行符" or 名称 == "摄妖香" or 名称 == "避水珠" or 名称 == "龙鳞" or 名称 == "夜光珠" or 名称 == "定魂珠" or 名称 == "金刚石"
		or 名称 == "花豆" or 名称 == "彩果" or 名称 == "玄武石"or 名称 == "朱雀石" or 名称 == "白虎石" or 名称 == "青龙石" or 名称 == "金银宝盒" or 名称 == "天眼通符" or 名称 == "法宝碎片"
		then
		for n = 1, 20 do
			if 参数 >= 1 and 玩家数据[id].角色.数据.道具.包裹[n] ~= nil and 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[n]] ~= nil and 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[n]] ~= 0 and 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[n]].名称 == 名称 then
				if 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[n]].数量 + 参数 <= 99 then
					玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[n]].数量 = 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[n]].数量 + 参数
					return 0
				elseif 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[n]].数量 < 99 then
					self.误差数量 = 99 - 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[n]].数量
					玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[n]].数量 = 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[n]].数量 + self.误差数量
					参数 = 参数 - self.误差数量
				end
			end
		end
	end

	self.临时格子 = 玩家数据[id].角色:取可用道具格子("包裹")

	if self.临时格子 == 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/您当前的包裹空间已满，无法获得新道具")
	else
		self.临时编号 = self:取道具编号()
		self.数据[self.临时编号] = {
			名称 = 名称,
			类型 = 类型
		}
		玩家数据[id].角色.数据.道具.包裹[self.临时格子] = self.临时编号

		if 类型 == "普通" then
			if 名称 == "魔兽要诀" or 名称 == "高级魔兽要诀" then
				self.数据[self.临时编号].参数 = 参数
			elseif 名称 == "花豆" or 名称 == "神兜兜" or 名称 == "彩果" or 名称 == "法宝碎片" then
				self.数据[self.临时编号].数量 = 参数
				if 名称 == "神兜兜" and 参数 == nil then
					self.数据[self.临时编号].数量 = 1
				elseif 名称 == "法宝碎片" and 参数 == nil then
					self.数据[self.临时编号].数量 = 1
				end
			elseif 名称 == "白虎石" or 名称 == "青龙石" or 名称 == "玄武石" or 名称 == "朱雀石" then
				self.数据[self.临时编号].数量 = 参数
            elseif 名称 == "避水珠" or 名称 == "金刚石" or 名称 == "夜光珠" or 名称 == "龙鳞" or 名称 == "定魂珠" then
				self.数据[self.临时编号].数量 = 参数
			elseif 名称 == "珍珠" then
				self.数据[self.临时编号].等级 = 参数
			end
		elseif 类型 == "未激活符石" then
			self.数据[self.临时编号].等级=参数

		elseif 类型 == "打造" then

			if 名称 == "灵饰指南书" or 名称 == "元灵晶石" then
				self.数据[self.临时编号].参数 = 参数[取随机数(1, #参数)] * 10
			else 
				self.数据[self.临时编号].参数 = 取随机数(参数, 数量) * 10
			end

			if 名称 == "制造指南书" then
				self.数据[self.临时编号].制造 = 技能
			elseif 名称 == "灵饰指南书" then
				self.数据[self.临时编号].制造 = 随机灵饰[取随机数(1, #随机灵饰)]
			elseif 名称 == "元灵晶石" then
			elseif 名称 == "珍珠" then
			else
				self.数据[self.临时编号].制造 = 技能
			end
		elseif 类型 == "变身卡" then
			self.数据[self.临时编号].等级 = 参数
			self.数据[self.临时编号].次数 = 参数
			self.数据[self.临时编号].类型 = 变身卡范围[参数][取随机数(1, #变身卡范围[参数])]
		elseif 类型 == "功能" then
			if 名称 == "藏宝图" or 名称 == "高级藏宝图" then
				self.临时地图11 = self.宝图地图[取随机数(1, #self.宝图地图)]
				self.临时坐标11 = 地图处理类.地图数据[self.临时地图11].坐标:取随机格子()
				self.数据[self.临时编号].x = math.floor(self.临时坐标11.x / 20)
				self.数据[self.临时编号].y = math.floor(self.临时坐标11.y / 20)
				self.数据[self.临时编号].地图编号 = self.临时地图
				self.数据[self.临时编号].地图名称 = 地图处理类.地图数据[self.临时地图11].名称
				self.临时数量11 = "一"
				self.临时量词 = "张"
			elseif 名称=="新手玩家礼包"  then
				self.数据[self.临时编号].等级 = 50
            	self.数据[self.临时编号].绑定id=id
           	elseif 名称=="九转金丹"  then
           		self.数据[self.临时编号].品质=取随机数(10,15)*10
			elseif 名称 == "修炼果" or 名称 == "天眼通符" or 名称 == "摄妖香" then
				self.数据[self.临时编号].数量 = 参数
			elseif 名称 == "无名秘籍" then
				self.数据[self.临时编号].等级 = 参数
				self.数据[self.临时编号].部位 = 幻化部位[取随机数(1, #幻化部位)]
			elseif 名称 == "飞行卷" or 名称 == "如意棒" then
				self.数据[self.临时编号].起始 = os.time()
			elseif 名称 == "月华露" or 名称 == "人参果" then
				self.数据[self.临时编号].参数 = 参数
			elseif 名称 == "元宵" then
				self.数据[self.临时编号].参数 = 全局变量.资质标识[取随机数(1, #全局变量.资质标识)]
			elseif 名称 == "上古锻造图策" then
				self.数据[self.临时编号].参数 = 数量
				self.数据[self.临时编号].等级 = 参数
			elseif 名称 == "小铲子" then
				self.数据[self.临时编号].x = 0
				self.数据[self.临时编号].y = 0
			elseif 名称 == "点化石" then
				 self.数据[self.临时编号].技能 = 参数
			elseif 名称 == "鬼谷子" then
				self.数据[self.临时编号].参数 = 阵法名称[取随机数(1, #阵法名称)]
			end
		elseif 类型 == "宝石" then
			self.数据[self.临时编号].参数 = 参数
	    elseif 类型 == "杂货" then
			if 名称 == "飞行符" then
				self.数据[self.临时编号].数量 = 参数
			end
		elseif 名称 == "吸附石" then
			self.数据[self.临时编号].数量 = 参数
		end
		if self.数据[self.临时编号].数量 ==nil then
		发送数据(玩家数据[id].连接id,7,"#y/你获得了:#r"..self.数据[self.临时编号].名称)
		else
		发送数据(玩家数据[id].连接id,7,"#y/你获得了:#r"..self.数据[self.临时编号].名称..self.数据[self.临时编号].数量.."#y/个")
		end
	end
 end


function 道具处理类:级120礼包处理(id,数据)-----------
 self.临时id1=玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
 if self.数据[self.临时id1]==nil or self.数据[self.临时id1]==0 then
   发送数据(玩家数据[id].连接id,7,"#y/你似乎并没有这样的道具")
    return 0
 elseif self.数据[self.临时id1].名称~="120级玩家礼包" then
    发送数据(玩家数据[id].连接id,7,"#y/错误的封包代码，发生超过5次将直接封IP")
   return 0
 elseif 玩家数据[id].角色:取可用格子数量("包裹") < 6 then
		发送数据(玩家数据[id].连接id, 7, "#y/请先预留出6个道具空间")
	return 0
 else
   玩家数据[id].装备:取120级装备礼包(id)
   发送数据(玩家数据[id].连接id,7,"#y/你的新玩家礼包消失了")
   self.数据[self.临时id1]=nil
   玩家数据[id].角色.数据.道具[数据.类型][数据.格子]=nil
   发送数据(玩家数据[id].连接id, 3006, "66")
   end
 end

function 道具处理类:级150礼包处理(id,数据)-----------
 self.临时id1=玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
 if self.数据[self.临时id1]==nil or self.数据[self.临时id1]==0 then
   发送数据(玩家数据[id].连接id,7,"#y/你似乎并没有这样的道具")
    return 0
 elseif self.数据[self.临时id1].名称~="150级玩家礼包" then
    发送数据(玩家数据[id].连接id,7,"#y/错误的封包代码，发生超过5次将直接封IP")
   return 0
 elseif 玩家数据[id].角色:取可用格子数量("包裹") < 6 then
		发送数据(玩家数据[id].连接id, 7, "#y/请先预留出6个道具空间")
	return 0
 else
   玩家数据[id].装备:取150级装备礼包(id)
   self.数据[self.临时id1]=nil
   玩家数据[id].角色.数据.道具[数据.类型][数据.格子]=nil
   发送数据(玩家数据[id].连接id, 3006, "66")
   end
 end
 function 道具处理类:级160礼包处理(id,数据)-----------
 self.临时id1=玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
 if self.数据[self.临时id1]==nil or self.数据[self.临时id1]==0 then
   发送数据(玩家数据[id].连接id,7,"#y/你似乎并没有这样的道具")
    return 0
 elseif self.数据[self.临时id1].名称~="160级玩家礼包" then
    发送数据(玩家数据[id].连接id,7,"#y/错误的封包代码，发生超过5次将直接封IP")
   return 0
 elseif 玩家数据[id].角色:取可用格子数量("包裹") < 6 then
		发送数据(玩家数据[id].连接id, 7, "#y/请先预留出6个道具空间")
	return 0
 else
   玩家数据[id].装备:取160级装备礼包(id)
   self.数据[self.临时id1]=nil
   玩家数据[id].角色.数据.道具[数据.类型][数据.格子]=nil
   发送数据(玩家数据[id].连接id, 3006, "66")
   end
 end
function 道具处理类:银币礼包处理(id,数据)-----------
 self.临时id1=玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
 if self.数据[self.临时id1]==nil or self.数据[self.临时id1]==0 then
   发送数据(玩家数据[id].连接id,7,"#y/你似乎并没有这样的道具")
    return 0
 elseif self.数据[self.临时id1].名称~="银币礼包" then
    发送数据(玩家数据[id].连接id,7,"#y/错误的封包代码，发生超过5次将直接封IP")
   return 0
 else
   玩家数据[id].角色:添加银子(id,200000000,"银币礼包")
   self.数据[self.临时id1]=nil
   玩家数据[id].角色.数据.道具[数据.类型][数据.格子]=nil
   发送数据(玩家数据[id].连接id, 3006, "66")
   end
 end
function 道具处理类:储备礼包处理(id,数据)-----------
 self.临时id1=玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
 if self.数据[self.临时id1]==nil or self.数据[self.临时id1]==0 then
   发送数据(玩家数据[id].连接id,7,"#y/你似乎并没有这样的道具")
    return 0
 elseif self.数据[self.临时id1].名称~="储备礼包" then
    发送数据(玩家数据[id].连接id,7,"#y/错误的封包代码，发生超过5次将直接封IP")
   return 0
 else
   玩家数据[id].角色:添加储备(id,400000000,"储备礼包")
   self.数据[self.临时id1]=nil
   玩家数据[id].角色.数据.道具[数据.类型][数据.格子]=nil
   发送数据(玩家数据[id].连接id, 3006, "66")
   end
 end
function 道具处理类:元30礼包处理(id,数据)-----------

self.临时id1=玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
 if self.数据[self.临时id1]==nil or self.数据[self.临时id1]==0 then
   发送数据(玩家数据[id].连接id,7,"#y/你似乎并没有这样的道具")
    return 0
 --elseif self.数据[self.临时id1].名称~="储备礼包" then
  --  发送数据(玩家数据[id].连接id,7,"#y/错误的封包代码，发生超过5次将直接封IP")
  -- return 0
 else
   玩家数据[id].角色:添加经验(100000000,id,"新手礼包")
   玩家数据[id].角色:添加储备(id,400000000,"储备礼包")
   玩家数据[id].角色:添加银子(id,100000000,"银币礼包")
  -- 玩家数据[id].装备:取100级装备礼包(id, 100)
		--	玩家数据[id].角色:购买神兽(id,"进阶混沌兽",0)
			--发送数据(玩家数据[id].连接id, 7, "#y/你获得了一只古代瑞兽")
			--self:给予道具(id,"天赋阵","鬼谷子","功能",1)
			--发送数据(玩家数据[id].连接id, 7, "#y/你获得了#r/天覆阵*1")
			self:给予道具(id,"花豆","普通",30)
			发送数据(玩家数据[id].连接id, 7, "#y/你获得了#r/花豆*30")
			self:给予道具(id,"彩果","普通",30)
			发送数据(玩家数据[id].连接id, 7, "#y/你获得了#r/彩果*30")
	玩家数据[id].角色:添加仙玉(50000,id,"新手礼包")
   self.数据[self.临时id1]=nil
   玩家数据[id].角色.数据.道具[数据.类型][数据.格子]=nil
   发送数据(玩家数据[id].连接id, 3006, "66")
   end
 end
function 道具处理类:元88礼包处理(id,数据)-----------
self.临时id1=玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
 if self.数据[self.临时id1]==nil or self.数据[self.临时id1]==0 then
   发送数据(玩家数据[id].连接id,7,"#y/你似乎并没有这样的道具")
    return 0
 --elseif self.数据[self.临时id1].名称~="储备礼包" then
  --  发送数据(玩家数据[id].连接id,7,"#y/错误的封包代码，发生超过5次将直接封IP")
  -- return 0
 else
   玩家数据[id].角色:添加经验(100000000,id,"新手礼包")
   玩家数据[id].角色:添加储备(id,200000000,"储备礼包")
   玩家数据[id].角色:添加银子(id,300000000,"银币礼包")
   发送数据(玩家数据[id].连接id, 7, "#y/自行购买阵法书钱已发")
 --  玩家数据[id].装备:取100级装备礼包(id, 100)
			--玩家数据[id].角色:购买神兽(id,"进阶混沌兽",0)
			--发送数据(玩家数据[id].连接id, 7, "#y/你获得了一只古代瑞兽")
			--self:给予道具(id,"天赋阵","鬼谷子","功能",1)
			--发送数据(玩家数据[id].连接id, 7, "#y/你获得了#r/天覆阵*1")
			self:给予道具(id,"花豆","普通",30)
			发送数据(玩家数据[id].连接id, 7, "#y/你获得了#r/花豆*30")
			self:给予道具(id,"彩果","普通",30)
			发送数据(玩家数据[id].连接id, 7, "#y/你获得了#r/彩果*30")
	玩家数据[id].角色:添加仙玉(100000,id,"新手礼包")
   self.数据[self.临时id1]=nil
   玩家数据[id].角色.数据.道具[数据.类型][数据.格子]=nil
   发送数据(玩家数据[id].连接id, 3006, "66")
   end
 end
function 道具处理类:元188礼包处理(id,数据)-----------
self.临时id1=玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
 if self.数据[self.临时id1]==nil or self.数据[self.临时id1]==0 then
   发送数据(玩家数据[id].连接id,7,"#y/你似乎并没有这样的道具")
    return 0
 --elseif self.数据[self.临时id1].名称~="储备礼包" then
  --  发送数据(玩家数据[id].连接id,7,"#y/错误的封包代码，发生超过5次将直接封IP")
  -- return 0
 else
   玩家数据[id].角色:添加经验(100000000,id,"新手礼包")
  -- 玩家数据[id].角色:添加储备(id,400000000,"储备礼包")
   玩家数据[id].角色:添加银子(id,600000000,"银币礼包")
      发送数据(玩家数据[id].连接id, 7, "#y/自行购买阵法书钱已发")
   玩家数据[id].装备:取120级装备礼包(id, 120)
			玩家数据[id].角色:购买神兽(id,"进阶混沌兽",0)
			发送数据(玩家数据[id].连接id, 7, "#y/你获得了一只进阶混沌兽")
			--self:给予道具(id,"天赋阵","鬼谷子","功能",1)
			--发送数据(玩家数据[id].连接id, 7, "#y/你获得了#r/天覆阵*1")
			self:给予道具(id,"花豆","普通",30)
			发送数据(玩家数据[id].连接id, 7, "#y/你获得了#r/花豆*30")
			self:给予道具(id,"彩果","普通",30)
			发送数据(玩家数据[id].连接id, 7, "#y/你获得了#r/彩果*30")
	玩家数据[id].角色:添加仙玉(200000,id,"新手礼包")
   self.数据[self.临时id1]=nil
   玩家数据[id].角色.数据.道具[数据.类型][数据.格子]=nil
   发送数据(玩家数据[id].连接id, 3006, "66")
   end
 end
 function 道具处理类:星辉石礼包处理(id,数据)-----------
self.临时id1=玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
 if self.数据[self.临时id1]==nil or self.数据[self.临时id1]==0 then
   发送数据(玩家数据[id].连接id,7,"#y/你似乎并没有这样的道具")
    return 0
 --elseif self.数据[self.临时id1].名称~="储备礼包" then
  --  发送数据(玩家数据[id].连接id,7,"#y/错误的封包代码，发生超过5次将直接封IP")
  -- return 0
 else
   self:给予道具(id,"星辉石","宝石",1)
   self:给予道具(id,"星辉石","宝石",2)
   self:给予道具(id,"星辉石","宝石",3)
   self:给予道具(id,"星辉石","宝石",4)
   self:给予道具(id,"星辉石","宝石",5)
   self:给予道具(id,"星辉石","宝石",6)
   self:给予道具(id,"星辉石","宝石",7)
   self:给予道具(id,"星辉石","宝石",8)
   self:给予道具(id,"星辉石","宝石",9)
   self:给予道具(id,"星辉石","宝石",10)
   self.数据[self.临时id1]=nil
   玩家数据[id].角色.数据.道具[数据.类型][数据.格子]=nil
   发送数据(玩家数据[id].连接id, 3006, "66")
   end
 end
function 道具处理类:太阳石礼包处理(id,数据)-----------
self.临时id1=玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
 if self.数据[self.临时id1]==nil or self.数据[self.临时id1]==0 then
   发送数据(玩家数据[id].连接id,7,"#y/你似乎并没有这样的道具")
    return 0
 else
   self:给予道具(id,"太阳石","宝石",1)
   self:给予道具(id,"太阳石","宝石",2)
   self:给予道具(id,"太阳石","宝石",3)
   self:给予道具(id,"太阳石","宝石",4)
   self:给予道具(id,"太阳石","宝石",5)
   self:给予道具(id,"太阳石","宝石",6)
   self:给予道具(id,"太阳石","宝石",7)
   self:给予道具(id,"太阳石","宝石",8)
   self:给予道具(id,"太阳石","宝石",9)
   self:给予道具(id,"太阳石","宝石",10)
   self.数据[self.临时id1]=nil
   玩家数据[id].角色.数据.道具[数据.类型][数据.格子]=nil
   发送数据(玩家数据[id].连接id, 3006, "66")
   end
 end

function 道具处理类:月亮石礼包处理(id,数据)-----------
self.临时id1=玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
 if self.数据[self.临时id1]==nil or self.数据[self.临时id1]==0 then
   发送数据(玩家数据[id].连接id,7,"#y/你似乎并没有这样的道具")
    return 0
 else
   self:给予道具(id,"月亮石","宝石",1)
   self:给予道具(id,"月亮石","宝石",2)
   self:给予道具(id,"月亮石","宝石",3)
   self:给予道具(id,"月亮石","宝石",4)
   self:给予道具(id,"月亮石","宝石",5)
   self:给予道具(id,"月亮石","宝石",6)
   self:给予道具(id,"月亮石","宝石",7)
   self:给予道具(id,"月亮石","宝石",8)
   self:给予道具(id,"月亮石","宝石",9)
   self:给予道具(id,"月亮石","宝石",10)
   self.数据[self.临时id1]=nil
   玩家数据[id].角色.数据.道具[数据.类型][数据.格子]=nil
   发送数据(玩家数据[id].连接id, 3006, "66")
   end
 end

function 道具处理类:舍利子礼包处理(id,数据)-----------
self.临时id1=玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
 if self.数据[self.临时id1]==nil or self.数据[self.临时id1]==0 then
   发送数据(玩家数据[id].连接id,7,"#y/你似乎并没有这样的道具")
    return 0
 else
   self:给予道具(id,"舍利子","宝石",1)
   self:给予道具(id,"舍利子","宝石",2)
   self:给予道具(id,"舍利子","宝石",3)
   self:给予道具(id,"舍利子","宝石",4)
   self:给予道具(id,"舍利子","宝石",5)
   self:给予道具(id,"舍利子","宝石",6)
   self:给予道具(id,"舍利子","宝石",7)
   self:给予道具(id,"舍利子","宝石",8)
   self:给予道具(id,"舍利子","宝石",9)
   self:给予道具(id,"舍利子","宝石",10)
   self.数据[self.临时id1]=nil
   玩家数据[id].角色.数据.道具[数据.类型][数据.格子]=nil
   发送数据(玩家数据[id].连接id, 3006, "66")
   end
 end
function 道具处理类:黑宝石礼包处理(id,数据)-----------
self.临时id1=玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
 if self.数据[self.临时id1]==nil or self.数据[self.临时id1]==0 then
   发送数据(玩家数据[id].连接id,7,"#y/你似乎并没有这样的道具")
    return 0
 else
   self:给予道具(id,"黑宝石","宝石",1)
   self:给予道具(id,"黑宝石","宝石",2)
   self:给予道具(id,"黑宝石","宝石",3)
   self:给予道具(id,"黑宝石","宝石",4)
   self:给予道具(id,"黑宝石","宝石",5)
   self:给予道具(id,"黑宝石","宝石",6)
   self:给予道具(id,"黑宝石","宝石",7)
   self:给予道具(id,"黑宝石","宝石",8)
   self:给予道具(id,"黑宝石","宝石",9)
   self:给予道具(id,"黑宝石","宝石",10)
   self.数据[self.临时id1]=nil
   玩家数据[id].角色.数据.道具[数据.类型][数据.格子]=nil
   发送数据(玩家数据[id].连接id, 3006, "66")
   end
 end

function 道具处理类:光芒石礼包处理(id,数据)-----------
self.临时id1=玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
 if self.数据[self.临时id1]==nil or self.数据[self.临时id1]==0 then
   发送数据(玩家数据[id].连接id,7,"#y/你似乎并没有这样的道具")
    return 0
 else
   self:给予道具(id,"光芒石","宝石",1)
   self:给予道具(id,"光芒石","宝石",2)
   self:给予道具(id,"光芒石","宝石",3)
   self:给予道具(id,"光芒石","宝石",4)
   self:给予道具(id,"光芒石","宝石",5)
   self:给予道具(id,"光芒石","宝石",6)
   self:给予道具(id,"光芒石","宝石",7)
   self:给予道具(id,"光芒石","宝石",8)
   self:给予道具(id,"光芒石","宝石",9)
   self:给予道具(id,"光芒石","宝石",10)
   self.数据[self.临时id1]=nil
   玩家数据[id].角色.数据.道具[数据.类型][数据.格子]=nil
   发送数据(玩家数据[id].连接id, 3006, "66")
   end
 end


function 道具处理类:红玛瑙礼包处理(id,数据)-----------
self.临时id1=玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
 if self.数据[self.临时id1]==nil or self.数据[self.临时id1]==0 then
   发送数据(玩家数据[id].连接id,7,"#y/你似乎并没有这样的道具")
    return 0
 else
   self:给予道具(id,"红玛瑙","宝石",1)
   self:给予道具(id,"红玛瑙","宝石",2)
   self:给予道具(id,"红玛瑙","宝石",3)
   self:给予道具(id,"红玛瑙","宝石",4)
   self:给予道具(id,"红玛瑙","宝石",5)
   self:给予道具(id,"红玛瑙","宝石",6)
   self:给予道具(id,"红玛瑙","宝石",7)
   self:给予道具(id,"红玛瑙","宝石",8)
   self:给予道具(id,"红玛瑙","宝石",9)
   self:给予道具(id,"红玛瑙","宝石",10)
   self.数据[self.临时id1]=nil
   玩家数据[id].角色.数据.道具[数据.类型][数据.格子]=nil
   发送数据(玩家数据[id].连接id, 3006, "66")
   end
 end





 function 道具处理类:图册礼包处理(id,数据)-----------
self.临时id1=玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
 if self.数据[self.临时id1]==nil or self.数据[self.临时id1]==0 then
   发送数据(玩家数据[id].连接id,7,"#y/你似乎并没有这样的道具")
    return 0
 --elseif self.数据[self.临时id1].名称~="储备礼包" then
  --  发送数据(玩家数据[id].连接id,7,"#y/错误的封包代码，发生超过5次将直接封IP")
  -- return 0
 else
   --self:给予道具(id,"星辉石","宝石",1)
   --self:给予道具(id,"星辉石","宝石",2)
   --self:给予道具(id,"星辉石","宝石",3)
   --self:给予道具(id,"星辉石","宝石",4)
   --self:给予道具(id,"星辉石","宝石",5)
   --self:给予道具(id,"星辉石","宝石",6)
   --self:给予道具(id,"星辉石","宝石",7)
   --self:给予道具(id,"星辉石","宝石",8)
   --self:给予道具(id,"星辉石","宝石",9)
   --self:给予道具(id,"星辉石","宝石",10)
   --self:给予道具(id,"星辉石","宝石",11)
   --self:给予道具(id,"星辉石","宝石",12)
   --self:给予道具(id,"星辉石","宝石",13)
   --self:给予道具(id,"星辉石","宝石",14)
   --self:给予道具(id,"星辉石","宝石",15)
    self.生成等级 = 取随机数(10, 14) * 10
   玩家数据[id].道具:给予道具(id, "上古锻造图策", "功能", self.生成等级, 取上古锻造图策())
   发送数据(玩家数据[id].连接id, 7, "#y/你获得了上古锻造图策")
    self.生成等级 = 取随机数(10, 14) * 10
   玩家数据[id].道具:给予道具(id, "上古锻造图策", "功能", self.生成等级, 取上古锻造图策())
   发送数据(玩家数据[id].连接id, 7, "#y/你获得了上古锻造图策")
    self.生成等级 = 取随机数(10, 14) * 10
   玩家数据[id].道具:给予道具(id, "上古锻造图策", "功能", self.生成等级, 取上古锻造图策())
   发送数据(玩家数据[id].连接id, 7, "#y/你获得了上古锻造图策")
    self.生成等级 = 取随机数(10, 14) * 10
   玩家数据[id].道具:给予道具(id, "上古锻造图策", "功能", self.生成等级, 取上古锻造图策())
   发送数据(玩家数据[id].连接id, 7, "#y/你获得了上古锻造图策")
    self.生成等级 = 取随机数(10, 14) * 10
   玩家数据[id].道具:给予道具(id, "上古锻造图策", "功能", self.生成等级, 取上古锻造图策())
   发送数据(玩家数据[id].连接id, 7, "#y/你获得了上古锻造图策")
    self.生成等级 = 取随机数(10, 14) * 10
   玩家数据[id].道具:给予道具(id, "上古锻造图策", "功能", self.生成等级, 取上古锻造图策())
   发送数据(玩家数据[id].连接id, 7, "#y/你获得了上古锻造图策")
    self.生成等级 = 取随机数(10, 14) * 10
   玩家数据[id].道具:给予道具(id, "上古锻造图策", "功能", self.生成等级, 取上古锻造图策())
   发送数据(玩家数据[id].连接id, 7, "#y/你获得了上古锻造图策")
    self.生成等级 = 取随机数(10, 14) * 10
   玩家数据[id].道具:给予道具(id, "上古锻造图策", "功能", self.生成等级, 取上古锻造图策())
   发送数据(玩家数据[id].连接id, 7, "#y/你获得了上古锻造图策")
    self.生成等级 = 取随机数(10, 14) * 10
   玩家数据[id].道具:给予道具(id, "上古锻造图策", "功能", self.生成等级, 取上古锻造图策())
   发送数据(玩家数据[id].连接id, 7, "#y/你获得了上古锻造图策")
    self.生成等级 = 取随机数(10, 14) * 10
   玩家数据[id].道具:给予道具(id, "上古锻造图策", "功能", self.生成等级, 取上古锻造图策())
   发送数据(玩家数据[id].连接id, 7, "#y/你获得了上古锻造图策")
    self.生成等级 = 取随机数(10, 14) * 10
   玩家数据[id].道具:给予道具(id, "上古锻造图策", "功能", self.生成等级, 取上古锻造图策())
   发送数据(玩家数据[id].连接id, 7, "#y/你获得了上古锻造图策")
   self.数据[self.临时id1]=nil
   玩家数据[id].角色.数据.道具[数据.类型][数据.格子]=nil
   发送数据(玩家数据[id].连接id, 3006, "66")
   end
 end

function 道具处理类:召唤兽饰品处理(id,数据)
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
	if self.数据[self.临时id1] == nil or self.数据[self.临时id1] == 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/你似乎并没有这样的道具")
		return 0
	elseif 玩家数据[id].召唤兽.数据.参战 == 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/请先将要使用道具的召唤兽设置为参战状态")
		return 0
	elseif	玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].饰品 then
		发送数据(玩家数据[id].连接id, 7, "#y/这个召唤兽已经拥有饰品")
		return 0
	elseif self.数据[self.临时id1].名称 == '圣兽丹' or 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].造型.."饰品" == self.数据[self.临时id1].名称 then
		玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].饰品 =true
		玩家数据[id].召唤兽:刷新属性(玩家数据[id].召唤兽.数据.参战)
		self.数据[self.临时id1] = nil
		玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil
		发送数据(玩家数据[id].连接id, 7, "#y/贡献你的#r/"..玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].名称.."#y/穿戴饰品成功!")
		发送数据(玩家数据[id].连接id, 3006, "66")
	else
		发送数据(玩家数据[id].连接id, 7, "#y/请将对应的召唤兽选择参战模式!")
	end
end
function 道具处理类:九转金丹处理(id, 数据)
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]

	if self.数据[self.临时id1] == nil or self.数据[self.临时id1] == 0 or self.数据[self.临时id1].名称 ~= "九转金丹" then
		发送数据(玩家数据[id].连接id, 7, "#y/你似乎并没有这样的道具")
		return 0
	end

	if 玩家数据[id].角色.数据.人物修炼[玩家数据[id].角色.数据.人物修炼.当前].上限 <= 玩家数据[id].角色.数据.人物修炼[玩家数据[id].角色.数据.人物修炼.当前].等级 then
		发送数据(玩家数据[id].连接id, 7, "#y/你当前的这项修炼等级已达上限，无法再服用九转金丹")

		return 0
	end
	玩家数据[id].角色:添加人物修炼经验(玩家数据[id].道具.数据[self.临时id1].品质)

		玩家数据[id].道具.数据[self.临时id1] = 0
		玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil



	发送数据(玩家数据[id].连接id, 3006, "66")
end
function 道具处理类:月华露处理(id, 格子, 类型)
	local 数据 = {
		格子 = 格子,
		类型 = 类型
	}
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
	if self.数据[self.临时id1] == nil or self.数据[self.临时id1] == 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/你似乎并没有这样的道具")
		return 0
	elseif 玩家数据[id].召唤兽.数据.参战 == 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/请先将要使用道具的召唤兽设置为参战状态")
		return 0
	end

	if 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].等级 >= 30 then
		self.添加经验 = self.数据[self.临时id1].参数 * 2 * 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].等级 + 100

		玩家数据[id].召唤兽:添加经验(self.添加经验, id, 玩家数据[id].召唤兽.数据.参战, 10)
	else
		玩家数据[id].召唤兽:提级处理1(玩家数据[id].召唤兽.数据.参战, id, 30)
	end

	self.数据[self.临时id1] = nil
	玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil

	发送数据(玩家数据[id].连接id, 3006, "66")
end
function 道具处理类:设置内政(id, 类型)
	self.帮派编号 = 玩家数据[id].角色.数据.帮派

	if 取帮派踢人权限(id, 玩家数据[id].角色.数据.帮派) == false then
		发送数据(玩家数据[id].连接id, 7, "#y/你没有此种操作的权限")

		return 0
	elseif 帮派数据[self.帮派编号].当前内政.名称 ~= "无" then
		发送数据(玩家数据[id].连接id, 7, "#y/帮派不允许同时进行两种内政")

		return 0
	end

	if 类型 == "聚义厅" then
		self.满足数量 = 0
		self.查找类型 = {"书院","仓库","金库","厢房","兽室"}

		for n, v in pairs(self.查找类型) do
			if 帮派数据[self.帮派编号][self.查找类型[n]] >= 帮派数据[self.帮派编号].规模 * 4 then
				self.满足数量 = self.满足数量 + 1
			end
		end

		if self.满足数量 < 2 then
			发送数据(玩家数据[id].连接id, 7, "#y/升级聚义厅需要其它两个建筑等级不低于" .. 帮派数据[self.帮派编号].规模 * 4 .. "级")

			return 0
		end
	elseif 帮派数据[self.帮派编号][类型] >= 帮派数据[self.帮派编号].规模 * 4 then
		发送数据(玩家数据[id].连接id, 7, "#y/该建筑等级已达上限！")

		return 0
	end

	if 类型 == "金库" then
		self.所需进度 = 500
		self.损耗资金 = 5000000
		self.损耗繁荣 = 300
		self.损耗人气 = 200
	elseif 类型 == "书院" then
		self.所需进度 = 500
		self.损耗资金 = 5000000
		self.损耗繁荣 = 300
		self.损耗人气 = 200
	elseif 类型 == "聚义厅" then
		self.所需进度 = 帮派数据[self.帮派编号].规模 * 500 + 500
		self.损耗资金 = 帮派数据[self.帮派编号].规模 * 5000000 + 5000000
		self.损耗繁荣 = 500
		self.损耗人气 = 500
	else
		if 类型 == "仓库" or 类型 == "厢房" then
			self.所需进度 = 帮派数据[self.帮派编号].规模*5
		else
			self.所需进度 = 200
		end
		self.损耗资金 = 3000000
		self.损耗繁荣 = 100
		self.损耗人气 = 100
	end

	if 帮派数据[self.帮派编号].人气 < self.损耗人气 then
		发送数据(玩家数据[id].连接id, 7, "#y/你帮派的人气不够")

		return 0
	elseif 帮派数据[self.帮派编号].繁荣 < self.损耗繁荣 then
		发送数据(玩家数据[id].连接id, 7, "#y/你帮派的繁荣不够")

		return 0
	elseif 帮派数据[self.帮派编号].资金 < self.损耗资金 + 帮派数据[self.帮派编号].规模 * 3000000 then
		发送数据(玩家数据[id].连接id, 7, "#y/你帮派的资金不够")

		return 0
	end

	帮派数据[self.帮派编号].人气 = 帮派数据[self.帮派编号].人气 - self.损耗人气
	帮派数据[self.帮派编号].繁荣 = 帮派数据[self.帮派编号].繁荣 - self.损耗繁荣
	帮派数据[self.帮派编号].资金 = 帮派数据[self.帮派编号].资金 - self.损耗资金
	帮派数据[self.帮派编号].当前内政.名称 = 类型
	帮派数据[self.帮派编号].当前内政.要求进度 = self.所需进度
	帮派数据[self.帮派编号].当前内政.总进度 = 0

	发送数据(玩家数据[id].连接id, 7, "#y/设置帮派内政成功！")
	广播帮派消息(玩家数据[id].角色.数据.帮派, "#bp/#r/" .. 玩家数据[id].角色.数据.名称 .. "#y/设置了新的内政建设")
end
function 道具处理类:分解装备(id, 格子)
	local lv  = self.数据[玩家数据[id].角色.数据.道具.包裹[格子]].等级
	if lv < 60 then
		发送数据(玩家数据[id].连接id, 7, "#y/只有60级以上的装备才可以分解")
		return
	end
	self.临时id1 = 玩家数据[id].角色.数据.道具.包裹[格子]

  if lv > 150 then
	-- if lv < 60 then
	-- 		发送数据(玩家数据[id].连接id, 7, "#y/只有60级以上的装备才可以分解")
	-- 	return
  	if self.数据[self.临时id1].类型 =="头盔" then
  		if 取防具性别(self.数据[self.临时id1].名称) == "男" then
  			self:给予道具(id,"头盔·元身","打造",16,16,"头盔")
  		else
  			self:给予道具(id,"冠冕·元身","打造",16,16,"发钗")
  		end
  	elseif self.数据[self.临时id1].类型 =="衣服" then
  	    if 取防具性别(self.数据[self.临时id1].名称) == "男" then
  			self:给予道具(id,"坚甲·元身","打造",16,16,"男衣")
  		else
  			self:给予道具(id,"纱衣·元身","打造",16,16,"女衣")
  		end
    elseif self.数据[self.临时id1].类型 =="项链" then
    	self:给予道具(id,"挂坠·元身","打造",16,16,self.数据[self.临时id1].类型)
    elseif self.数据[self.临时id1].类型 =="鞋子" then
    	self:给予道具(id,"鞋履·元身","打造",16,16,self.数据[self.临时id1].类型)
  	elseif self.数据[self.临时id1].类型 =="腰带" then
  		self:给予道具(id,"束带·元身","打造",16,16,self.数据[self.临时id1].类型)
  	elseif self.数据[self.临时id1].类型 =="武器"  then
        if 取武器类型(self.数据[self.临时id1].名称) == "法杖" then
        	self:给予道具(id,"长杖·元身","打造",16,16,"法杖")
        elseif 取武器类型(self.数据[self.临时id1].名称) == "弓弩" then
        	self:给予道具(id,"弓·元身","打造",16,16,"弓弩")
        elseif 取武器类型(self.数据[self.临时id1].名称) == "飘" then
        	self:给予道具(id,"飘带·元身","打造",16,16,"飘")
        elseif 取武器类型(self.数据[self.临时id1].名称) == "棒" then
        	self:给予道具(id,"魔棒·元身","打造",16,16,"棒")
        elseif 取武器类型(self.数据[self.临时id1].名称) == "环" then
        	self:给予道具(id,"双环·元身","打造",16,16,"环")
        elseif 取武器类型(self.数据[self.临时id1].名称) == "双" then
        	self:给予道具(id,"双剑·元身","打造",16,16,"双")
        elseif 取武器类型(self.数据[self.临时id1].名称) == "鞭" then
        	self:给予道具(id,"长鞭·元身","打造",16,16,"鞭")
        elseif 取武器类型(self.数据[self.临时id1].名称) == "爪" then
        	self:给予道具(id,"爪刺·元身","打造",16,16,"爪")
        elseif 取武器类型(self.数据[self.临时id1].名称) == "巨剑" then
        	self:给予道具(id,"巨剑·元身","打造",16,16,"巨剑")
        elseif 取武器类型(self.数据[self.临时id1].名称) == "伞" then
        	self:给予道具(id,"伞·元身","打造",16,16,"伞")
        elseif 取武器类型(self.数据[self.临时id1].名称) == "灯笼" then
        	self:给予道具(id,"灯笼·元身","打造",16,16,"灯笼")
        elseif 取武器类型(self.数据[self.临时id1].名称) == "宝珠" then
        	self:给予道具(id,"宝珠·元身","打造",16,16,"宝珠")
        elseif 取武器类型(self.数据[self.临时id1].名称) == "扇" then
        	self:给予道具(id,"扇·元身","打造",16,16,"扇")
        elseif 取武器类型(self.数据[self.临时id1].名称) == "刀" then
        	self:给予道具(id,"刀·元身","打造",16,16,"刀")
        elseif 取武器类型(self.数据[self.临时id1].名称) == "剑" then
        	self:给予道具(id,"剑·元身","打造",16,16,"剑")
        elseif 取武器类型(self.数据[self.临时id1].名称) == "斧" then
        	self:给予道具(id,"斧·元身","打造",16,16,"斧")
        elseif 取武器类型(self.数据[self.临时id1].名称) == "锤" then
        	self:给予道具(id,"锤·元身","打造",16,16,"锤")
        elseif 取武器类型(self.数据[self.临时id1].名称) == "枪" then
        	self:给予道具(id,"枪·元身","打造",16,16,"枪" )
        end
  	end
  else
  	-- self:给予道具(id,"吸附石","炼妖", math.floor(lv/40))
  	local stones = { [1] = "白虎石", [2] = "青龙石", [3] = "玄武石", [4] = "朱雀石" }
  	self:给予道具(id,stones[math.floor(1, 4)],"普通", math.floor((lv - 50) / 10))

  end
	if 玩家数据[id].删除内容 ~= nil and 玩家数据[id].删除内容.格子 ~= nil then
		玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[玩家数据[id].删除内容.格子]] = nil  --试试
	玩家数据[id].角色.数据.道具.包裹[玩家数据[id].删除内容.格子] = nil
	end
	玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[格子]] = nil
	玩家数据[id].角色.数据.道具.包裹[格子] = nil
	发送数据(玩家数据[id].连接id, 3006, "66")
end

function 道具处理类:取职务数量(编号, 职务)
	self.找到数量 = 0

	for n, v in pairs(帮派数据[编号].成员名单) do
		if 帮派数据[编号].成员名单[n].职务 == 职务 then
			self.找到数量 = self.找到数量 + 1
		end
	end

	return self.找到数量
end

function 道具处理类:职务变更(id, 职务)
	self.操作id = 玩家数据[id].帮派操作id
	玩家数据[id].帮派操作id = nil

	if self.操作id == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/请先选择一位成员")

		return 0
	end

	self.帮派编号 = 玩家数据[id].角色.数据.帮派

	if 帮派数据[self.帮派编号] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/该帮派不存在")

		return 0
	end

	if id == self.操作id then
		if 帮派数据[self.帮派编号].成员名单[self.操作id].职务 == "帮主" or 帮派数据[self.帮派编号].成员名单[self.操作id].职务 == "帮众" then
			发送数据(玩家数据[id].连接id, 7, "#y/对方无法进行这样的职务变动")

			return 0
		elseif 职务 ~= "帮众" then
			发送数据(玩家数据[id].连接id, 7, "#y/您只能将自己任命为帮众！")

			return 0
		end

		帮派数据[self.帮派编号].成员名单[self.操作id].职务 = "帮众"

		广播帮派消息(self.帮派编号, "#bp/#r/" .. 玩家数据[id].角色.数据.名称 .. "#y/将#r/" .. 帮派数据[self.帮派编号].成员名单[self.操作id].名称 .. "#y/任命为" .. 职务)
		发送数据(玩家数据[self.操作id].连接id, 7, "#y/你在帮中的职务发生了变化")

		return 0
	elseif 职务 == "副帮主" then
		if 帮派数据[self.帮派编号].成员名单[id].职务 ~= "帮主" then
			发送数据(玩家数据[id].连接id, 7, "#y/你没有权限进行此种操作！")

			return 0
		elseif self:取职务数量(self.帮派编号, 职务) >= 1 then
			发送数据(玩家数据[id].连接id, 7, "#y/该担任该职务的人数已达上限")

			return 0
		else
			帮派数据[self.帮派编号].成员名单[self.操作id].职务 = 职务

			广播帮派消息(self.帮派编号, "#bp/#r/" .. 玩家数据[id].角色.数据.名称 .. "#y/将#r/" .. 帮派数据[self.帮派编号].成员名单[self.操作id].名称 .. "#y/任命为" .. 职务)

			if 玩家数据[self.操作id] ~= nil then
				发送数据(玩家数据[self.操作id].连接id, 7, "#y/你在帮中的职务发生了变化")
			end
		end
	elseif 职务 == "左护法" or 职务 == "右护法" then
		if 取帮派踢人权限(id, self.帮派编号) == false then
			发送数据(玩家数据[id].连接id, 7, "#y/你没有权限进行此种操作！")

			return 0
		elseif self:取职务数量(self.帮派编号, 职务) >= 1 then
			发送数据(玩家数据[id].连接id, 7, "#y/该担任该职务的人数已达上限")

			return 0
		else
			帮派数据[self.帮派编号].成员名单[self.操作id].职务 = 职务

			广播帮派消息(self.帮派编号, "#bp/#r/" .. 玩家数据[id].角色.数据.名称 .. "#y/将#r/" .. 帮派数据[self.帮派编号].成员名单[self.操作id].名称 .. "#y/任命为" .. 职务)

			if 玩家数据[self.操作id] ~= nil then
				发送数据(玩家数据[self.操作id].连接id, 7, "#y/你在帮中的职务发生了变化")
			end
		end
	elseif 职务 == "长老" then
		if 取帮派踢人权限(id, self.帮派编号) == false then
			发送数据(玩家数据[id].连接id, 7, "#y/你没有权限进行此种操作！")

			return 0
		elseif self:取职务数量(self.帮派编号, 职务) >= 4 then
			发送数据(玩家数据[id].连接id, 7, "#y/该担任该职务的人数已达上限")

			return 0
		else
			帮派数据[self.帮派编号].成员名单[self.操作id].职务 = 职务

			广播帮派消息(self.帮派编号, "#bp/#r/" .. 玩家数据[id].角色.数据.名称 .. "#y/将#r/" .. 帮派数据[self.帮派编号].成员名单[self.操作id].名称 .. "#y/任命为" .. 职务)

			if 玩家数据[self.操作id] ~= nil then
				发送数据(玩家数据[self.操作id].连接id, 7, "#y/你在帮中的职务发生了变化")
			end
		end
	elseif 职务 == "帮众" then
		if 取帮派踢人权限(id, self.帮派编号) == false then
			发送数据(玩家数据[id].连接id, 7, "#y/你没有权限进行此种操作！")

			return 0
		elseif 帮派数据[self.帮派编号].成员名单[self.操作id].职务 == "帮主" then
			发送数据(玩家数据[id].连接id, 7, "#y/你没有权限进行此种操作！")

			return 0
		elseif 帮派数据[self.帮派编号].成员名单[self.操作id].职务 == "副帮主" and 帮派数据[self.帮派编号].成员名单[id].职务 ~= "帮主" then
			发送数据(玩家数据[id].连接id, 7, "#y/你没有权限进行此种操作！")

			return 0
		else
			帮派数据[self.帮派编号].成员名单[self.操作id].职务 = 职务

			广播帮派消息(self.帮派编号, "#bp/#r/" .. 玩家数据[id].角色.数据.名称 .. "#y/将#r/" .. 帮派数据[self.帮派编号].成员名单[self.操作id].名称 .. "#y/任命为" .. 职务)

			if 玩家数据[self.操作id] ~= nil then
				发送数据(玩家数据[self.操作id].连接id, 7, "#y/你在帮中的职务发生了变化")
			end
		end
	end
end

function 道具处理类:创建帮派(id, 内容)
	if 取帮派数量() >= 6 then
		发送数据(玩家数据[id].连接id, 7, "#y/帮派数量已达上限，无法再创建新的帮派")

		return 0
	elseif 玩家数据[id].角色.数据.帮派 ~= nil then
		发送数据(玩家数据[id].连接id, 7, "#y/请先脱离原有的帮派")

		return 0
	end

	if 玩家数据[id].角色:扣除仙玉(10000,"创建帮派")==false then
		发送数据(玩家数据[id].连接id, 7, "#y/你没有那么多的仙玉")

		return 0

  else
	帮派数据.编号 = 帮派数据.编号 + 1
	self.临时编号 = 帮派数据.编号
	玩家数据[id].角色.数据.帮派 = self.临时编号
	帮派数据[self.临时编号] = {
		繁荣 = 500,
		左护法 = 0,
		安定 = 800,
		朱雀堂主 = 0,
		药房 = 1,
		炼化等级 = 0,
		人气 = 500,
		金库 = 1,
		厢房 = 1,
		养生之道 = 0,
		药品增加量 = 0,
		修改宗旨 = false,
		学费指数 = 0,
		右护法 = 0,
		白虎堂主 = 0,
		守护兽等级 = 1,
		资金 = 5000000,
		健身术 = 0,
		青龙堂主 = 0,
		宗旨 = "",
		资材 = 0,
		仓库 = 1,
		书院 = 1,
		兽室 = 1,
		规模 = 1,
		技能上限 = 150,
		副帮主 = 0,
		物价指数 = 0,
		玄武堂主 = 0,
		修理指数 = 0,
		强身术 = 0,
		淬炼等级 = 0,
		行动力 = 0,
		创始人 = {
			id = id,
			名称 = 玩家数据[id].角色.数据.名称
		},
		现任帮主 = {
			id = id,
			名称 = 玩家数据[id].角色.数据.名称
		},
		编号 = self.临时编号,
		帮主 = id,
		临时强化效果 = {
			灵力 = 0,
			伤害 = 0,
			速度 = 0,
			气血 = 0,
			防御 = 0
		},
		成员名单 = {
			[id] = {
				职务 = "帮主",
				id = id,
				名称 = 玩家数据[id].角色.数据.名称,
				帮贡 = {
					当前 = 100,
					获得 = 100
				},
				入帮时间 = os.time(),
				离线时间 = os.time()
			}
		},
		申请名单 = {},
		同盟 = {
			编号 = 0,
			名称 = "无"
		},
		敌对 = {
			编号 = 0,
			名称 = "无"
		},
		掌控区域 = {
			编号 = 0,
			名称 = "无"
		},
		当前内政 = {
			名称 = "无",
			要求进度 = 0,
			总进度 = 0
		},
		名称 = 内容,
		研究数据 = {
			类型 = "无",
			当前经验 = 0,
			升级经验 = 0
		}
	}

	广播消息(9, "#xt/#g/" .. 玩家数据[id].角色.数据.名称 .. "#w/在长安城帮派总管处成功创建了新帮派#r/" .. 内容)
	玩家数据[id].角色:添加系统消息(id, "#h/你成功创建了一个帮派。")

	玩家数据[id].角色.数据.帮派 = self.临时编号
	end
end

function 道具处理类:取帮派人数(编号)
	self.临时人数 = 0

	for n, v in pairs(帮派数据[编号].成员名单) do
		if 帮派数据[编号].成员名单[n] ~= nil then
			self.临时人数 = self.临时人数 + 1
		end
	end

	return self.临时人数
end

function 道具处理类:获取帮派信息(id, 编号)
	if 帮派数据[编号] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/帮派数据异常")

		return 0
	end

	self.发送信息 = table.copy(帮派数据[编号])
	self.发送信息.当前人数 = 0

	for n, v in pairs(self.发送信息.成员名单) do
		self.发送信息.当前人数 = self.发送信息.当前人数 + 1

		if 玩家数据[n] ~= nil then
			self.发送信息.成员名单[n].在线 = true
		else
			self.发送信息.成员名单[n].在线 = false
		end
	end

	self.发送信息.人数上限 = 20 + self.发送信息.厢房 * 20

	发送数据(玩家数据[id].连接id, 20020, self.发送信息)
end

function 道具处理类:获取帮派信息2(id, 编号)
	self.发送信息 = {
		名单 = 帮派数据[编号].成员名单
	}

	发送数据(玩家数据[id].连接id, 20024, self.发送信息)
end

function 道具处理类:获取帮派信息1(id, 编号)
	if 帮派数据[编号] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/帮派数据异常")

		return 0
	end

	self.发送信息 = table.copy(帮派数据[编号])
	self.发送信息.当前人数 = 0

	for n, v in pairs(self.发送信息.成员名单) do
		self.发送信息.当前人数 = self.发送信息.当前人数 + 1

		if 玩家数据[n] ~= nil then
			self.发送信息.成员名单[n].在线 = true
		else
			self.发送信息.成员名单[n].在线 = false
		end
	end

	self.发送信息.成员名单 = nil
	self.发送信息.申请名单 = nil
	self.发送信息.人数上限 = 20 + self.发送信息.厢房 * 20

	发送数据(玩家数据[id].连接id, 20026, self.发送信息)
end



function 道具处理类:摊位处理(id)
	摆摊处理类:发送摊位信息(id)
end

function 道具处理类:增加识别码(id, 道具id)
	self.数据[道具id].识别码 = 取随机数(1, 999999) .. os.time() .. 取随机数(1, 999999) .. id .. 取随机数(1, 999999)
end



function 道具处理类:灵饰处理(id, 道具id, 等级, 强化, 类型)
	self.幻化id = 道具id
	self.数据[self.幻化id] = {
		幻化等级 = 0,
		幻化属性 = {
			附加 = {}
		}
	}
	self.临时属性 = 灵饰属性[类型].主属性[取随机数(1, #灵饰属性[类型].主属性)]
	self.临时数值 = 灵饰属性.基础[self.临时属性][等级].b
	self.临时下限 = 灵饰属性.基础[self.临时属性][等级].a
	self.临时数值 = 取随机数(self.临时下限, self.临时数值)

	if 强化 == 1 then
		self.临时数值 = math.floor(self.临时数值 * 1.1)
	end

	self.数据[self.幻化id].幻化属性.基础 = {
		强化 = 0,
		类型 = self.临时属性,
		数值 = self.临时数值
	}
    if 取随机数() <= 40 then
    self.数据[self.幻化id].特效="超级简易"
   end
	for n = 1, 4 do --取随机数(1, 4)
		self.临时属性 = 灵饰属性[类型].副属性[取随机数(1, #灵饰属性[类型].副属性)]
		self.临时数值 = 灵饰属性.基础[self.临时属性][等级].b
		self.临时下限 = 灵饰属性.基础[self.临时属性][等级].a
		self.临时数值 = 取随机数(self.临时下限, self.临时数值)

		for i = 1, #self.数据[self.幻化id].幻化属性.附加 do
			if self.数据[self.幻化id].幻化属性.附加[i].类型 == self.临时属性 then
				self.临时数值 = self.数据[self.幻化id].幻化属性.附加[i].数值
			end
		end

		self.数据[self.幻化id].幻化属性.附加[n] = {
			强化 = 0,
			类型 = self.临时属性,
			数值 = self.临时数值
		}
	end
end

function 道具处理类:幻化装备(id)
	local 类型 = 玩家数据[id].道具id.类型
	local 格子 = 玩家数据[id].道具id.格子
	self.道具id = 玩家数据[id].角色.数据.道具[类型][格子]

	if self.数据[self.道具id] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/你并没有这样的道具")

		return false
	elseif self.数据[self.道具id].名称 ~= "无名秘籍" then
		发送数据(玩家数据[id].连接id, 7, "#y/你并没有这样的道具")

		return false
	elseif self:幻化检查(id, self.道具id) then
		self.消耗数据 = self:幻化消耗(self.道具id)

		if 银子检查(id, self.消耗数据.银子) == false then
			发送数据(玩家数据[id].连接id, 7, "#y/你没那么多的银子")

			return false
		elseif 玩家数据[id].角色.数据.当前体力 < self.消耗数据.体力 then
			发送数据(玩家数据[id].连接id, 7, "#y/你没那么多的体力")

			return false
		else
			玩家数据[id].角色:扣除银子(id, self.消耗数据.银子, 21)

			玩家数据[id].角色.数据.当前体力 = 玩家数据[id].角色.数据.当前体力 - self.消耗数据.体力

			if 取随机数() <= self.消耗数据.失败 then
				发送数据(玩家数据[id].连接id, 7, "#y/很遗憾，本次幻化失败了")
			else
				self.幻化id = 玩家数据[id].角色.数据.装备数据[self.查找部位]
				self.数据[self.幻化id].幻化属性 = {
					附加 = {}
				}
				self.临时属性 = 幻化属性值[取随机数(1, #幻化属性值)]
				self.临时数值 = math.floor(幻化属性基数[self.临时属性] * self.数据[self.道具id].等级)
				self.临时下限 = math.floor(self.临时数值 * 0.5)

				if self.临时下限 < 1 then
					self.临时下限 = 1
				end

				self.数据[self.幻化id].幻化属性.基础 = {
					强化 = 0,
					类型 = self.临时属性,
					数值 = 取随机数(self.临时下限, self.临时数值)
				}

				for n = 1, self.消耗数据.附加 do
					self.临时属性 = 幻化属性值[取随机数(1, #幻化属性值)]
					self.临时数值 = math.floor(幻化属性基数[self.临时属性] * self.数据[self.道具id].等级)
					self.临时下限 = math.floor(self.临时数值 * 0.5)

					if self.临时下限 < 1 then
						self.临时下限 = 1
					end

					self.数据[self.幻化id].幻化属性.附加[#self.数据[self.幻化id].幻化属性.附加 + 1] = {
						强化 = 0,
						类型 = self.临时属性,
						数值 = 取随机数(self.临时下限, self.临时数值)
					}
				end

				玩家数据[id].角色:刷新装备属性(id)
				发送数据(玩家数据[id].连接id, 7, "#y/装备幻化成功！")
			end
		end

		玩家数据[id].道具.数据[self.道具id] = 0
		玩家数据[id].角色.数据.道具[类型][格子] = nil

		发送数据(玩家数据[id].连接id, 3006, "66")
		发送数据(玩家数据[id].连接id, 3030, "66")
	end
end

function 道具处理类:无名秘籍处理(id, 格子, 类型)
	self.道具id = 玩家数据[id].角色.数据.道具[类型][格子]

	if self.数据[self.道具id] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/你并没有这样的道具")

		return false
	elseif self.数据[self.道具id].名称 ~= "无名秘籍" then
		发送数据(玩家数据[id].连接id, 7, "#y/你并没有这样的道具")

		return false
	elseif self:幻化检查(id, self.道具id) then
		玩家数据[id].道具id = {
			类型 = 类型,
			格子 = 格子
		}
		self.消耗数据 = self:幻化消耗(self.道具id)
		self.连接id内容 = " 你将进行幻化装备操作，幻化成功后将激发装备幻化属性。本次幻化需要消耗#R/" .. self.消耗数据.银子 .. "两银子、" .. self.消耗数据.体力 .. "点体力，并有" .. self.消耗数据.失败 .. "%几率幻化失败#W/，幻化失败后将不返还银子与消耗。如本次幻化成功，将出现#R/" .. self.消耗数据.附加 .. [[
#W/条附加属性。请确认是否继续幻化装备：


  ]] .. "#R/ht|确认幻化" .. [[
/确认幻化
  #R/ht|0/取消


       ]]
		self.发送信息 = {
			名称 = "???",
			连接id = self.连接id内容
		}

		发送数据(玩家数据[id].连接id, 20, self.发送信息)
	end
end

function 道具处理类:幻化消耗(道具id)
	if self.数据[道具id].等级 <= 5 then
		return {
			银子 = 3000000,
			附加 = 1,
			失败 = 0,
			体力 = 150
		}
	elseif self.数据[道具id].等级 <= 10 then
		return {
			银子 = 10000000,
			附加 = 2,
			失败 = 10,
			体力 = 250
		}
	elseif self.数据[道具id].等级 <= 15 then
		return {
			银子 = 50000000,
			附加 = 3,
			失败 = 20,
			体力 = 300
		}
	elseif self.数据[道具id].等级 <= 20 then
		return {
			银子 = 100000000,
			附加 = 4,
			失败 = 30,
			体力 = 500
		}
	end
end

function 道具处理类:幻化检查(id, 道具id)
	self.部位名称 = self.数据[道具id].部位
	self.查找部位 = 0

	if self.部位名称 == "武器" then
		self.查找部位 = 21
	elseif self.部位名称 == "头盔" then
		self.查找部位 = 22
	elseif self.部位名称 == "腰带" then
		self.查找部位 = 23
	elseif self.部位名称 == "项链" then
		self.查找部位 = 24
	elseif self.部位名称 == "鞋子" then
		self.查找部位 = 25
	elseif self.部位名称 == "衣服" then
		self.查找部位 = 26
	end

	if 玩家数据[id].角色.数据.装备数据[self.查找部位] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/请先佩戴要幻化的装备")

		return false
	else
		return true
	end
end



function 道具处理类:修理装备处理1(id, 数据)
	if 玩家数据[id].角色.数据.道具.包裹[玩家数据[id].道具id] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/你并没有这样的道具")

		return false
	elseif 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[玩家数据[id].道具id]].耐久度 == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/只有装备才可以修理噢")

		return false
	elseif 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[玩家数据[id].道具id]].耐久度 >= 500 then
		发送数据(玩家数据[id].连接id, 7, "#y/您的这件装备无须修理")

		return false
	elseif 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[玩家数据[id].道具id]].修理失败 ~= nil and 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[玩家数据[id].道具id]].修理失败 >= 3 then
		发送数据(玩家数据[id].连接id, 7, "#y/您的这件装备已经无法修理了")

		return false
	else
		self.花费金钱 = math.floor(玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[玩家数据[id].道具id]].等级 / 500 * 1500 * (500 - 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[玩家数据[id].道具id]].耐久度))

		if 银子检查(id, self.花费金钱) == false then
			发送数据(玩家数据[id].连接id, 7, "#y/你没那么多的银子")

			return 0
		end

		玩家数据[id].角色:扣除银子(id, self.花费金钱, 18)

		self.失败几率 = 20

		if 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[玩家数据[id].道具id]].特效 == "易修理" then
			self.失败几率 = 10
		end

		if 取随机数() <= self.失败几率 then
			发送数据(玩家数据[id].连接id, 7, "#y/你的装备在修理时出现了点意外")

			if 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[玩家数据[id].道具id]].修理失败 == nil then
				玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[玩家数据[id].道具id]].修理失败 = 1
			else
				玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[玩家数据[id].道具id]].修理失败 = 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[玩家数据[id].道具id]].修理失败 + 1
			end
		end

		玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[玩家数据[id].道具id]].耐久度 = 500

		发送数据(玩家数据[id].连接id, 7, "#y/装备修理成功！")
		发送数据(玩家数据[id].连接id, 3008, "66")
	end
end

function 道具处理类:修理装备处理(id, 数据)
	if 玩家数据[id].角色.数据.道具.包裹[数据.格子] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/你并没有这样的道具")

		return false
	elseif 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[数据.格子]].耐久度 == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/只有装备才可以修理噢")

		return false
	elseif 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[数据.格子]].耐久度 >= 5000 then
		发送数据(玩家数据[id].连接id, 7, "#y/您的这件装备无须修理")

		return false
	elseif 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[数据.格子]].修理失败 ~= nil and 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[数据.格子]].修理失败 >= 3 then
		发送数据(玩家数据[id].连接id, 7, "#y/您的这件装备已经无法修理了")

		return false
	else
		玩家数据[id].道具id = 数据.格子
		self.花费金钱 = math.floor(玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[数据.格子]].等级 / 500 * 1500 * (500 - 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[数据.格子]].耐久度))
		self.连接id内容 = " 修理该装备需要花费#Y/" .. self.花费金钱 .. [[
#W/两银子，请确认是否需要修理？

  #R/ht|xiulizhuangbei/确认修理
  #R/ht|0/我还是卖给收破烂的吧


     ]]
		self.发送信息 = {
			名称 = "装备修理商",
			编号 = 1276,
			对话 = self.连接id内容
		}

		发送数据(玩家数据[id].连接id, 20, self.发送信息)
	end
end

function 道具处理类:填写推广员处理(id, 推广号)
	if 玩家数据[id].角色.数据.推广号 ~= nil then
		发送数据(玩家数据[id].连接id, 7, "#y/你已经填写过推广员号了，每个玩家只能填写一次")

		return false
	elseif 推广号 == "0" then
		self:给予道具(id, "新玩家礼包", "功能")

		玩家数据[id].角色.数据.推广号 = 推广号

		发送数据(玩家数据[id].连接id, 7, "#y/你获得了一个新玩家礼包")
	elseif 推广数据[推广号 + 0] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/这样的推广号并不存在")
	else
		推广号 = 推广号 + 0
		推广数据[推广号].填写人数 = 推广数据[推广号].填写人数 + 1

		self:给予道具(id, "新玩家礼包", "功能")

		玩家数据[id].角色.数据.推广号 = 推广号

		发送数据(玩家数据[id].连接id, 7, "#y/你获得了一个新玩家礼包")
		发送数据(玩家数据[id].连接id, 7, "#y/当你的等级达到50级时，id为" .. 推广号 .. "的玩家将获得推广奖励")
	end
end

function 道具处理类:装备添加特效处理(id,数据)

	self.装备类型=""
	self.特效重复 = false
    self.道具id=玩家数据[id].角色.数据.道具.包裹[数据.格子]
    self.装备类型=玩家数据[id].道具.数据[self.道具id].类型
    local 附加装备类型=玩家数据[id].道具.数据[self.道具id].类型
    local 附加装备等级=玩家数据[id].道具.数据[self.道具id].等级
    if 玩家数据[id].角色.数据.道具.包裹[数据.格子]==nil then
	    发送数据(玩家数据[id].连接did,7,"#y/你并没有这样的道具")

	    return 0
	elseif self.装备类型~="武器" and self.装备类型~="衣服" and self.装备类型~="头盔" and self.装备类型~="项链" and self.装备类型~="腰带" and self.装备类型~="鞋子"  then
	    发送数据(玩家数据[id].连接did,7,"#y/只有装备才可附加特效！")

	    return 0

    elseif 附加装备等级 < 120 then
	     发送数据(玩家数据[id].连接did,7,"#y/只有120级-160级装备才可附加特效！")

	    return 0
    end
	    if 附加装备等级 == 120 then
	    	   消耗数额 = 200
	    elseif 附加装备等级 == 130 then
	    	   消耗数额 = 400
	    elseif 附加装备等级 == 140 then
	    	   消耗数额 = 600
	    elseif 附加装备等级 == 150 then
	    	   消耗数额 = 800
	    elseif 附加装备等级 == 160 then
	    	   消耗数额 = 1000
	    end
    if 玩家数据[id].角色:扣除仙玉(消耗数额,id) then
	   玩家数据[id].角色:添加消费日志("消耗"..消耗数额.."点仙玉附加无级别特效")
	else
		发送数据(玩家数据[id].连接did, 7, "#y/你没有那么多的仙玉")

		return 0
	end

    if 玩家数据[id].道具.数据[self.道具id].鉴定 == false then
       发送数据(玩家数据[id].连接did,7,"#y/请先鉴定！")
    	return 0
	end

	if 玩家数据[id].道具.数据[self.道具id].特效 == nil then
		if self.特效重复 == false and 取随机数()<=30 then
			玩家数据[id].道具.数据[self.道具id].特效 = {[1] = "无级别限制"}
			发送数据(玩家数据[id].连接did,7,"#y/恭喜你为装备附加无级别特效成功！")
    		return 0
		end
	else
		for i=1,#玩家数据[id].道具.数据[self.道具id].特效 do
	    	if 玩家数据[id].道具.数据[self.道具id].特效[i] == "无级别限制" then
	    		self.特效重复 = true
	    		发送数据(玩家数据[id].连接did,7,"#y/该装备已经有无级别特效了！")
	    		return 0
	     	end
	    end
	    if self.特效重复 == false and 取随机数()<= 5 then
	    	if #玩家数据[id].道具.数据[self.道具id].特效 >=2 then
    			玩家数据[id].道具.数据[self.道具id].特效[1] = "无级别限制"

    		elseif #玩家数据[id].道具.数据[self.道具id].特效 >=1 then
    			玩家数据[id].道具.数据[self.道具id].特效[2] = "无级别限制"
    		end
    		发送数据(玩家数据[id].连接did,7,"#y/恭喜你为装备附加无级别特效成功！")

    		return 0
	    end
	end
    	发送数据(玩家数据[id].连接did,7,"#y/上厕所没洗手,附加无级别特效失败！")
end

function 道具处理类:装备添加特技处理(id,数据)

	self.装备类型=""
	self.特效重复 = false
    self.道具id=玩家数据[id].角色.数据.道具.包裹[数据.格子]
    self.装备类型=玩家数据[id].道具.数据[self.道具id].类型
    local 附加装备类型=玩家数据[id].道具.数据[self.道具id].类型
    local 附加装备等级=玩家数据[id].道具.数据[self.道具id].等级

    if 玩家数据[id].角色.数据.道具.包裹[数据.格子]==nil then

      发送数据(玩家数据[id].连接did,7,"#y/你并没有这样的道具")
      return 0
 	elseif self.装备类型~="武器" and self.装备类型~="衣服" and self.装备类型~="头盔" and self.装备类型~="项链" and self.装备类型~="腰带" and self.装备类型~="鞋子"  then
      发送数据(玩家数据[id].连接did,7,"#y/只有装备才可附加特技！")
      return 0
    end
    self.消耗仙玉 = 200

	if 玩家数据[id].角色:扣除仙玉(200,"添加特技") then
		玩家数据[id].角色:添加消费日志("消耗"..self.消耗仙玉 .."点仙玉附加无级别特效")
	else
		发送数据(玩家数据[id].连接did, 7, "#y/你没有那么多的仙玉")
		return 0
	end
	self.特技名称 = {
		"破血狂攻",
		"弱点击破",
		"凝气决",
		"凝神决",
		"冥王暴杀",
		"慈航普渡",
		"诅咒之伤",
		"气疗术",
		"命疗术",
		"气归术",
		"命归术",
		"水清诀",
		"冰清诀",
		"玉清诀",
		"晶清诀",
		"四海升平",
		"罗汉金钟",
		"圣灵之甲",
		"魔兽之印",
		"野兽之力",
		"放下屠刀",
		"太极护法",
		"光辉之甲"
	}

	if 取随机数()<=50 then
		玩家数据[id].道具.数据[self.道具id].特技 = self.特技名称[取随机数(1,#self.特技名称)]
		发送数据(玩家数据[id].连接did,7,"#y/恭喜你为装备装备添加特技处理成功！")
    	return 0
    else
    	发送数据(玩家数据[id].连接did,7,"#y/上厕所没洗手,附加特技失败！")
	end

end


function 道具处理类:添加装备临时效果(id, 格子)
	self.临时类型 = 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[格子]].类型
	self.帮派编号 = 玩家数据[id].角色.数据.帮派

	if self.帮派编号 == nil or 帮派数据[self.帮派编号] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/帮派数据异常")

		return 0
	end

	if 帮派数据[self.帮派编号].成员名单[id].帮贡.当前 < 10 then
		发送数据(玩家数据[id].连接id, 7, "#y/本次操作需要消耗10点帮贡")

		return 0
	end

	帮派数据[self.帮派编号].成员名单[id].帮贡.当前 = 帮派数据[self.帮派编号].成员名单[id].帮贡.当前 - 10
	玩家数据[id].角色.数据.帮贡 = 帮派数据[self.帮派编号].成员名单[id].帮贡.当前
	发送数据(玩家数据[id].连接id, 7, "#y/你失去了10点帮贡")

	self.临时等级 = 帮派数据[self.帮派编号].兽室

	if self.临时类型 == "武器" then
		self.临时数值 = 取随机数(math.floor(帮派数据[self.帮派编号].兽室 * 5), math.floor(帮派数据[self.帮派编号].兽室 * 10))
		玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[格子]].临时效果 = {
			类型 = "伤害",
			数值 = self.临时数值,
			时间 = os.time() + 172800
		}
	elseif self.临时类型 == "衣服" then
		self.临时数值 = 取随机数(math.floor(帮派数据[self.帮派编号].兽室 * 5), math.floor(帮派数据[self.帮派编号].兽室 * 10))
		玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[格子]].临时效果 = {
			类型 = "防御",
			数值 = self.临时数值,
			时间 = os.time() + 172800
		}
	elseif self.临时类型 == "头盔" then
		self.临时数值 = 取随机数(math.floor(帮派数据[self.帮派编号].兽室 * 5), math.floor(帮派数据[self.帮派编号].兽室 * 10))
		玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[格子]].临时效果 = {
			类型 = "魔法",
			数值 = self.临时数值,
			时间 = os.time() + 172800
		}
	elseif self.临时类型 == "项链" then
		self.临时数值 = 取随机数(math.floor(帮派数据[self.帮派编号].兽室 * 2.5), math.floor(帮派数据[self.帮派编号].兽室 * 5))
		玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[格子]].临时效果 = {
			类型 = "灵力",
			数值 = self.临时数值,
			时间 = os.time() + 172800
		}
	elseif self.临时类型 == "腰带" then
		self.临时数值 = 取随机数(math.floor(帮派数据[self.帮派编号].兽室 * 5), math.floor(帮派数据[self.帮派编号].兽室 * 10))
		玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[格子]].临时效果 = {
			类型 = "气血",
			数值 = self.临时数值,
			时间 = os.time() + 172800
		}
	elseif self.临时类型 == "鞋子" then
		self.临时数值 = 取随机数(math.floor(帮派数据[self.帮派编号].兽室 * 1), math.floor(帮派数据[self.帮派编号].兽室 * 2.5))
		玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[格子]].临时效果 = {
			类型 = "速度",
			数值 = self.临时数值,
			时间 = os.time() + 172800
		}
	end

	发送数据(玩家数据[id].连接id, 7, "#y/添加装备临时效果成功！")
end

function 道具处理类:删除道具处理(id, 数据)
	if 数据.id == -705 then
		self.临时类型 = 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[数据.格子]].类型

		if self.临时类型 ~= "武器" and self.临时类型 ~= "衣服" and self.临时类型 ~= "头盔" and self.临时类型 ~= "项链" and self.临时类型 ~= "腰带" and self.临时类型 ~= "鞋子" then
			发送数据(玩家数据[id].连接id, 7, "#y/只有装备才可以添加临时效果")

			return false
		end

		return true
	elseif 数据.id == -381 then
		self.临时类型 = 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[数据.格子]].类型

		if self.临时类型 ~= "武器" and self.临时类型 ~= "衣服" and self.临时类型 ~= "头盔" and self.临时类型 ~= "项链" and self.临时类型 ~= "腰带" and self.临时类型 ~= "鞋子" then
			发送数据(玩家数据[id].连接id, 7, "#y/只有装备才可以分解")
			return false
		else
			return true
		end

	elseif 数据.id == -701 and 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[数据.格子]].名称 == "金银宝盒" then
		玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[数据.格子]] = nil
		玩家数据[id].角色.数据.道具.包裹[数据.格子] = nil

		return true
	elseif 任务数据[数据.id] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/这个任务并不存在")

		return false
	elseif 玩家数据[id].角色.数据.道具.包裹[数据.格子] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/你并没有这样的道具")

		return false
	elseif 任务数据[数据.id].道具 ~= 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[数据.格子]].名称 then
		发送数据(玩家数据[id].连接id, 7, "#y/对方需要的不是这个东西")

		return false
	elseif 任务数据[数据.id].数量 == nil then
		if 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[数据.格子]].数量 == nil then
			玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[数据.格子]] = nil
			玩家数据[id].角色.数据.道具.包裹[数据.格子] = nil

			发送数据(玩家数据[id].连接id, 3008, "66")

			return true
		else
			玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[数据.格子]].数量 = 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[数据.格子]].数量 - 1

			if 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[数据.格子]].数量 <= 0 then
				玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[数据.格子]] = nil
				玩家数据[id].角色.数据.道具.包裹[数据.格子] = nil
			end

			发送数据(玩家数据[id].连接id, 3008, "66")

			return true
		end
	else
		if 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[数据.格子]].数量 == nil or 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[数据.格子]].数量 < 任务数据[数据.id].数量 then
			发送数据(玩家数据[id].连接id, 7, "#y/给予对方的道具数量不足")

			return false
		else
			玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[数据.格子]].数量 = 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[数据.格子]].数量 - 任务数据[数据.id].数量

			if 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[数据.格子]].数量 <= 0 then
				玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[数据.格子]] = nil
				玩家数据[id].角色.数据.道具.包裹[数据.格子] = nil
			end

			发送数据(玩家数据[id].连接id, 3008, "66")
		end

		return true
	end
end

function 道具处理类:小铲子处理(id, 格子, 类型)
	local 数据 = {
		类型 = 类型,
		格子 = 格子
	}
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]

	if 宝藏山名单[玩家数据[id].数字id] == nil then
		宝藏山名单[玩家数据[id].数字id] = 20
	end

	if self.数据[self.临时id1] == nil or self.数据[self.临时id1] == 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/你似乎并没有这样的道具")

		return 0
	elseif 宝藏山名单[玩家数据[id].数字id] <= 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/你本日的可用挖宝次数已用光了")
		发送数据(玩家数据[id].连接id, 7, "#y/您的这个道具已被回收")

		self.数据[self.临时id1] = nil
		玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil

		发送数据(玩家数据[id].连接id, 3006, "66")

		return 0
	elseif 宝藏山开关 == false then
		发送数据(玩家数据[id].连接id, 7, "#y/您的这个道具已被回收")

		self.数据[self.临时id1] = nil
		玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil

		发送数据(玩家数据[id].连接id, 3006, "66")

		return 0
	elseif 玩家数据[id].地图 ~= 1511 then
		发送数据(玩家数据[id].连接id, 7, "#y/该道具只能在宝藏山使用")

		return 0
	elseif 取两点距离a(玩家数据[id].角色.数据.地图数据.x, 玩家数据[id].角色.数据.地图数据.y, self.数据[self.临时id1].x, self.数据[self.临时id1].y) <= 200 then
		发送数据(玩家数据[id].连接id, 7, "#y/这里你已经挖过了，请换个地方试试")

		return 0
	elseif 玩家数据[id].角色.数据.当前体力 < 20 then
		发送数据(玩家数据[id].连接id, 7, "#y/你的体力不足20点")

		return 0
	else
		宝藏山名单[玩家数据[id].数字id] = 宝藏山名单[玩家数据[id].数字id] - 1

		发送数据(玩家数据[id].连接id, 7, "#y/你本日在宝藏山可挖宝次数为" .. 宝藏山名单[玩家数据[id].数字id] .. "次")

		玩家数据[id].角色.数据.当前体力 = 玩家数据[id].角色.数据.当前体力 - 20
		self.数据[self.临时id1].y = 玩家数据[id].角色.数据.地图数据.y
		self.数据[self.临时id1].x = 玩家数据[id].角色.数据.地图数据.x

		self:宝藏山奖励(id)
		发送数据(玩家数据[id].连接id, 3006, "66")
	end
end

function 道具处理类:幻域迷宫奖励(id)
	if 幻域迷宫数据[玩家数据[id].数字id] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/数据异常")

		return 0
	elseif 幻域迷宫数据[玩家数据[id].数字id].完成 ~= false then
		发送数据(玩家数据[id].连接id, 7, "#y/你已经领取过奖励噢")

		return 0
	elseif 玩家数据[id].地图 ~= 7020 then
		f函数.写配置(data目录 .. 玩家数据[id].账号.. _账号txt, "账号信息", "封禁", "1")
		网络处理类:强制断开1(玩家数据[账号取id(玩家数据[id].账号)].连接id, "你的账号已经被管理员封禁，你已经被强制断开连接。")
		登录处理类:玩家退出(账号取id(玩家数据[id].账号))

		return 0
	end
	幻域迷宫数据[玩家数据[id].数字id].完成 = true
	self.经验奖励 = math.floor(玩家数据[id].角色.数据.等级 * 50 + 玩家数据[id].角色.数据.等级 * 5000 / 2 * 10) * 5
	self.抓鬼奖励银子 = math.floor(玩家数据[id].角色.数据.等级 * 30000)
	玩家数据[id].角色:添加经验(self.经验奖励, id, 15, 提示)
	玩家数据[id].角色:添加银子(id, self.抓鬼奖励银子, 405)
	self.奖励参数 = 取随机数()

	if #幻域迷宫数据.排名奖励 < 2 then
		幻域迷宫数据.排名奖励[#幻域迷宫数据.排名奖励 + 1] = id
		self.生成等级 = 取随机数(12, 13) * 10
		玩家数据[id].道具:给予道具(id, "上古锻造图策", "功能", self.生成等级, 取上古锻造图策())
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/以惊人的速度率先到达幻域迷宫第20层，获得了金毛猿额外奖励的/#r/" .. self.生成等级 .. "级上古锻造图策")
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了上古锻造图策")
	end

	if self.奖励参数 <= 25 then
		self:给予道具(id, "元宵", "功能", 1)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了元宵")
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/成功到达幻域迷宫第20层，获得了金毛猿奖励的#r/" .. "元宵")
	elseif self.奖励参数 <= 35 then
		self:给予道具(id, "高级魔兽要诀", "普通", 取高级兽诀名称())
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了一本高级魔兽要诀")
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/成功到达幻域迷宫第20层，获得了金毛猿奖励的#r/" .. "高级魔兽要诀")
	elseif self.奖励参数 <= 45 then
		self:给予道具(id, "魔兽要诀", "普通", 取低级兽诀名称())
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了一本魔兽要诀")
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/成功到达幻域迷宫第20层，获得了金毛猿奖励的#r/" .. "魔兽要诀")
	elseif self.奖励参数 <= 65 then
		self:给予道具(id, "摇钱树树苗", "功能", 20)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了摇钱树树苗")
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/成功到达幻域迷宫第20层，获得了金毛猿奖励的#r/" .. "摇钱树树苗")
	elseif self.奖励参数 <= 75 then
		self.随机名称 = 取随机书铁()
		self.任务链等级 = 取随机数(8, 10)

		玩家数据[id].道具:给予道具(id, self.随机名称, "打造", self.任务链等级, self.任务链等级, 取随机制造())

		self.任务链等级 = self.任务链等级 * 10

		发送数据(玩家数据[id].连接id, 7, "#y/你获得了" .. self.随机名称)
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/成功到达幻域迷宫第20层，获得了金毛猿奖励的" .. self.任务链等级 .. "级的#r/" .. self.随机名称)
	elseif self.奖励参数 <= 85 then
		self.生成等级 = 取随机数(12, 13) * 10

		玩家数据[id].道具:给予道具(id, "上古锻造图策", "功能", self.生成等级, 取上古锻造图策())
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/成功到达幻域迷宫第20层，获得了金毛猿奖励的#r/" .. self.生成等级 .. "级上古锻造图策")
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了上古锻造图策")
	else
		self:给予道具(id, "珍珠", "普通", 150)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了珍珠")
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/成功到达幻域迷宫第20层，获得了金毛猿奖励的#r/" .. "150级珍珠")
	end
end

function 道具处理类:摇钱树奖励(id, 标识)
	self.任务id = 地图处理类.地图数据[玩家数据[id].地图].单位组[标识].id

	if 任务数据[self.任务id] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/任务数据异常")

		return 0
	elseif 任务数据[self.任务id].次数 <= 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/您的这棵摇钱树已经没有可摇动次数了")

		return 0
	elseif 任务数据[self.任务id].数字id ~= id then
		发送数据(玩家数据[id].连接id, 7, "#y/这不是你的摇钱树")

		return 0
	end

	self.奖励参数 = 取随机数()

	if self.奖励参数 <= 15 then
		self.宝石名称 = Q_宝图宝石[取随机数(1, #Q_宝图宝石)]

		玩家数据[id].道具:给予道具(id, self.宝石名称, "宝石", 取随机数(1, 3))
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了" .. self.宝石名称)
	elseif self.奖励参数 <= 20 then
		self:给予道具(id, "魔兽要诀", "普通", 取低级兽诀名称())
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了一本魔兽要诀")
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在摇动摇钱树时，竟然获得了从树上掉落的#r/" .. "魔兽要诀")
	elseif self.奖励参数 <= 25 then
		self:给予道具(id, "高级魔兽要诀", "普通", 取高级兽诀名称())
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了一本高级魔兽要诀")
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在摇动摇钱树时，竟然获得了从树上掉落的#r/" .. "高级魔兽要诀")
	elseif self.奖励参数 <= 40 then
		self.强化石名称 = 取强化石()

		玩家数据[id].道具:给予道具(id, self.强化石名称, "普通", 2)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了" .. self.强化石名称)
	elseif self.奖励参数 <= 50 then
		玩家数据[id].道具:给予道具(id, "彩果", "普通", 1)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了" .. "彩果")
	elseif self.奖励参数 <= 70 then
		玩家数据[id].道具:给予道具(id, "花豆", "普通", 5)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了" .. "花豆")
	elseif self.奖励参数 <= 85 then
		self.随机名称 = 取随机书铁()
		self.任务链等级 = 取随机数(8, 11)

		玩家数据[id].道具:给予道具(id, self.随机名称, "打造", self.任务链等级, self.任务链等级, 取随机制造())

		self.任务链等级 = self.任务链等级 * 10

		发送数据(玩家数据[id].连接id, 7, "#y/你获得了" .. self.随机名称)
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在摇动摇钱树时，竟然获得了从树上掉落的#r/" .. self.任务链等级 .. "级的#r/" .. self.随机名称)
	else
		玩家数据[id].角色:添加银子(id, 100000, 31)
	end

	任务数据[self.任务id].次数 = 任务数据[self.任务id].次数 - 1

	if 任务数据[self.任务id].次数 <= 0 then
		玩家数据[id].角色:取消任务(self.任务id)
		地图处理类:移除单位(玩家数据[id].地图, self.任务id)

		任务数据[self.任务id] = nil

		发送数据(玩家数据[id].连接id, 7, "#y/你的这棵摇钱树消失了")
	end
end

function 道具处理类:水果抽奖(id)
	if 玩家数据[id].角色.数据.水果积分.当前 == nil or 玩家数据[id].角色.数据.水果积分.当前 < 9 then
		发送数据(玩家数据[id].连接id, 7, "#y/你的积分不够")

		return 0
	end

	玩家数据[id].角色.数据.水果积分.当前 = 玩家数据[id].角色.数据.水果积分.当前 - 9
	self.临时等级 = 玩家数据[id].角色.数据.等级
	self.储备奖励 = self.临时等级 * self.临时等级 * 50

	玩家数据[id].角色:添加储备(id, self.储备奖励, 288888)

	self.奖励参数 = 取随机数(1, 150)

	if  self.奖励参数 <= 10 then
		self.随机名称 = 取随机书铁()
		self.任务链等级 = 取随机数(12, 14)

		玩家数据[id].道具:给予道具(id, self.随机名称, "打造", self.任务链等级, self.任务链等级, 取随机制造())

		self.任务链等级 = self.任务链等级 * 10

		发送数据(玩家数据[id].连接id, 7, "#y/你获得了" .. self.随机名称)
	elseif self.奖励参数 <= 20 then
		self:给予道具(id, "魔兽要诀", "普通", 取低级兽诀名称())
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了一本魔兽要诀")
	elseif self.奖励参数 <= 30 then
		self:给予道具(id, "金银宝盒", "杂货", 1)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了一个金银宝盒")
	elseif self.奖励参数 <= 50 then
		self.强化石名称 = 取强化石()

		self:给予道具(id, self.强化石名称, "普通", 5)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了" .. self.强化石名称)
	elseif self.奖励参数 <= 70 then
		self:给予道具(id, "天眼通符", "功能", 5)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了天眼通符")
	else
		self:给予道具(id, "超级金柳露", "普通", 1)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了超级金柳露")
	end
end

function 道具处理类:妖魔抽奖(id)
	if 玩家数据[id].角色.数据.妖魔积分.当前 == nil or 玩家数据[id].角色.数据.妖魔积分.当前 < 10 then
		发送数据(玩家数据[id].连接id, 7, "#y/你的积分不够")

		return 0
	end

	玩家数据[id].角色.数据.妖魔积分.当前 = 玩家数据[id].角色.数据.妖魔积分.当前 - 10
	self.临时等级 = 玩家数据[id].角色.数据.等级
	self.储备奖励 = self.临时等级 * self.临时等级 * 50

	玩家数据[id].角色:添加储备(id, self.储备奖励, 288888)

	self.奖励参数 = 取随机数()

	if self.奖励参数 <= 35 then
		self:给予道具(id, "魔兽要诀", "普通", 取低级兽诀名称())
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了一本魔兽要诀")
		--广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在太白金星使用妖魔积分幸运抽奖功能时，居然抽到了#r/" .. "低级魔兽要诀")
	--elseif self.奖励参数 <= 30 then
	--	self:给予道具(id, "金银宝盒", "杂货", 1)
	--	发送数据(玩家数据[id].连接id, 7, "#y/你获得了一个金银宝盒")
		--广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在太白金星使用妖魔积分幸运抽奖功能时，居然抽到了#r/" .. "金银宝盒")
	elseif  self.奖励参数 <= 42 then
		self.随机名称 = 取随机书铁()
		self.任务链等级 = 取随机数(6, 13)

		玩家数据[id].道具:给予道具(id, self.随机名称, "打造", self.任务链等级, self.任务链等级, 取随机制造())

		self.任务链等级 = self.任务链等级 * 10

		发送数据(玩家数据[id].连接id, 7, "#y/你获得了" .. self.随机名称)
	--广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在太白金星使用妖魔积分幸运抽奖功能时，居然抽到了#r/" .. "强化石")
	elseif self.奖励参数 <= 80 then
		self.强化石名称 = 取强化石()

		self:给予道具(id, self.强化石名称, "普通", 5)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了" .. self.强化石名称)
		--广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在太白金星使用妖魔积分幸运抽奖功能时，居然抽到了#r/" .. "强化石")
	else
		self:给予道具(id, "法宝碎片", "普通", 2)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了法宝碎片")
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在太白金星使用妖魔积分幸运抽奖功能时，居然抽到了#r/" .. "法宝碎片*2")

	--else
		--self:给予道具(id, "超级金柳露", "普通", 1)
		--发送数据(玩家数据[id].连接id, 7, "#y/你获得了超级金柳露")
		--广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在太白金星使用妖魔积分幸运抽奖功能时，居然抽到了#r/" .. "超级金柳露")
	end
end

function 道具处理类:门贡兑换(id)
	if 玩家数据[id].角色.数据.门贡 < 50 then
		发送数据(玩家数据[id].连接id, 7, "#y/你没有那么多的门派贡献度")

		return 0
	end

	玩家数据[id].角色.数据.门贡 = 玩家数据[id].角色.数据.门贡 - 50

	发送数据(玩家数据[id].连接id, 7, "#y/你消耗了50点门派贡献度")

	self.奖励参数 = 取随机数(1, 120)

	if self.奖励参数 <= 15 then
		self:给予道具(id, "魔兽要诀", "普通", 取低级兽诀名称())
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了一本魔兽要诀")
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在门派首席弟子处使用门派贡献度兑换时，居然获得了#r/" .. "魔兽要诀")
	elseif self.奖励参数 <= 25 then
		self:给予道具(id, "高级魔兽要诀", "普通", 取高级兽诀名称())
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了一本高级魔兽要诀")
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在门派首席弟子处使用门派贡献度兑换时，居然获得了#r/" .. "高级魔兽要诀")
	elseif self.奖励参数 <= 35 then
		self:给予道具(id, "神兜兜", "普通", 1)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了一个神兜兜")
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在门派首席弟子处使用门派贡献度兑换时，居然获得了#r/" .. "神兜兜")
	elseif self.奖励参数 <= 55 then
		self:给予道具(id, "摇钱树树苗", "功能", 20)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了摇钱树树苗")
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在门派首席弟子处使用门派贡献度兑换时，居然获得了#r/" .. "摇钱树树苗")
	elseif self.奖励参数 <= 65 then
		self:给予道具(id, "神兜兜", "普通", 1)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了一个神兜兜")
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在门派首席弟子处使用门派贡献度兑换时，居然获得了#r/" .. "神兜兜")
	elseif self.奖励参数 <= 75 then
		玩家数据[id].道具:给予道具(id, "鬼谷子", "功能", "魔力")
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了一本鬼谷子")
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在门派首席弟子处使用门派贡献度兑换时，居然获得了#r/" .. "鬼谷子")
	else
		self.随机储备 = 取随机数(2000000, 8000000)

		玩家数据[id].角色:添加储备(id, self.随机储备, 31)
	end
end

function 道具处理类:幸运抽奖(id)
	if 玩家数据[id].角色:扣除仙玉(2000,"仙玉抽奖") == false then
		发送数据(玩家数据[id].连接id, 7, "#y/你没有那么多的仙玉")

		return 0
	end

	发送数据(玩家数据[id].连接id, 7, "#y/你消耗了2000点仙玉")

	self.奖励参数 = 取随机数(1, 130)

	if self.奖励参数 <= 2 then
		self:给予道具(id, "蟠桃", "功能", 1)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了蟠桃")
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在神树中使用幸运抽奖功能时，居然抽到了#r/" .. "蟠桃")
		  elseif self.奖励参数<=3 then


   self.临时名称=玩家数据[id].装备:取150级随机装备(id)

   广播消息(9,"#xt/#g/ "..玩家数据[id].角色.数据.名称.."#y/在转盘中使用转盘抽奖功能时，居然抽到了150级的#r/"..self.临时名称)

 elseif self.奖励参数<=10 then


   self.临时名称=玩家数据[id].装备:取130级随机装备(id)

   广播消息(9,"#xt/#g/ "..玩家数据[id].角色.数据.名称.."#y/在转盘中使用转盘抽奖功能时，居然抽到了130级的#r/"..self.临时名称)
	elseif self.奖励参数 <= 8 then
		self:给予道具(id, "元宵", "功能", 1)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了元宵")
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在神树中使用幸运抽奖功能时，居然抽到了#r/" .. "元宵")
	elseif self.奖励参数 <= 20 then
		self:给予道具(id, "元宵", "功能", 1)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了元宵")
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在神树中使用幸运抽奖功能时，居然抽到了#r/" .. "元宵")
	elseif self.奖励参数 <= 30 then
		self.随机名称 = 取随机书铁()
		self.任务链等级 = 取随机数(10, 15)

		玩家数据[id].道具:给予道具(id, self.随机名称, "打造", self.任务链等级, self.任务链等级, 取随机制造())

		self.任务链等级 = self.任务链等级 * 10

		发送数据(玩家数据[id].连接id, 7, "#y/你获得了" .. self.随机名称)
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在神树中使用幸运抽奖功能时，居然抽到了" .. self.任务链等级 .. "级的#r/" .. self.随机名称)
	elseif self.奖励参数 <= 40 then
		self.宝石名称 = Q_宝图宝石[取随机数(1, #Q_宝图宝石)]

		玩家数据[id].道具:给予道具(id, self.宝石名称, "宝石", 取随机数(5, 8))
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了" .. self.宝石名称)
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在神树中使用幸运抽奖功能时，居然抽到了#r/" .. self.宝石名称)
	elseif self.奖励参数 <= 45 then
		self.宝石名称 = Q_宝图宝石[取随机数(1, #Q_宝图宝石)]

		玩家数据[id].道具:给予道具(id, self.宝石名称, "宝石", 取随机数(5, 8))
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了" .. self.宝石名称)
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在神树中使用幸运抽奖功能时，居然抽到了#r/" .. self.宝石名称)
	elseif self.奖励参数 <= 50 then
		self.随机名称 = 取随机书铁()
		self.任务链等级 = 取随机数(10, 15)
		玩家数据[id].道具:给予道具(id, self.随机名称, "打造", self.任务链等级, self.任务链等级, 取随机制造())
		self.任务链等级 = self.任务链等级 * 10
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了" .. self.随机名称)
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在神树中使用幸运抽奖功能时，居然抽到了" .. self.任务链等级 .. "级的#r/" .. self.随机名称)
	elseif self.奖励参数 <= 60 then
		self:给予道具(id, "元宵", "功能", 1)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了元宵")
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在神树中使用幸运抽奖功能时，居然抽到了#r/" .. "元宵")
	elseif self.奖励参数 <= 65 then
		self:给予道具(id, "蟠桃", "功能", 1)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了蟠桃")
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在神树中使用幸运抽奖功能时，居然抽到了#r/" .. "蟠桃")
	elseif self.奖励参数 <= 25 then
		self.生成等级 = 取随机数(10, 14) * 10

		玩家数据[id].道具:给予道具(id, "上古锻造图策", "功能", self.生成等级, 取上古锻造图策())
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了上古锻造图策")
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在神树中使用幸运抽奖功能时，居然抽到了#r/" .. "上古锻造图策")
	elseif self.奖励参数 <= 85 then
		self:给予道具(id, "高级魔兽要诀", "普通", 取高级兽诀名称())
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了一本高级魔兽要诀")
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在神树中使用幸运抽奖功能时，居然抽到了#r/" .. "高级魔兽要诀")
	elseif self.奖励参数 <= 55 then
		self.生成等级 = 取随机数(60, 150)

		玩家数据[id].道具:给予道具(id, "灵饰指南书", "打造", self.生成等级, 取灵饰指南书())
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了灵饰指南书")
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在神树中使用幸运抽奖功能时，居然抽到了#r/" .. "灵饰指南书")
	elseif self.奖励参数 <= 75 then
		self.生成等级 = 取随机数(60, 150)

		玩家数据[id].道具:给予道具(id, "元灵晶石", "打造", self.生成等级, 取元灵晶石())
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了元灵晶石")
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在神树中使用幸运抽奖功能时，居然抽到了#r/" .. "元灵晶石")
	elseif self.奖励参数 <= 110 then
		self:给予道具(id, "魔兽要诀", "普通", 取低级兽诀名称())
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了一本魔兽要诀")
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在神树中使用幸运抽奖功能时，居然抽到了#r/" .. "魔兽要诀")
	elseif self.奖励参数 <= 20 then
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在神树中使用幸运抽奖功能时，居然抽到了#r/" .. 玩家数据[id].装备:取150级随机装备(id))
	elseif self.奖励参数 <= 130 then
		if 玩家数据[id].召唤兽.数据.参战 == 0 then
			发送数据(玩家数据[id].连接id, 7, "#y/你的召唤兽尚未设置为参战状态，无法获得召唤兽经验")
		else
			玩家数据[id].召唤兽:添加经验(5000000, id, 玩家数据[id].召唤兽.数据.参战, 10)
			发送数据(玩家数据[id].连接id, 7, "#y/你的召唤兽获得了50万经验")
		end
	else
		玩家数据[id].角色:添加经验(10000000, id, 10)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了1000万经验")
	end
end

function 道具处理类:银子抽奖(id)
	if 银子检查(id, 20000000) then
		玩家数据[id].角色:扣除银子(id, 20000000, 26)

		self.奖励参数 = 取随机数(1, 120)

		if self.奖励参数 <= 15 then
			玩家数据[id].角色:添加随机坐骑(id)
			发送数据(玩家数据[id].连接id, 7, "#y/你获得了坐骑")
			广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在西凉女国摇钱树中使用幸运抽奖功能时，居然抽到了#r/" .. "坐骑")
		elseif self.奖励参数 <= 2 then
			self:给予道具(id, "蟠桃", "功能", 1)
			发送数据(玩家数据[id].连接id, 7, "#y/你获得了蟠桃")
			广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在西凉女国摇钱树中使用幸运抽奖功能时，居然抽到了#r/" .. "蟠桃")
		--elseif self.奖励参数 <= 999 then
		--	玩家数据[id].道具:给予道具(id组[n],"召唤兽内丹",取内丹(低级))
		--	发送数据(玩家数据[id].连接id, 7, "#y/你获得了召唤兽内丹")
		--	广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在西凉女国摇钱树中使用幸运抽奖功能时，居然抽到了#r/" .. "蟠桃")
		elseif self.奖励参数 <= 5 then
			self:给予道具(id, "神兜兜", "普通", 1)
			发送数据(玩家数据[id].连接id, 7, "#y/你获得了神兜兜")
			广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在西凉女国摇钱树中使用幸运抽奖功能时，居然抽到了#r/" .. "神兜兜")
		--elseif self.奖励参数 <= 8 then
		--	 玩家数据[id组[n]].道具:新加给予道具(id组[n],"高级召唤兽内丹",取内丹("高级"))
		--	发送数据(玩家数据[id].连接id, 7, "#y/你获得了高级召唤兽内丹")
		--	广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在西凉女国摇钱树中使用幸运抽奖功能时，居然抽到了#r/" .. "元宵")
		elseif self.奖励参数 <= 20 then
			self:给予道具(id, "元宵", "功能", 1)
			发送数据(玩家数据[id].连接id, 7, "#y/你获得了元宵")
			广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在西凉女国摇钱树中使用幸运抽奖功能时，居然抽到了#r/" .. "元宵")
		elseif self.奖励参数 <= 40 then
			self.宝石名称 = Q_宝图宝石[取随机数(1, #Q_宝图宝石)]

			玩家数据[id].道具:给予道具(id, self.宝石名称, "宝石", 取随机数(3, 6))
			发送数据(玩家数据[id].连接id, 7, "#y/你获得了" .. self.宝石名称)
			广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在西凉女国摇钱树中使用幸运抽奖功能时，居然抽到了#r/" .. self.宝石名称)
		elseif self.奖励参数 <= 45 then
			self.宝石名称 = Q_宝图宝石[取随机数(1, #Q_宝图宝石)]

			玩家数据[id].道具:给予道具(id, self.宝石名称, "宝石", 取随机数(3, 6))
			发送数据(玩家数据[id].连接id, 7, "#y/你获得了" .. self.宝石名称)
			广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在西凉女国摇钱树中使用幸运抽奖功能时，居然抽到了#r/" .. self.宝石名称)
		elseif self.奖励参数 <= 50 then
			self.随机名称 = 取随机书铁()
			self.任务链等级 = 取随机数(10, 12)

			玩家数据[id].道具:给予道具(id, self.随机名称, "打造", self.任务链等级, self.任务链等级, 取随机制造())

			self.任务链等级 = self.任务链等级 * 10

			发送数据(玩家数据[id].连接id, 7, "#y/你获得了" .. self.随机名称)
			广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在西凉女国摇钱树中使用幸运抽奖功能时，居然抽到了" .. self.任务链等级 .. "级的#r/" .. self.随机名称)
		elseif self.奖励参数 <= 60 then
			self:给予道具(id, "元宵", "功能", 1)
			发送数据(玩家数据[id].连接id, 7, "#y/你获得了元宵")
			广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在西凉女国摇钱树中使用幸运抽奖功能时，居然抽到了#r/" .. "元宵")
		elseif self.奖励参数 <= 15 then
			self.生成等级 = 取随机数(6, 12) * 10

			玩家数据[id].道具:给予道具(id, "上古锻造图策", "功能", self.生成等级, 取上古锻造图策())
			发送数据(玩家数据[id].连接id, 7, "#y/你获得了上古锻造图策")
			广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在西凉女国摇钱树中使用幸运抽奖功能时，居然抽到了#r/" .. "上古锻造图策")
		elseif self.奖励参数 <= 85 then
			self:给予道具(id, "高级魔兽要诀", "普通", 取高级兽诀名称())
			发送数据(玩家数据[id].连接id, 7, "#y/你获得了一本高级魔兽要诀")
			广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在西凉女国摇钱树中使用幸运抽奖功能时，居然抽到了#r/" .. "高级魔兽要诀")
		elseif self.奖励参数 <= 110 then
			self:给予道具(id, "魔兽要诀", "普通", 取低级兽诀名称())
			发送数据(玩家数据[id].连接id, 7, "#y/你获得了一本魔兽要诀")
			广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在西凉女国摇钱树中使用幸运抽奖功能时，居然抽到了#r/" .. "魔兽要诀")
		elseif self.奖励参数 <= 120 then
			self:给予道具(id, "天眼通符", "功能", 10)
			发送数据(玩家数据[id].连接id, 7, "#y/你获得了十个天眼通符")
			广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在西凉女国摇钱树中使用幸运抽奖功能时，居然抽到了#r/" .. "10个天眼通符")
		elseif self.奖励参数 <= 30 then
		self.随机银子 = 取随机数(5000000, 5000000)

		玩家数据[id].角色:添加银子(id, self.随机银子, 403)
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在西凉女国摇钱树中使用幸运抽奖功能时，居然抽到了#r/" .. "500W银子")
				elseif self.奖励参数 <= 25 then
		self.随机银子 = 取随机数(10000000, 10000000)

		玩家数据[id].角色:添加银子(id, self.随机银子, 403)
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在西凉女国摇钱树中使用幸运抽奖功能时，居然抽到了#r/" .. "500W银子")
				elseif self.奖励参数 <= 15 then
		self.随机银子 = 取随机数(20000000, 20000000)

		玩家数据[id].角色:添加银子(id, self.随机银子, 403)
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在西凉女国摇钱树中使用幸运抽奖功能时，居然抽到了#r/" .. "500W银子")
				elseif self.奖励参数 <= 10 then
		self.随机银子 = 取随机数(50000000, 50000000)

		玩家数据[id].角色:添加银子(id, self.随机银子, 403)
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在西凉女国摇钱树中使用幸运抽奖功能时，居然抽到了#r/" .. "500W银子")
				elseif self.奖励参数 <= 2 then
		self.随机银子 = 取随机数(100000000, 100000000)

		玩家数据[id].角色:添加银子(id, self.随机银子, 403)
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在西凉女国摇钱树中使用幸运抽奖功能时，居然抽到了#r/" .. "500W银子")
		elseif self.奖励参数 <= 110 then
			if 玩家数据[id].召唤兽.数据.参战 == 0 then
				发送数据(玩家数据[id].连接id, 7, "#y/你的召唤兽尚未设置为参战状态，无法获得召唤兽经验")
			else
				玩家数据[id].召唤兽:添加经验(5000000, id, 玩家数据[id].召唤兽.数据.参战, 10)
				发送数据(玩家数据[id].连接id, 7, "#y/你的召唤兽获得了500万经验")
			end
		else
			玩家数据[id].角色:添加经验(10000000, id, 10)
			发送数据(玩家数据[id].连接id, 7, "#y/你获得了1000万经验")
		end
	else
		发送数据(玩家数据[id].连接id, 7, "#y/你没有那么多的银子")
	end
end

function 道具处理类:光棍节奖励(id)
	self.奖励参数 = 取随机数(1, 120)

	if self.奖励参数 <= 20 then
		self:给予道具(id, "高级魔兽要诀", "普通", 取高级兽诀名称())
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了一高级本魔兽要诀")
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在在线奖励中获得了#r/" .. "高级魔兽要诀")
	elseif self.奖励参数 <= 40 then
		self.随机银子 = 取随机数(500000, 2000000)

		玩家数据[id].角色:添加银子(id, self.随机银子, 403)
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/获得了游戏管理员赠送的开服红包，打开一看竟是#r/" .. self.随机银子 .. "#y/两银子")
	elseif self.奖励参数 <= 80 then
		self.随机银子 = 取随机数(1000000, 5000000)

		玩家数据[id].角色:添加储备(id, self.随机银子, 403)
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/获得了游戏管理员赠送的开服红包，打开一看竟是#r/" .. self.随机银子 .. "#y/储备")
	else
		self.随机银子 = 取随机数(500000, 2000000)

		玩家数据[id].角色:添加银子(id, self.随机银子, 403)
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/获得了游戏管理员赠送的开服红包，打开一看竟是#r/" .. self.随机银子 .. "#y/两银子")
	end
end

function 道具处理类:宝藏山奖励(id)
	self.奖励参数 = 取随机数(1, 120)

	if self.奖励参数 <= 5 then
		self:给予道具(id, "月华露", "功能", 1500)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了月华露")
		广播消息("#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在宝藏山挖宝时，居然挖到了#r/" .. "月华露")
	elseif self.奖励参数 <= 10 then
		广播消息("#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在宝藏山挖宝时，居然挖到了#r/" .. self:给予随机五宝(id))
	elseif self.奖励参数 <= 18 then
		self:给予道具(id, "月华露", "功能", 1000)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了月华露")
		广播消息("#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在宝藏山挖宝时，居然挖到了#r/" .. "月华露")
	elseif self.奖励参数 <= 28 then
		self:给予道具(id, "超级金柳露", "普通", 1)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了超级金柳露")
	elseif self.奖励参数 <= 38 then
		self:给予道具(id, "花豆", "普通", 1)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了花豆")
	elseif self.奖励参数 <= 48 then
		self.强化石名称 = 取强化石()

		self:给予道具(id, self.强化石名称, "普通", 1)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了" .. self.强化石名称)
		广播消息("#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在宝藏山挖宝时，居然挖到了#r/" .. self.强化石名称)
	elseif self.奖励参数 <= 58 then
		self:给予道具(id, "天眼通符", "功能", 1)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了" .. "天眼通符")
	elseif self.奖励参数 <= 68 then
		self.宝石名称 = Q_宝图宝石[取随机数(1, #Q_宝图宝石)]

		玩家数据[id].道具:给予道具(id, self.宝石名称, "宝石", 取随机数(1, 1))
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了" .. self.宝石名称)
		广播消息("#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在宝藏山挖宝时，居然挖到了#r/" .. self.宝石名称)
	elseif self.奖励参数 <= 78 then
		任务处理类:设置封妖任务(id, 1, 1)
	elseif self.奖励参数 <= 100 then
		self:给予道具(id, "珍珠", "普通", 取随机数(6, 12) * 10)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了珍珠")
		广播消息("#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在宝藏山挖宝时，居然挖到了#r/" .. "珍珠")
	else
		self.临时经验 = 玩家数据[id].角色.数据.等级 * 玩家数据[id].角色.数据.等级 * 5

		发送数据(玩家数据[id].连接id, 7, "#y/你获得了" .. self.临时经验 .. "点经验")
		玩家数据[id].角色:添加经验(self.临时经验, id, 11)
	end
end
function 道具处理类:金银宝盒处理(id, 数据)
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]

	if self.数据[self.临时id1] == nil or self.数据[self.临时id1] == 0 or self.数据[self.临时id1].名称 ~= "金银宝盒" then
		发送数据(玩家数据[id].连接id, 7, "#y/你似乎并没有这样的道具")

		return 0
	end
    if 玩家数据[id].角色.数据.帮派==nil then
       发送数据(玩家数据[id].连接id, 7, "#y/帮派都没有你用个毛啊？")

        return 0
    end



	self.帮派id = 玩家数据[id].角色.数据.帮派

	if self.帮派id ~= nil and 帮派数据[self.帮派id] ~= nil then
		帮派数据[self.帮派id].资金 = 帮派数据[self.帮派id].资金 + 50000

		if 帮派数据[self.帮派id].资金 > 帮派数据[self.帮派id].金库 * 5000000 + 3000000 then
			帮派数据[self.帮派id].资金 = 帮派数据[self.帮派id].金库 * 5000000 + 3000000
		end

		帮派数据[self.帮派id].繁荣 = 帮派数据[self.帮派id].繁荣 + 1
		帮派数据[self.帮派id].成员名单[id].帮贡.获得 = 帮派数据[self.帮派id].成员名单[id].帮贡.获得 + 5
		帮派数据[self.帮派id].成员名单[id].帮贡.当前 = 帮派数据[self.帮派id].成员名单[id].帮贡.当前 + 5
		玩家数据[id].角色.数据.帮贡 = 帮派数据[self.帮派id].成员名单[id].帮贡.当前
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了5点帮贡")
		广播帮派消息(玩家数据[id].角色.数据.帮派, "#bp/#w/" .. 玩家数据[id].角色.数据.名称 .. "#w/上交了金银宝盒，为帮派增加了50000两的资金。")
	end

	if 玩家数据[id].道具.数据[self.临时id1].数量 == nil then
		玩家数据[id].道具.数据[self.临时id1].数量 = 1
	end

	玩家数据[id].道具.数据[self.临时id1].数量 = 玩家数据[id].道具.数据[self.临时id1].数量 - 1

	if 玩家数据[id].道具.数据[self.临时id1].数量 <= 0 then
		玩家数据[id].道具.数据[self.临时id1] = 0
		玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil
	end

	发送数据(玩家数据[id].连接id, 3006, "66")
end

function 道具处理类:小雪块处理(id, 数据)
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]

	if self.数据[self.临时id1] == nil or self.数据[self.临时id1] == 0 or self.数据[self.临时id1].名称 ~= "小雪块" then
		发送数据(玩家数据[id].连接id, 7, "#y/你似乎并没有这样的道具")

		return 0
	end

	self.奖励参数 = 取随机数()
	if 玩家数据[id].角色.数据.雪人积分 == nil then
					玩家数据[id].角色.数据.雪人积分 = {
						累积 = 0,
						当前 = 0
					}
				end
				玩家数据[id].角色.数据.雪人积分.当前 = 玩家数据[id].角色.数据.雪人积分.当前 + 1
				玩家数据[id].角色.数据.雪人积分.累积 = 玩家数据[id].角色.数据.雪人积分.累积 + 1
				发送数据(玩家数据[id].连接id, 7, "#y/你获得了1点雪人积分！")
	if self.奖励参数 <= 50 then
		self:给予道具(id, "魔兽要诀", "普通", 取低级兽诀名称())
	else
		玩家数据[id].角色:添加储备(id,500000,"储备礼包")
	end

	if 玩家数据[id].道具.数据[self.临时id1].数量 == nil then
		玩家数据[id].道具.数据[self.临时id1].数量 = 1
	end

	玩家数据[id].道具.数据[self.临时id1].数量 = 玩家数据[id].道具.数据[self.临时id1].数量 - 1

	if 玩家数据[id].道具.数据[self.临时id1].数量 <= 0 then
		玩家数据[id].道具.数据[self.临时id1] = 0
		玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil
	end

	发送数据(玩家数据[id].连接id, 3006, "66")
end

function 道具处理类:大雪块处理(id, 数据)
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]

	if self.数据[self.临时id1] == nil or self.数据[self.临时id1] == 0 or self.数据[self.临时id1].名称 ~= "大雪块" then
		发送数据(玩家数据[id].连接id, 7, "#y/你似乎并没有这样的道具")

		return 0
	end

	self.奖励参数 = 取随机数()
	if 玩家数据[id].角色.数据.雪人积分 == nil then
					玩家数据[id].角色.数据.雪人积分 = {
						累积 = 0,
						当前 = 0
					}
				end
				玩家数据[id].角色.数据.雪人积分.当前 = 玩家数据[id].角色.数据.雪人积分.当前 + 5
				玩家数据[id].角色.数据.雪人积分.累积 = 玩家数据[id].角色.数据.雪人积分.累积 + 5
				发送数据(玩家数据[id].连接id, 7, "#y/你获得了5点雪人积分！")
	if self.奖励参数 <= 5 then
       self:给予道具(id, "神兜兜", "普通", 1)
	elseif	self.奖励参数 <= 45 then
		self:给予道具(id, "高级魔兽要诀", "普通", 取高级兽诀名称())
	else
		self.随机银子 = 取随机数(500000, 2000000)
		玩家数据[id].角色:添加银子(id, self.随机银子, 403)
		广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/使用了雪人活动的大雪块获得#r/" .. self.随机银子 .. "#y/两银子")
	end

	if 玩家数据[id].道具.数据[self.临时id1].数量 == nil then
		玩家数据[id].道具.数据[self.临时id1].数量 = 1
	end

	玩家数据[id].道具.数据[self.临时id1].数量 = 玩家数据[id].道具.数据[self.临时id1].数量 - 1

	if 玩家数据[id].道具.数据[self.临时id1].数量 <= 0 then
		玩家数据[id].道具.数据[self.临时id1] = 0
		玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil
	end

	发送数据(玩家数据[id].连接id, 3006, "66")
end

function 道具处理类:修炼果处理(id, 数据)
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]

	if self.数据[self.临时id1] == nil or self.数据[self.临时id1] == 0 or self.数据[self.临时id1].名称 ~= "修炼果" then
		发送数据(玩家数据[id].连接id, 7, "#y/你似乎并没有这样的道具")

		return 0
	end

	if 玩家数据[id].角色.数据.召唤兽修炼[玩家数据[id].角色.数据.召唤兽修炼.当前].上限 <= 玩家数据[id].角色.数据.召唤兽修炼[玩家数据[id].角色.数据.召唤兽修炼.当前].等级 then
		发送数据(玩家数据[id].连接id, 7, "#y/你当前的这项修炼等级已达上限，无法再服用修炼果")

		return 0
	end

	if 玩家数据[id].道具.数据[self.临时id1].数量 == nil then
		玩家数据[id].道具.数据[self.临时id1].数量 = 1
	end

	玩家数据[id].道具.数据[self.临时id1].数量 = 玩家数据[id].道具.数据[self.临时id1].数量 - 1

	if 玩家数据[id].道具.数据[self.临时id1].数量 <= 0 then
		玩家数据[id].道具.数据[self.临时id1] = 0
		玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil
	end

	玩家数据[id].角色:添加召唤兽修炼经验(150)
	发送数据(玩家数据[id].连接id, 3006, "66")
end

function 道具处理类:完成交易(id, 交易id)
	if 交易数据[交易id] == nil or 交易数据[交易id].结束标记 ~= nil then
		发送数据(玩家数据[id].连接id, 7, "#w/这样的交易并不存在")

		交易数据[交易id] = nil

		return 0
	elseif 交易数据[交易id].确认[1] == false or 交易数据[交易id].确认[2] == false then
		发送数据(玩家数据[id].连接id, 7, "#w/双方尚未锁定交易，无法完成交易")

		return 0
	elseif 交易数据[交易id].发起方.id == id then
		交易数据[交易id].确定[1] = true

		if 交易数据[交易id].确定[2] == false then
			发送数据(玩家数据[id].连接id, 7, "#y/请等待对方确认交易")

			return 0
		end
	else
		交易数据[交易id].确定[2] = true

		if 交易数据[交易id].确定[1] == false then
			发送数据(玩家数据[id].连接id, 7, "#y/请等待对方确认交易")

			return 0
		end
	end
	self.接受id = 交易数据[交易id].接受方.id
	self.发起id = 交易数据[交易id].发起方.id
	self.禁止交易 = false
	self.禁止提示语 = ""

	for n = 1, #交易数据[交易id].发起方.召唤兽 do
		for i = 1, #玩家数据[self.发起id].召唤兽.数据[交易数据[交易id].发起方.召唤兽[n]].装备 do
			if 玩家数据[self.发起id].召唤兽.数据[交易数据[交易id].发起方.召唤兽[n]].装备[i] ~= nil then
				self.禁止提示语 = "#y/" .. 玩家数据[self.发起id].角色.数据.名称 .. "的召唤兽携带有装备，无法交易"
				self.禁止交易 = true
			end
		end
	end

	for n = 1, #交易数据[交易id].接受方.召唤兽 do
		for i = 1, #玩家数据[self.接受id].召唤兽.数据[交易数据[交易id].接受方.召唤兽[n]].装备 do
			if 玩家数据[self.接受id].召唤兽.数据[交易数据[交易id].接受方.召唤兽[n]].装备[i] ~= nil then
				self.禁止提示语 = "#y/" .. 玩家数据[self.接受id].角色.数据.名称 .. "的召唤兽携带有装备，无法交易"
				self.禁止交易 = true
			end
		end
	end

	if 交易数据[交易id].接受方.银两 == "" or 交易数据[交易id].接受方.银两 == nil then
		交易数据[交易id].接受方.银两 = 0
	end

	if 交易数据[交易id].发起方.银两 == "" or 交易数据[交易id].发起方.银两 == nil then
		交易数据[交易id].发起方.银两 = 0
	end

	if 取角色银子(self.发起id) < 交易数据[交易id].发起方.银两 or 交易数据[交易id].发起方.银两 < 0 then
		self.禁止提示语 = "#y/" .. 玩家数据[self.发起id].角色.数据.名称 .. "没有那么多的银子"
		self.禁止交易 = true
	elseif 取角色银子(self.接受id) < 交易数据[交易id].接受方.银两 or 交易数据[交易id].接受方.银两 < 0 then
		self.禁止提示语 = "#y/" .. 玩家数据[self.接受id].角色.数据.名称 .. "没有那么多的银子"
		self.禁止交易 = true
	end

	if self.禁止交易 == false then
		self.格子总数 = #交易数据[交易id].发起方.道具
		self.格子数量 = 玩家数据[交易数据[交易id].发起方.id].角色:取可用格子数量("包裹")

		if self.格子数量 < self.格子总数 then
			self.禁止提示语 = "#y/" .. 玩家数据[self.发起id].角色.数据.名称 .. "身上的道具空间不足"
			self.禁止交易 = true
		end

		self.格子总数 = #交易数据[交易id].接受方.道具
		self.格子数量 = 玩家数据[交易数据[交易id].接受方.id].角色:取可用格子数量("包裹")

		if self.格子数量 < self.格子总数 then
			self.禁止提示语 = "#y/" .. 玩家数据[self.接受id].角色.数据.名称 .. "身上的道具空间不足"
			self.禁止交易 = true
		end

		self.格子总数 = #交易数据[交易id].接受方.召唤兽
		self.格子数量 = #玩家数据[交易数据[交易id].发起方.id].召唤兽.数据

		if self.格子总数 + self.格子数量 > 5 then
			self.禁止提示语 = "#y/" .. 玩家数据[self.发起id].角色.数据.名称 .. "无法携带更多的召唤兽了"
			self.禁止交易 = true
		end

		self.格子总数 = #交易数据[交易id].发起方.召唤兽
		self.格子数量 = #玩家数据[交易数据[交易id].接受方.id].召唤兽.数据

		if self.格子总数 + self.格子数量 > 5 then
			self.禁止提示语 = "#y/" .. 玩家数据[self.接受id].角色.数据.名称 .. "无法携带更多的召唤兽了"
			self.禁止交易 = true
		end

		if self.禁止交易 == false then
			交易数据[交易id].结束标记 = true
			self.发起名称 = 玩家数据[self.发起id].角色.数据.名称
			self.接受名称 = 玩家数据[self.接受id].角色.数据.名称

			if 交易数据[交易id].接受方.银两 > 0 then
				玩家数据[self.发起id].角色:添加银子(self.发起id, 交易数据[交易id].接受方.银两, 11)
				玩家数据[self.发起id].角色:添加消费日志("[交易]获得" .. 玩家数据[self.接受id].账号 .. "的" .. 交易数据[交易id].接受方.银两 .. "两银子")
				玩家数据[self.接受id].角色:扣除银子(self.接受id, 交易数据[交易id].接受方.银两, 12)
				玩家数据[self.接受id].角色:添加消费日志("[交易]给予" .. 玩家数据[self.发起id].账号 .. "的" .. 交易数据[交易id].接受方.银两 .. "两银子")
				发送数据(玩家数据[self.发起id].连接id, 7, "#y/" .. self.接受名称 .. "给了你" .. 交易数据[交易id].接受方.银两 .. "两银子")
			end

			if 交易数据[交易id].发起方.银两 > 0 then
				玩家数据[self.接受id].角色:添加银子(self.接受id, 交易数据[交易id].发起方.银两, 11)
				玩家数据[self.接受id].角色:添加消费日志("[交易]获得" .. 玩家数据[self.发起id].账号 .. "的" .. 交易数据[交易id].发起方.银两 .. "两银子")
				玩家数据[self.发起id].角色:扣除银子(self.发起id, 交易数据[交易id].发起方.银两, 12)
				玩家数据[self.发起id].角色:添加消费日志("[交易]给予" .. 玩家数据[self.接受id].账号 .. "的" .. 交易数据[交易id].发起方.银两 .. "两银子")
				发送数据(玩家数据[self.接受id].连接id, 7, "#y/" .. self.发起名称 .. "给了你" .. 交易数据[交易id].发起方.银两 .. "两银子")
			end

			for n = 1, #交易数据[交易id].发起方.道具 do
				if 玩家数据[self.发起id].角色.数据.道具.包裹[交易数据[交易id].发起方.道具[n]] ~= nil then
					self.临时格子 = 玩家数据[self.接受id].角色:取可用道具格子("包裹")
					self.临时道具 = 玩家数据[self.接受id].道具:取道具编号()
					self.临时数据 = table.tostring(玩家数据[self.发起id].道具.数据[玩家数据[self.发起id].角色.数据.道具.包裹[交易数据[交易id].发起方.道具[n]]])
					玩家数据[self.接受id].道具.数据[self.临时道具] = table.loadstring(self.临时数据)
					玩家数据[self.接受id].角色.数据.道具.包裹[self.临时格子] = self.临时道具

					发送数据(玩家数据[self.接受id].连接id, 7, "#y/" .. self.发起名称 .. "给了你" .. 玩家数据[self.接受id].道具.数据[self.临时道具].名称)
					玩家数据[self.发起id].角色:添加消费日志("[交易]给予" .. 玩家数据[self.接受id].账号 .. "的" .. 玩家数据[self.接受id].道具.数据[self.临时道具].名称)
					玩家数据[self.接受id].角色:添加消费日志("[交易]获得" .. 玩家数据[self.发起id].账号 .. "的" .. 玩家数据[self.接受id].道具.数据[self.临时道具].名称)

					玩家数据[self.发起id].道具.数据[玩家数据[self.发起id].角色.数据.道具.包裹[交易数据[交易id].发起方.道具[n]]] = nil
					玩家数据[self.发起id].角色.数据.道具.包裹[交易数据[交易id].发起方.道具[n]] = nil
				end
			end

			for n = 1, #交易数据[交易id].接受方.道具 do
				if 玩家数据[self.接受id].角色.数据.道具.包裹[交易数据[交易id].接受方.道具[n]] ~= nil then
					self.临时格子 = 玩家数据[self.发起id].角色:取可用道具格子("包裹")
					self.临时道具 = 玩家数据[self.发起id].道具:取道具编号()
					self.临时数据 = table.tostring(玩家数据[self.接受id].道具.数据[玩家数据[self.接受id].角色.数据.道具.包裹[交易数据[交易id].接受方.道具[n]]])
					玩家数据[self.发起id].道具.数据[self.临时道具] = table.loadstring(self.临时数据)
					玩家数据[self.发起id].角色.数据.道具.包裹[self.临时格子] = self.临时道具

					发送数据(玩家数据[self.发起id].连接id, 7, "#y/" .. self.接受名称 .. "给了你" .. 玩家数据[self.发起id].道具.数据[self.临时道具].名称)
					玩家数据[self.接受id].角色:添加消费日志("[交易]给予" .. 玩家数据[self.发起id].账号 .. "的" .. 玩家数据[self.发起id].道具.数据[self.临时道具].名称)
					玩家数据[self.发起id].角色:添加消费日志("[交易]获得" .. 玩家数据[self.接受id].账号 .. "的" .. 玩家数据[self.发起id].道具.数据[self.临时道具].名称)

					玩家数据[self.接受id].道具.数据[玩家数据[self.接受id].角色.数据.道具.包裹[交易数据[交易id].接受方.道具[n]]] = nil
					玩家数据[self.接受id].角色.数据.道具.包裹[交易数据[交易id].接受方.道具[n]] = nil
				end
			end

			发送数据(玩家数据[self.发起id].连接id, 3008, "1")
			发送数据(玩家数据[self.接受id].连接id, 3008, "1")

			self.发起方召唤 = false
			self.接受方召唤 = false

			for n = 1, #交易数据[交易id].发起方.召唤兽 do
				self.临时数据 = table.tostring(玩家数据[self.发起id].召唤兽.数据[交易数据[交易id].发起方.召唤兽[n]])
				玩家数据[self.发起id].召唤兽.数据[交易数据[交易id].发起方.召唤兽[n]] = "1"
				玩家数据[self.接受id].召唤兽.数据[#玩家数据[self.接受id].召唤兽.数据 + 1] = table.loadstring(self.临时数据)

				发送数据(玩家数据[self.接受id].连接id, 7, "#y/" .. self.发起名称 .. "给了你" .. 玩家数据[self.接受id].召唤兽.数据[#玩家数据[self.接受id].召唤兽.数据].名称)
				玩家数据[self.发起id].角色:添加消费日志("[交易]给予" .. 玩家数据[self.接受id].账号 .. "的" .. 玩家数据[self.接受id].召唤兽.数据[#玩家数据[self.接受id].召唤兽.数据].名称)
				玩家数据[self.接受id].角色:添加消费日志("[交易]获得" .. 玩家数据[self.发起id].账号 .. "的" .. 玩家数据[self.接受id].召唤兽.数据[#玩家数据[self.接受id].召唤兽.数据].名称)

				self.发起方召唤 = true
			end

			for n = 1, #交易数据[交易id].接受方.召唤兽 do
				self.临时数据 = table.tostring(玩家数据[self.接受id].召唤兽.数据[交易数据[交易id].接受方.召唤兽[n]])
				玩家数据[self.接受id].召唤兽.数据[交易数据[交易id].接受方.召唤兽[n]] = "1"
				玩家数据[self.发起id].召唤兽.数据[#玩家数据[self.发起id].召唤兽.数据 + 1] = table.loadstring(self.临时数据)

				发送数据(玩家数据[self.发起id].连接id, 7, "#y/" .. self.接受名称 .. "给了你" .. 玩家数据[self.发起id].召唤兽.数据[#玩家数据[self.发起id].召唤兽.数据].名称)
				玩家数据[self.接受id].角色:添加消费日志("[交易]给予" .. 玩家数据[self.发起id].账号 .. "的" .. 玩家数据[self.发起id].召唤兽.数据[#玩家数据[self.发起id].召唤兽.数据].名称)
				玩家数据[self.发起id].角色:添加消费日志("[交易]获得" .. 玩家数据[self.接受id].账号 .. "的" .. 玩家数据[self.发起id].召唤兽.数据[#玩家数据[self.发起id].召唤兽.数据].名称)

				self.接受方召唤 = true
			end

			if self.发起方召唤 then
				self.临时bb = {}
				self.参战标记 = 0

				for n = 1, #玩家数据[self.发起id].召唤兽.数据 do
					if 玩家数据[self.发起id].召唤兽.数据[n] ~= nil and 玩家数据[self.发起id].召唤兽.数据[n] ~= "1" and 玩家数据[self.发起id].召唤兽.数据[n].造型 ~= nil then
						if 玩家数据[self.发起id].召唤兽.数据.参战 == n then
							玩家数据[self.发起id].召唤兽.数据[n].参战标记 = true
						end

						self.临时bb[#self.临时bb + 1] = table.tostring(玩家数据[self.发起id].召唤兽.数据[n])
					end
				end

				玩家数据[self.发起id].召唤兽.数据 = {
					参战 = 0
				}

				for n = 1, #self.临时bb do
					玩家数据[self.发起id].召唤兽.数据[n] = table.loadstring(self.临时bb[n])

					if 玩家数据[self.发起id].召唤兽.数据[n].参战标记 then
						玩家数据[self.发起id].召唤兽.数据.参战 = n
					end
				end

				发送数据(玩家数据[self.发起id].连接id, 2017, 玩家数据[self.发起id].角色:获取角色数据())
				发送数据(玩家数据[self.发起id].连接id, 2005, 玩家数据[self.发起id].召唤兽:取头像数据())
			end

			if self.接受方召唤 then
				self.临时bb = {}
				self.参战标记 = 0

				for n = 1, #玩家数据[self.接受id].召唤兽.数据 do
					if 玩家数据[self.接受id].召唤兽.数据[n] ~= nil and 玩家数据[self.接受id].召唤兽.数据[n] ~= 1 and 玩家数据[self.接受id].召唤兽.数据[n].造型 ~= nil then
						if 玩家数据[self.接受id].召唤兽.数据.参战 == n then
							玩家数据[self.接受id].召唤兽.数据[n].参战标记 = true
						end

						self.临时bb[#self.临时bb + 1] = table.tostring(玩家数据[self.接受id].召唤兽.数据[n])
					end
				end

				玩家数据[self.接受id].召唤兽.数据 = {
					参战 = 0
				}

				for n = 1, #self.临时bb do
					玩家数据[self.接受id].召唤兽.数据[n] = table.loadstring(self.临时bb[n])

					if 玩家数据[self.接受id].召唤兽.数据[n].参战标记 then
						玩家数据[self.接受id].召唤兽.数据.参战 = n
					end
				end

				发送数据(玩家数据[self.接受id].连接id, 2017, 玩家数据[self.接受id].角色:获取角色数据())
				发送数据(玩家数据[self.接受id].连接id, 2005, 玩家数据[self.接受id].召唤兽:取头像数据())
			end
		end
	end

	if self.禁止交易 then
		发送数据(玩家数据[self.发起id].连接id, 7, self.禁止提示语)
		发送数据(玩家数据[self.接受id].连接id, 7, self.禁止提示语)
	end

	发送数据(玩家数据[self.发起id].连接id, 3018, "1")
	发送数据(玩家数据[self.接受id].连接id, 3018, "1")

	交易数据[交易id] = nil
	玩家数据[self.发起id].交易 = 0
	玩家数据[self.接受id].交易 = 0
end

function 道具处理类:锁定交易(id, id1, 银子, 数据)
	self.临时数据 = 分割文本(数据, "@-@")
	local 数据 = {
		id = id1,
		银两 = 银子,
		道具 = {},
		召唤兽 = {}
	}
	self.临时数据1 = 分割文本(self.临时数据[1], "*-*")

	for n = 1, 3 do
		数据.道具[n] = self.临时数据1[n] + 0
	end

	self.临时数据1 = 分割文本(self.临时数据[2], "*-*")

	for n = 1, 3 do
		数据.召唤兽[n] = self.临时数据1[n] + 0
	end

	if 交易数据[数据.id] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/这样的交易并不存在")
		发送数据(玩家数据[id].连接id, 3016, "66")

		return 0
	else
		if 交易数据[数据.id].发起方.id == id then
			if 交易数据[数据.id].确认[1] then
				发送数据(玩家数据[id].连接id, 7, "#y/你已经锁定了交易，无法进行二次锁定")

				return 0
			end

			self.交易id = "发起方"
			self.接受id = "接受方"
			交易数据[数据.id].确认[1] = true
		else
			if 交易数据[数据.id].确认[2] then
				发送数据(玩家数据[id].连接id, 7, "#y/你已经锁定了交易，无法进行二次锁定")

				return 0
			end

			self.交易id = "接受方"
			self.接受id = "发起方"
			交易数据[数据.id].确认[2] = true
		end

		self.发送信息 = {
			道具 = {},
			召唤兽 = {},
			银两 = 数据.银两
		}

		for n = 1, 3 do
			if 数据.道具[n] ~= 0 then
				交易数据[数据.id][self.交易id].道具[#交易数据[数据.id][self.交易id].道具 + 1] = 数据.道具[n]
				self.发送信息.道具[#self.发送信息.道具 + 1] = 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具.包裹[数据.道具[n]]]
			end
		end

		for n = 1, 3 do
			if 数据.召唤兽[n] ~= 0 then
				交易数据[数据.id][self.交易id].召唤兽[#交易数据[数据.id][self.交易id].召唤兽 + 1] = 数据.召唤兽[n]
				self.发送信息.召唤兽[#self.发送信息.召唤兽 + 1] = 玩家数据[id].召唤兽.数据[数据.召唤兽[n]]
			end
		end

		交易数据[数据.id][self.交易id].银两 = 数据.银两

		发送数据(玩家数据[交易数据[数据.id][self.接受id].id].连接id, 3017, self.发送信息)
		发送数据(玩家数据[id].连接id, 7, "#y/你锁定了交易状态，你将无法修改交易中的东西")
		发送数据(玩家数据[交易数据[数据.id][self.接受id].id].连接id, 7, "#y/" .. 玩家数据[id].角色.数据.名称 .. "锁定了交易状态")
	end
end



function 道具处理类:点化装备处理(id,数据)
   local 套装效果 ={
    {"知己知彼","似玉生香","三味真火","日月乾坤","镇妖","尸腐毒","阎罗令","百万神兵","勾魂","判官令","雷击","魔音摄魂","天崩地裂",
    "摄魄","紧箍咒","落岩","含情脉脉","姐妹同心","日光华","水攻","威慑","唧唧歪歪","靛沧海","烈火","催眠符","龙卷雨击",
    "巨岩破","奔雷咒","失心符","龙腾","苍茫树","泰山压顶","落魄符","龙吟","地裂火","水漫金山","定身符","五雷咒","后发制人",
    "地狱烈火","满天花雨","飞砂走石","横扫千军","落叶萧萧","尘土刃","荆棘舞","冰川怒","夺命咒","夺魄令","浪涌","裂石"},
    {"盘丝阵","定心术","极度疯狂","金刚护法","生命之泉","魔王回首","幽冥鬼眼","楚楚可怜","百毒不侵","变身",
    "普渡众生","炼气化神","修罗隐身","杀气诀","一苇渡江","碎星诀","明光宝烛"},
    {"巨蛙","狐狸精","黑熊精","凤凰","大海龟","老虎","僵尸","蛟龙","护卫","黑熊","牛头","雨师","树怪","花妖","马面",
    "如意仙子","赌徒","牛妖","雷鸟人","芙蓉仙子","强盗","小龙女","蝴蝶仙子","巡游天神","海毛虫","野鬼","古代瑞兽",
    "星灵仙子","大蝙蝠","狼","白熊","幽灵","山贼","虾兵","黑山老妖","鬼将","野猪","蟹将","天兵","吸血鬼","骷髅怪",
    "龟丞相","天将","净瓶女娲","羊头怪","兔子怪","地狱战神","律法女娲","蛤蟆精","蜘蛛精","风伯","灵符女娲","画魂",
    "噬天虎" ,"巴蛇"   ,"巨力神猿","幽萤娃娃","踏云兽","葫芦宝贝","修罗傀儡鬼","大力金刚","红萼仙子","修罗傀儡妖",
    "雾中仙","龙龟","金身罗汉","灵鹤","机关兽","蝎子精","藤蔓妖花","夜罗刹","机关鸟","混沌兽","曼珠沙华","炎魔神",
    "连弩车","长眉灵猴","蜃气妖"}}
    local 道具id=玩家数据[id].角色.数据.道具.包裹[数据.格子]
    local 临时装备类型=玩家数据[id].道具.数据[道具id].类型
     local 道具等级=玩家数据[id].道具.数据[道具id].等级
    if 临时装备类型~="武器" and 临时装备类型~="衣服" and 临时装备类型~="头盔" and 临时装备类型~="项链" and 临时装备类型~="腰带" and 临时装备类型~="鞋子"then
         发送数据(玩家数据[id].连接id,7,"#Y/只有装备才可点化")
         return 0
    elseif 玩家数据[id].道具.数据[道具id].鉴定==false then
          发送数据(玩家数据[id].连接id,7,"#Y/未鉴定的装备不可以点化")
         return 0
    -- elseif 道具等级>90 then
    --    发送数据(玩家数据[id].连接id,7,"#Y/在我这里只能点化80级以下的装备")
    --    return 0
    end
   if 银子检查(id,道具等级*200)==false then
      发送数据(玩家数据[id].连接id,7,"#Y/本次操作需要消耗"..(道具等级*200).."两银子")
      return 0
    end
     玩家数据[id].角色:扣除银子(id,道具等级*200,"点化装备")
   if 取随机数()<=70 then
     local 点化类型=取随机数(1,2)
        if 点化类型==1 then
        玩家数据[id].道具.数据[道具id].套装={类型="追加法术",名称=套装效果[点化类型][取随机数(1,#套装效果[点化类型])]}
        elseif 点化类型==2 then
        玩家数据[id].道具.数据[道具id].套装={类型="附加状态",名称=套装效果[点化类型][取随机数(1,#套装效果[点化类型])]}
        -- elseif 点化类型==3 then
        -- 玩家数据[id].道具.数据[道具id].套装={类型="变身术",名称=套装效果[点化类型][取随机数(1,#套装效果[点化类型])]}
        end
        发送数据(玩家数据[id].连接id,7,"#y/点化装备成功！")
        发送数据(玩家数据[id].连接id,3006,"66")
    else
      发送数据(玩家数据[id].连接id,7,"#y/点化装备失败")
    end
 end

function 道具处理类:炼药处理(id)
	self.中医等级 = 玩家数据[id].角色.数据.辅助技能.中药医理

	if self.中医等级 < 10 then
		发送数据(玩家数据[id].连接id, 7, "#y/你的中药医理技能未足10级，无法进行炼药")

		return 0
	elseif 玩家数据[id].角色.数据.当前活力 < self.中医等级 * 0.35 + 10 then
		发送数据(玩家数据[id].连接id, 7, "#y/你没那么多的活力")

		return 0
	elseif 银子检查(id, 5000) == false then
		发送数据(玩家数据[id].连接id, 7, "#y/本次炼药需要消耗5000两银子")

		return 0
	else
		玩家数据[id].角色:扣除银子(id, 5000, 10)
		玩家数据[id].角色:扣除活力(2, self.中医等级 * 0.15 + 10)

		self.三药几率 = self.中医等级 / 2 + 25

		if self.三药几率 > 90 then
			self.三药几率 = 90
		end

		if 取随机数() <= self.三药几率 then
			self.炼药名称 = {
				"小还丹",
				"金香玉",
				"千年保心丹",
				"佛光舍利子",
				"九转回魂丹",
				"蛇蝎美人",
				"十香返生丸",
				"风水混元丹",
				"五龙丹"
			}
		else
			self.炼药名称 = {
				"金创药",
				"金创药",
				"金创药",
				"金创药",
				"金创药",
				"金创药",
				"金创药",
				"金创药"
			}
		end

		self.药品名称 = self.炼药名称[取随机数(1, #self.炼药名称)]
		self.可用格子 = 玩家数据[id].角色:取可用道具格子("包裹")

		if self.可用格子 == 0 then
			发送数据(玩家数据[id].连接id, 7, "#y/你身上已经没有多余的道具格子了")

			return 0
		end

		self.可用id = self:取道具编号()
		self.数据[self.可用id] = {
			类型 = "药品",
			等级 = 3,
			名称 = self.药品名称,
			品质 = 取随机数(math.floor(self.中医等级 * 1.5), math.floor(self.中医等级 * 2))
		}
		玩家数据[id].角色.数据.道具.包裹[self.可用格子] = self.可用id

		发送数据(玩家数据[id].连接id, 7, "#y/你炼制出了#r/" .. self.药品名称)
		发送数据(玩家数据[id].连接id, 3006, "66")
	end
end

function 道具处理类:烹饪处理(id)
	self.中医等级 = 玩家数据[id].角色.数据.辅助技能.烹饪技巧
	if self.中医等级 < 10 then
		self.中医等级 = 10
	end
	if 玩家数据[id].角色.数据.当前活力 < self.中医等级 / 2 + 10 then
		发送数据(玩家数据[id].连接id, 7, "#y/你没那么多的活力")
		return 0
	else
		玩家数据[id].角色:扣除活力(2, self.中医等级 / 2 + 10)

		self.三药几率 = self.中医等级

		if self.三药几率 <= 10 then
			self.炼药名称 = {
				"包子"
			}
		elseif self.三药几率 <= 20 then
			self.炼药名称 = {
				"包子",
				"包子",
				"烤鸭",
				"佛跳墙"
			}
		elseif self.三药几率 <= 30 then
			self.炼药名称 = {
				"包子",
				"包子",
				"烤鸭",
				"佛跳墙",
				"女儿红",
				"珍露酒",
				"桂花丸"
			}
		elseif self.三药几率 <= 40 then
			self.炼药名称 = {
				"包子",
				"烤鸭",
				"佛跳墙",
				"女儿红",
				"珍露酒",
				"桂花丸",
				"长寿面",
			}

		elseif self.三药几率 <= 50 then
			self.炼药名称 = {
				"包子",
				"烤鸭",
				"佛跳墙",
				"佛跳墙",
				"女儿红",
				"珍露酒",
				"珍露酒",
				"桂花丸",
				"桂花丸",
				"长寿面",
				"长寿面",
				"醉生梦死"
			}
		else
			self.炼药名称 = {
				"烤鸭",
				"烤鸭",
				"佛跳墙",
				"佛跳墙",
				"女儿红",
				"珍露酒",
				"桂花丸",
				"桂花丸",
				"长寿面",
				"长寿面",
				"醉生梦死",
				"醉生梦死",
				"醉生梦死",
				"醉生梦死"
			}
		end

		self.药品名称 = self.炼药名称[取随机数(1, #self.炼药名称)]
		self.可用格子 = 玩家数据[id].角色:取可用道具格子("包裹")

		if self.可用格子 == 0 then
			发送数据(玩家数据[id].连接id, 7, "#y/你身上已经没有多余的道具格子了")

			return 0
		end

		self.可用id = self:取道具编号()
		self.数据[self.可用id] = {
			类型 = "烹饪",
			名称 = self.药品名称,
			品质 = 取随机数(math.floor(self.中医等级 * 0.35), self.中医等级)
		}

		if self.数据[self.可用id].名称 == "包子" then
			self.数据[self.可用id].数量 = 1
		end

		玩家数据[id].角色.数据.道具.包裹[self.可用格子] = self.可用id

		发送数据(玩家数据[id].连接id, 7, "#w/经过一阵忙碌，你烹饪出了#r/" .. self.药品名称)
		发送数据(玩家数据[id].连接id, 3006, "66")
	end
end

function 道具处理类:城市快捷传送(id)
	if 玩家数据[id].战斗 ~= 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/该功能无法在战斗中使用")
	elseif 玩家数据[id].队伍 ~= 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/该功能无法在组队状态下使用")
	elseif 玩家数据[id].角色.数据.等级 >= 60 and 取角色银子(id) < 1000 then
		发送数据(玩家数据[id].连接id, 7, "#y/使用此功能需要消耗1000两银子")
	elseif 玩家数据[id].角色:取任务id(206) ~= 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/游戏大赛过程中不可使用此功能")

		return 0
	else
		self.传送数据 = {
			地图 = 玩家数据[id].角色.数据.传送地图
		}
		self.跳转坐标 = {
			{z = 1001,x = 364,y = 36},
			{z = 1501,x = 65,y = 113},
			{z = 1092,x = 123,y = 57},
			{z = 1070,x = 117,y = 148}
		}
		if self.传送数据.地图 == 1001 then
			self.传送数据.x = 364
			self.传送数据.y = 36
		elseif self.传送数据.地图 == 1501 then
			self.传送数据.x = 65
			self.传送数据.y = 113
		elseif self.传送数据.地图 == 1092 then
			self.传送数据.x = 123
			self.传送数据.y = 57
		elseif self.传送数据.地图 == 1070 then
			self.传送数据.x = 117
			self.传送数据.y = 148
		end

		if 玩家数据[id].角色.数据.门派 ~= "无" then
			self.临时门派 = 玩家数据[id].角色.数据.门派
			self.传送数据 = {
				地图 = 门派传送[self.临时门派].z,
				x = 门派传送[self.临时门派].x,
				y = 门派传送[self.临时门派].y
			}
		end

		地图处理类:指定跳转1(id, self.传送数据.地图, self.传送数据.x, self.传送数据.y)

		if 玩家数据[id].角色.数据.等级 >= 60 then
			玩家数据[id].角色:扣除银子(id, 1000, 5)
		end
	end
end

function 道具处理类:学习强化技能处理(id, 数据)
	self.临时数据 = table.loadstring(数据)

	if self.临时数据 == nil then
		return 0
	end

	self.技能名称 = self.临时数据.名称
	self.技能类型 = "1"
	self.技能类型 = "辅助技能"
	self.消耗方式 = 5
	self.消耗等级 = 玩家数据[id].角色.数据.强化技能[self.技能名称]

	if self.技能类型 ~= "" then
		self.消耗经验 = 技能消耗.经验[self.消耗等级 + 1] * self.消耗方式
		self.消耗金钱 = 技能消耗.金钱[self.消耗等级 + 1] * self.消耗方式

		if 取角色银子(id) < self.消耗金钱 then
			发送数据(玩家数据[id].连接id, 7, "#y/你没那么多的银子")
		elseif 取角色经验(id) < self.消耗经验 then
			发送数据(玩家数据[id].连接id, 7, "#y/你没那么多的经验")
		elseif self.消耗等级 >= 150 then
			发送数据(玩家数据[id].连接id, 7, "#y/该技能等级已达上限")
		else
			玩家数据[id].角色:扣除经验(id, self.消耗经验)
			玩家数据[id].角色:扣除银子(id, self.消耗金钱, 10)

			self.消耗等级 = self.消耗等级 + 1
			玩家数据[id].角色.数据.强化技能[self.技能名称] = self.消耗等级

			玩家数据[id].角色:刷新战斗属性(1)

			self.发送信息 = 玩家数据[id].角色.数据.强化技能
			self.发送信息.银两 = 玩家数据[id].角色.数据.道具.货币.银子
			self.发送信息.经验 = 玩家数据[id].角色.数据.当前经验
			self.发送信息.方式 = 2
			self.发送信息.编号 = self.临时数据.编号

			发送数据(玩家数据[id].连接id, 2019, table.tostring(self.发送信息))
		end
	end
end

function 道具处理类:学习辅助技能处理(id, 数据)
	self.临时数据 = table.loadstring(数据)
	self.技能名称 = self.临时数据.名称
	self.技能类型 = "1"
	self.技能类型 = "辅助技能"
	self.消耗方式 = 2
	self.消耗等级 = 玩家数据[id].角色.数据.辅助技能[self.技能名称]

	if self.技能类型 ~= "" then
		self.消耗经验 = 技能消耗.经验[self.消耗等级 + 1] * self.消耗方式
		self.消耗金钱 = 技能消耗.金钱[self.消耗等级 + 1] * self.消耗方式

		if 取角色银子(id) < self.消耗金钱 then
			发送数据(玩家数据[id].连接id, 7, "#y/你没那么多的银子")
		elseif 取角色经验(id) < self.消耗经验 then
			发送数据(玩家数据[id].连接id, 7, "#y/你没那么多的经验")
		elseif self.消耗等级 >= 150 then
			发送数据(玩家数据[id].连接id, 7, "#y/该技能等级已达上限")
		else
			玩家数据[id].角色:扣除经验(id, self.消耗经验)
			玩家数据[id].角色:扣除银子(id, self.消耗金钱, 10)

			self.消耗等级 = self.消耗等级 + 1
			玩家数据[id].角色.数据.辅助技能[self.技能名称] = self.消耗等级

			玩家数据[id].角色:刷新战斗属性(1)

			self.发送信息 = 玩家数据[id].角色.数据.辅助技能
			self.发送信息.银两 = 玩家数据[id].角色.数据.道具.货币.银子
			self.发送信息.经验 = 玩家数据[id].角色.数据.当前经验
			self.发送信息.方式 = 2
			self.发送信息.编号 = self.临时数据.编号

			发送数据(玩家数据[id].连接id, 2018, table.tostring(self.发送信息))
		end
	end
end

function 道具处理类:学习生活技能处理(id, 数据)
	self.临时数据 = table.loadstring(数据)
	self.技能名称 = self.临时数据.名称
	self.技能类型 = "1"
	self.技能类型 = "生活技能"
	self.消耗方式 = 3
	self.消耗等级 = 玩家数据[id].角色.数据.生活技能[self.技能名称]

	if self.技能类型 ~= "" then
		self.消耗经验 = 技能消耗.经验[self.消耗等级 + 1] * self.消耗方式
		self.消耗金钱 = 技能消耗.金钱[self.消耗等级 + 1] * self.消耗方式

		if 取角色银子(id) < self.消耗金钱 then
			发送数据(玩家数据[id].连接id, 7, "#y/你没那么多的银子")
		elseif 取角色经验(id) < self.消耗经验 then
			发送数据(玩家数据[id].连接id, 7, "#y/你没那么多的经验")
		elseif self.消耗等级 >= 150 then
			发送数据(玩家数据[id].连接id, 7, "#y/该技能等级已达上限")
		else
			玩家数据[id].角色:扣除经验(id, self.消耗经验)
			玩家数据[id].角色:扣除银子(id, self.消耗金钱, 10)

			self.消耗等级 = self.消耗等级 + 1
			玩家数据[id].角色.数据.生活技能[self.技能名称] = self.消耗等级

			玩家数据[id].角色:刷新战斗属性(1)

			self.发送信息 = 玩家数据[id].角色.数据.生活技能
			self.发送信息.银两 = 玩家数据[id].角色.数据.道具.货币.银子
			self.发送信息.经验 = 玩家数据[id].角色.数据.当前经验
			self.发送信息.方式 = 2
			self.发送信息.编号 = self.临时数据.编号

			发送数据(玩家数据[id].连接id, 2022, self.发送信息)
		end
	end
end


function 道具处理类:回天丹处理(id, 数据)
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
	if self.数据[self.临时id1] == nil or self.数据[self.临时id1] == 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/你似乎并没有这样的道具")
		return 0
	end
	任务处理类:添加回天丹(id)
	发送数据(玩家数据[id].连接id, 7, "#y/你使用了回天丹")
	self.数据[self.临时id1] = nil
	玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil
	发送数据(玩家数据[id].连接id, 3006, "66")
end

function 道具处理类:超级回天丹处理(id, 数据)
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
	if self.数据[self.临时id1] == nil or self.数据[self.临时id1] == 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/你似乎并没有这样的道具")

		return 0
	end
	任务处理类:添加回天丹1(id)
	发送数据(玩家数据[id].连接id, 7, "#y/你使用了超级回天丹")
	self.数据[self.临时id1] = nil
	玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil
	发送数据(玩家数据[id].连接id, 3006, "66")
end

function 道具处理类:卸下bb装备处理(id, 格子, 类型)

 local 数据={格子=格子,类型=类型}
 self.临时id1=玩家数据[id].召唤兽.数据[数据.类型].装备[数据.格子]
 if self.数据[self.临时id1]==nil or self.数据[self.临时id1]==0 then

   发送数据(玩家数据[id].连接id,7,"#y/你似乎并没有这样的道具")
   return 0
  end

 self.可用格子=玩家数据[id].角色:取可用道具格子("包裹")
 if self.可用格子==0 then
   发送数据(玩家数据[id].连接id,7,"#y/你已经无法携带更多的道具了")
   return 0

   end

 玩家数据[id].角色.数据.道具["包裹"][self.可用格子]=self.临时id1
 玩家数据[id].召唤兽.数据[数据.类型].装备[数据.格子]=nil
 玩家数据[id].召唤兽:刷新装备属性(数据.类型,id)
 发送数据(玩家数据[id].连接id,2005,玩家数据[id].召唤兽:取头像数据())
 发送数据(玩家数据[id].连接id,2009,玩家数据[id].召唤兽:获取数据())

 发送数据(玩家数据[id].连接id,3006,"66")
end

function 道具处理类:佩戴bb装备处理(id,格子,类型)
  local 数据={格子=格子,类型=类型}
 self.临时id1=玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
 if self.数据[self.临时id1]==nil or self.数据[self.临时id1]==0 then

   发送数据(玩家数据[id].连接id,7,"#y/你似乎并没有这样的道具")
   return 0
  elseif 玩家数据[id].召唤兽.数据.参战==0 then
    发送数据(玩家数据[id].连接id,7,"#y/请先将要佩戴装备的召唤兽设置为参战状态")
   return 0
  elseif  1==2 and 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].等级<self.数据[self.临时id1].等级 then
    发送数据(玩家数据[id].连接id,7,"#y/您的召唤兽当前等级无法佩戴此装备")
   return 0
   end

  --寻找格子  1=项圈 2=铠甲 3=项圈

  if self.数据[self.临时id1].名称=="冰蚕丝圈" then

   self.格子类型=1

  elseif self.数据[self.临时id1].名称=="七星宝甲" then

   self.格子类型=2

  else
    self.格子类型=3

    end
 self.已佩戴id=nil
 if 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].装备[self.格子类型]~=nil then

   self.已佩戴id=玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].装备[self.格子类型]


   end
 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].装备[self.格子类型]=self.临时id1
 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]=self.已佩戴id
 发送数据(玩家数据[id].连接id,7,"#y/召唤兽佩戴装备成功")
 玩家数据[id].召唤兽:刷新装备属性(玩家数据[id].召唤兽.数据.参战,id)
 发送数据(玩家数据[id].连接id,2005,玩家数据[id].召唤兽:取头像数据())
 发送数据(玩家数据[id].连接id,3006,"66")






 end



function 道具处理类:上古锻造图策处理(id,格子,类型)
  local 数据={类型=类型,格子=格子}
 self.临时id1=玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
 if self.数据[self.临时id1]==nil or self.数据[self.临时id1]==0 then
   发送数据(玩家数据[id].连接id,7,"#y/你似乎并没有这样的道具")
   return 0
   end

 --
 self.临时等级=self.数据[self.临时id1].等级

 if 玩家数据[id].角色.数据.当前体力<self.临时等级*4 then

   发送数据(玩家数据[id].连接id,7,"#y/你没那么多的体力")
   return 0

  elseif 银子检查(id,self.临时等级*50000)==false then
    发送数据(玩家数据[id].连接id,7,"#y/你没那么多的银子")
   return 0
 -- elseif 玩家数据[id].角色.数据.生活技能.炼金术<self.临时等级 then
  --  发送数据(玩家数据[id].连接id,7,"#y/你的炼金术等级太低了")
  --  return 0
   end
  玩家数据[id].角色.数据.当前体力=玩家数据[id].角色.数据.当前体力-self.临时等级*4
  玩家数据[id].角色:扣除银子(id,self.临时等级*50000,20)
 self.临时类型=self.数据[self.临时id1].参数
 if self.临时类型=="铠甲" then
   self.临时属性="防御"
   self.临时名称="七星宝甲"
 elseif self.临时类型=="项圈" then
   self.临时属性="速度"
   self.临时名称="冰蚕丝圈"
  elseif self.临时类型=="护腕" then
   self.临时属性="伤害"
   self.临时名称="嵌宝金腕"
   end
 self.附加属性=""
 if 取随机数()<=35 then
   self.附加属性=self.附属性[取随机数(1,#self.附属性)]
   end
 self.数据[self.临时id1]={}
 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]=nil
 self.数据[self.临时id1].等级=self.临时等级
 self.数据[self.临时id1].主属性={名称=self.临时属性,数值=math.floor(取随机数(self.临时等级*0.65,self.临时等级*0.95))}
 self.数据[self.临时id1].类型="bb装备"
 self.数据[self.临时id1].名称=self.临时名称
 if self.附加属性~="" then
   self.数据[self.临时id1].附属性={名称=self.附加属性,数值=math.floor(取随机数(self.临时等级*0.65,self.临时等级*0.95))}
   if self.附加属性=="气血" or self.附加属性=="魔法" then
     self.数据[self.临时id1].附属性.数值=self.数据[self.临时id1].附属性.数值*2
     end
   end
 if 取随机数()<=30 then
   self.数据[self.临时id1].单加={名称=全局变量.基础属性[取随机数(1,#全局变量.基础属性)],数值=math.floor(取随机数(self.临时等级*0.15,self.临时等级*0.35))}
     if 取随机数()<=10 then
     self.数据[self.临时id1].双加={名称=全局变量.基础属性[取随机数(1,#全局变量.基础属性)],数值=math.floor(取随机数(self.临时等级*0.15,self.临时等级*0.35))}
      end
   end
 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]=self.临时id1
 发送数据(玩家数据[id].连接id,7,"你制造出了#r/"..self.临时名称)
 发送数据(玩家数据[id].连接id,3006,"66")
  end


function 道具处理类:元宵处理(id, 数据)
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]

	if self.数据[self.临时id1] == nil or self.数据[self.临时id1] == 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/你似乎并没有这样的道具")

		return 0
	end

	if 玩家数据[id].召唤兽.数据.参战 == 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/请先将要食用元宵的召唤兽设置为参战状态")

		return 0
	end

	self.召唤兽id = 玩家数据[id].召唤兽.数据.参战

	if 玩家数据[id].召唤兽.数据[self.召唤兽id].服用元宵 == nil then
		玩家数据[id].召唤兽.数据[self.召唤兽id].服用元宵 = 0
	end

	self.元宵上限 = 200 + math.floor(玩家数据[id].召唤兽.数据[self.召唤兽id].等级 / 10)

	if self.元宵上限 <= 玩家数据[id].召唤兽.数据[self.召唤兽id].服用元宵 then
		发送数据(玩家数据[id].连接id, 7, "#y/你的这只召唤兽已经无法服用更多的元宵了")

		return 0
	end

	玩家数据[id].召唤兽.数据[self.召唤兽id].服用元宵 = 玩家数据[id].召唤兽.数据[self.召唤兽id].服用元宵 + 1
	self.添加数额 = 0

	if self.数据[self.临时id1].参数 == "攻资" or self.数据[self.临时id1].参数 == "速资" or self.数据[self.临时id1].参数 == "躲资" or self.数据[self.临时id1].参数 == "防资" then
		if 玩家数据[id].召唤兽.数据[self.召唤兽id][self.数据[self.临时id1].参数] < 1300 then
			self.添加数额 = 15
		elseif 玩家数据[id].召唤兽.数据[self.召唤兽id][self.数据[self.临时id1].参数] < 1400 then
			self.添加数额 = 10
		elseif 玩家数据[id].召唤兽.数据[self.召唤兽id][self.数据[self.临时id1].参数] < 1500 then
			self.添加数额 = 5
		else
			self.添加数额 = 3
		end
	elseif self.数据[self.临时id1].参数 == "体资" then
		if 玩家数据[id].召唤兽.数据[self.召唤兽id][self.数据[self.临时id1].参数] < 3000 then
			self.添加数额 = 20
		elseif 玩家数据[id].召唤兽.数据[self.召唤兽id][self.数据[self.临时id1].参数] < 4000 then
			self.添加数额 = 15
		elseif 玩家数据[id].召唤兽.数据[self.召唤兽id][self.数据[self.临时id1].参数] < 5000 then
			self.添加数额 = 10
		else
			self.添加数额 = 5
		end
	elseif self.数据[self.临时id1].参数 == "法资" then
		if 玩家数据[id].召唤兽.数据[self.召唤兽id][self.数据[self.临时id1].参数] < 2000 then
			self.添加数额 = 20
		elseif 玩家数据[id].召唤兽.数据[self.召唤兽id][self.数据[self.临时id1].参数] < 2500 then
			self.添加数额 = 15
		elseif 玩家数据[id].召唤兽.数据[self.召唤兽id][self.数据[self.临时id1].参数] < 2800 then
			self.添加数额 = 10
		else
			self.添加数额 = 5
		end
	end

	玩家数据[id].召唤兽.数据[self.召唤兽id][self.数据[self.临时id1].参数] = 玩家数据[id].召唤兽.数据[self.召唤兽id][self.数据[self.临时id1].参数] + self.添加数额

	玩家数据[id].召唤兽:刷新属性(self.召唤兽id)
	发送数据(玩家数据[id].连接id, 7, "#y/你的这只召唤兽" .. self.数据[self.临时id1].参数 .. "增加了" .. self.添加数额 .. "点")
	发送数据(玩家数据[id].连接id, 7, "#y/你的这只召唤兽已经服用了" .. 玩家数据[id].召唤兽.数据[self.召唤兽id].服用元宵 .. "个元宵")

	self.数据[self.临时id1] = nil
	玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil

	发送数据(玩家数据[id].连接id, 3006, "66")
	发送数据(玩家数据[id].连接id, 3040, "6")
end

function 道具处理类:如意棒处理(id, 数据)
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]

	if self.数据[self.临时id1] == nil or self.数据[self.临时id1] == 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/你似乎并没有这样的道具")

		return 0
	end

	if 玩家数据[id].地图 > 7000 and 玩家数据[id].地图 <= 7020 then
		if 幻域迷宫开关 == false then
			发送数据(玩家数据[id].连接id, 7, "#y/本活动已经结束")

			return 0
		elseif os.time() - self.数据[self.临时id1].起始 >= 3600 then
			发送数据(玩家数据[id].连接id, 7, "#y/该道具已经超过使用期限")

			return 0
		end

		self.可选玩家组 = 地图处理类:取迷宫玩家组()
		self.随机id = self.可选玩家组[取随机数(1, #self.可选玩家组)]

		if 玩家数据[self.随机id].队伍 ~= 0 then
			self.随机id = 玩家数据[self.随机id].队伍
		end

		广播队伍消息(self.随机id, 7, "#r/" .. 玩家数据[id].角色.数据.名称 .. "#y/对你们使用了如意棒")
		广播队伍消息(self.随机id, 7, "#y/你获得了1点积分")

		幻域迷宫数据[玩家数据[id].数字id].积分 = 幻域迷宫数据[玩家数据[id].数字id].积分 + 3

		if 玩家数据[self.随机id].队伍 == 0 then
			幻域迷宫数据[玩家数据[self.随机id].数字id].积分 = 幻域迷宫数据[玩家数据[self.随机id].数字id].积分 + 1
		else
			for n = 1, #队伍数据[玩家数据[self.随机id].队伍].队员数据 do
				幻域迷宫数据[玩家数据[队伍数据[玩家数据[self.随机id].队伍].队员数据[n]].数字id].积分 = 幻域迷宫数据[玩家数据[队伍数据[玩家数据[self.随机id].队伍].队员数据[n]].数字id].积分 + 1
			end
		end

		战斗准备类:进入处理(self.随机id, 100025, "66", 0)

		self.数据[self.临时id1] = nil
		玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil

		发送数据(玩家数据[id].连接id, 3006, "66")
	else
		发送数据(玩家数据[id].连接id, 7, "#y/该道具只能在迷宫地图中使用")

		return 0
	end
end

function 道具处理类:飞行卷处理(id, 数据)
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]

	if self.数据[self.临时id1] == nil or self.数据[self.临时id1] == 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/你似乎并没有这样的道具")

		return 0
	end

	if 玩家数据[id].地图 > 7000 and 玩家数据[id].地图 <= 7020 then
		if 幻域迷宫开关 == false then
			发送数据(玩家数据[id].连接id, 7, "#y/本活动已经结束")

			return 0
		elseif os.time() - self.数据[self.临时id1].起始 >= 3600 then
			发送数据(玩家数据[id].连接id, 7, "#y/该道具已经超过使用期限")

			return 0
		end

		self.随机正负 = 取随机数()
		self.随机层数 = 取随机数(1, 3)

		if self.随机正负 <= 30 then
			self.随机层数 = 玩家数据[id].地图
			self.随机层数 = self.随机层数 - 1

			if self.随机层数 <= 7001 then
				self.随机层数 = 7001
			end

			广播队伍消息(id, 7, "#y/" .. 玩家数据[id].角色.数据.名称 .. "使用了飞行卷")
			广播队伍消息(id, 7, "#y/在飞行卷的作用下，你们来到了幻域迷宫" .. self.随机层数 - 7000 .. "层")
		else
			self.随机层数 = 玩家数据[id].地图 + self.随机层数

			if self.随机层数 >= 7020 then
				self.随机层数 = 7020
			end

			广播队伍消息(id, 7, "#y/" .. 玩家数据[id].角色.数据.名称 .. "使用了飞行卷")
			广播队伍消息(id, 7, "#y/在飞行卷的作用下，你们来到了幻域迷宫" .. self.随机层数 - 7000 .. "层")
		end

		self.临时坐标 = 地图处理类.地图数据[1193].坐标:取随机格子()

		地图处理类:指定跳转1(id, self.随机层数, math.floor(self.临时坐标.x / 20), math.floor(self.临时坐标.y / 20))

		幻域迷宫数据[玩家数据[id].数字id].积分 = 幻域迷宫数据[玩家数据[id].数字id].积分 + 1
		self.数据[self.临时id1] = nil
		玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil

		发送数据(玩家数据[id].连接id, 3006, "66")
	else
		发送数据(玩家数据[id].连接id, 7, "#y/该道具只能在迷宫地图中使用")

		return 0
	end
end

function 道具处理类:摇钱树处理(id, 数据)
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]

	if self.数据[self.临时id1] == nil or self.数据[self.临时id1] == 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/你似乎并没有这样的道具")

		return 0
	end

	if 玩家数据[id].地图 == 1001 or 玩家数据[id].地图 == 1501 or 玩家数据[id].地图 == 1070 or 玩家数据[id].地图 == 1092 then
		发送数据(玩家数据[id].连接id, 7, "#y/你无法在这样的场景种植摇钱树")

		return 0
	elseif 玩家数据[id].角色.数据.当前体力 < 100 then
		发送数据(玩家数据[id].连接id, 7, "#y/种植摇钱树需要扣除100点体力")

		return 0
	end

	玩家数据[id].角色.数据.当前体力 = 玩家数据[id].角色.数据.当前体力 - 100

	发送数据(玩家数据[id].连接id, 7, "#y/你消耗了100点体力")

	玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil
	self.数据[self.临时id1] = nil

	任务处理类:添加摇钱树(id)
	发送数据(玩家数据[id].连接id, 3006, "66")
end

function 道具处理类:合成旗传送处理(id, 参数)
	self.临时id1 = 玩家数据[id].角色.数据.道具[玩家数据[id].包裹类型][玩家数据[id].道具id]

	if self.数据[self.临时id1] == nil or self.数据[self.临时id1] == 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/你似乎并没有这样的道具")

		return 0
	end

	if 玩家数据[id].队伍 ~= 0 and 玩家数据[id].队长 == false then
		发送数据(玩家数据[id].连接id, 7, "#y/只有队长才可使用此道具")
	elseif 玩家数据[id].角色:取任务id(206) ~= 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/游泳大赛过程中不可使用此功能")

		return 0
	else
		self.允许使用 = true

		if 玩家数据[id].角色:取飞行限制() == false then
			self.允许使用 = false
			self.提示信息 = "#y/您当前无法使用飞行道具"
		end

		if 玩家数据[id].队伍 ~= 0 then
			for n = 1, #队伍数据[玩家数据[id].队伍].队员数据 do
				self.临时id = 队伍数据[玩家数据[id].队伍].队员数据[n]

				if 玩家数据[self.临时id].角色:取飞行限制() == false then
					self.允许使用 = false
					self.提示信息 = "#y/" .. 玩家数据[self.临时id].角色.数据.名称 .. "无法使用飞行道具"
				end
			end
		end

		if self.允许使用 == false then
			发送数据(玩家数据[id].连接id, 7, self.提示信息)
		else
			地图处理类:指定跳转1(id, 玩家数据[id].道具.数据[self.临时id1].地图编号, 玩家数据[id].道具.数据[self.临时id1].坐标[参数].x, 玩家数据[id].道具.数据[self.临时id1].坐标[参数].y)

			玩家数据[id].道具.数据[self.临时id1].次数 = 玩家数据[id].道具.数据[self.临时id1].次数 - 1

			if 玩家数据[id].道具.数据[self.临时id1].次数 <= 0 then
				玩家数据[id].道具.数据[self.临时id1] = nil
				玩家数据[id].角色.数据.道具[玩家数据[id].包裹类型][玩家数据[id].道具id] = nil

				发送数据(玩家数据[id].连接id, 7, "#y/您的合成旗次数已经用尽")
			end

			发送数据(玩家数据[id].连接id, 3006, "#y/您的导标旗次数已经用尽")
		end
	end
end

function 道具处理类:合成旗处理(id, 数据)
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]

	if self.数据[self.临时id1] == nil or self.数据[self.临时id1] == 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/你似乎并没有这样的道具")

		return 0
	end

	if #self.数据[self.临时id1].坐标 < 7 then
		if self.数据[self.临时id1].地图编号 == 0 then
			if 玩家数据[id].地图 == 1001 or 玩家数据[id].地图 == 1070 or 玩家数据[id].地图 == 1501 or 玩家数据[id].地图 == 1092  or 玩家数据[id].地图 == 1208 or 玩家数据[id].地图 == 1040 then
				self.数据[self.临时id1].地图编号 = 玩家数据[id].地图
				self.数据[self.临时id1].地图 = 地图处理类.地图数据[玩家数据[id].地图].名称
				self.数据[self.临时id1].坐标[#self.数据[self.临时id1].坐标 + 1] = {
					x = math.floor(玩家数据[id].角色.数据.地图数据.x / 20),
					y = math.floor(玩家数据[id].角色.数据.地图数据.y / 20)
				}

				发送数据(玩家数据[id].连接id, 7, "#y/定标成功，当前已定标#r/" .. #self.数据[self.临时id1].坐标 .. "#y/个点")
				发送数据(玩家数据[id].连接id, 3006, "#y/您的导标旗次数已经用尽")
			else
				发送数据(玩家数据[id].连接id, 7, "#y/只可以在长安城、建邺城、傲来国、长寿村、朱紫国、西凉女国处定标")

				return 0
			end
		elseif self.数据[self.临时id1].地图编号 == 玩家数据[id].地图 then
			self.数据[self.临时id1].坐标[#self.数据[self.临时id1].坐标 + 1] = {
				x = math.floor(玩家数据[id].角色.数据.地图数据.x / 20),
				y = math.floor(玩家数据[id].角色.数据.地图数据.y / 20)
			}

			发送数据(玩家数据[id].连接id, 7, "#y/定标成功，当前已定标#r/" .. #self.数据[self.临时id1].坐标 .. "#y/个点")
		else
			发送数据(玩家数据[id].连接id, 7, "#y/您只能在#g/" .. 地图处理类.地图数据[self.数据[self.临时id1].编号].名称 .. "#y/处定标")
		end
	else
		玩家数据[id].道具id = 数据.格子
		玩家数据[id].包裹类型 = 数据.类型
		self.数据[self.临时id1].编号 = self.数据[self.临时id1].地图编号

		发送数据(玩家数据[id].连接id, 20018, self.数据[self.临时id1])
	end
end

function 道具处理类:法宝传送处理(id, 数据)
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]

	if self.数据[self.临时id1] == nil or self.数据[self.临时id1] == 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/你似乎并没有这样的道具")
		return 0
	end
	if 玩家数据[id].队伍~=0 and 玩家数据[id].队长==false then
	   发送数据(玩家数据[id].连接id,7,"#y/只有队长才可使用此道具")
	elseif 玩家数据[id].角色:取任务id(206)~=0 then
	   发送数据(玩家数据[id].连接id,7,"#y/游泳大赛过程中不可使用此功能")
	   return 0
	else
	    self.允许使用=true
	    if 玩家数据[id].角色:取飞行限制()==false then
		    self.允许使用=false
		    self.提示信息="#y/您当前无法使用飞行道具"
	    end
	    if 玩家数据[id].队伍~=0 then
	      	for n=1,#队伍数据[玩家数据[id].队伍].队员数据 do
		        self.临时id=队伍数据[玩家数据[id].队伍].队员数据[n]
		        if 玩家数据[self.临时id].角色:取飞行限制()==false then
		          self.允许使用=false
		          self.提示信息="#y/"..玩家数据[self.临时id].角色.数据.名称.."无法使用飞行道具"
		        end
	        end
	    end
	    if self.允许使用==false then
	     	发送数据(玩家数据[id].连接id,7,self.提示信息)
	    else
	    	地图处理类:指定跳转1(id,self.数据[self.临时id1].地图数据.编号,self.数据[self.临时id1].地图数据.坐标x/20,self.数据[self.临时id1].地图数据.坐标y/20)
	    	self.数据[self.临时id1].地图数据 = nil
	    	发送数据(玩家数据[id].连接id, 3006, "66")
	    end
	end
end

function 道具处理类:变身卡处理(id, 数据)
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]

	if self.数据[self.临时id1] == nil or self.数据[self.临时id1] == 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/你似乎并没有这样的道具")

		return 0
	end

	玩家数据[id].角色.数据.变身.造型 = self.数据[self.临时id1].类型

	任务处理类:添加变身卡(id)
	发送数据(玩家数据[id].连接id, 2010, 玩家数据[id].角色:获取角色数据())
	地图处理类:发送数据(id, 1016, 玩家数据[id].角色:获取角色数据(), 玩家数据[id].地图)

	self.数据[self.临时id1].次数 = self.数据[self.临时id1].次数 - 1

	if self.数据[self.临时id1].次数 <= 0 then
		玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil
		self.数据[self.临时id1] = nil
	end

	玩家数据[id].角色:刷新战斗属性()
	发送数据(玩家数据[id].连接id, 3006, "66")
	发送数据(玩家数据[id].连接id, 7, "#y/你使用了变身卡")
end
function 道具处理类:双倍经验丹处理(id, 数据)

	if 玩家数据[id].角色:取任务id(3819) ~= 0 then
		任务数据[玩家数据[id].角色:取任务id(3819)].起始 = os.time()
		发送数据(玩家数据[id].连接id, 7, "#y/你使用了双倍经验丹")
		任务处理类:刷新追踪任务信息(id)
	else
		任务处理类:添加双倍(id)
		发送数据(玩家数据[id].连接id, 7, "#y/你使用了双倍经验丹")
	end

	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]

	if self.数据[self.临时id1].数量 == nil then
		self.数据[self.临时id1].数量 = 1
	end

	self.数据[self.临时id1].数量 = self.数据[self.临时id1].数量 - 1

	if self.数据[self.临时id1].数量 <= 0 then
		self.数据[self.临时id1] = nil
		玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil
	end

	发送数据(玩家数据[id].连接id, 3006, "66")
end
function 道具处理类:神兜兜处理(id, 数据)
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
	if self.数据[self.临时id1] == nil or self.数据[self.临时id1] == 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/你似乎并没有这样的道具")

		return 0
	end
	if self.数据[self.临时id1].数量 ~= nil and self.数据[self.临时id1].数量 >= 99 then
		if #玩家数据[id].召唤兽.数据 >= 5 then
			发送数据(玩家数据[id].连接id, 7, "#y/你当前可携带的召唤兽已达上限")

			return 0
		end
		self.随机神兽 = {
			"超级金猴",
			"超级灵鹿",
			"超级白泽",
		}

		玩家数据[id].角色:购买神兽1(id, self.随机神兽[取随机数(1, #self.随机神兽)], 0)

		self.数据[self.临时id1] = nil
		玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil

		发送数据(玩家数据[id].连接id, 3006, "66")
	else
		发送数据(玩家数据[id].连接id, 7, "#y/只有集齐99个神兜兜才可兑换神兽")
	end
end
function 道具处理类:孤儿名册处理(id, 数据)
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
	if self.数据[self.临时id1] == nil or self.数据[self.临时id1] == 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/你似乎并没有这样的道具")

		return 0
	end
	if self.数据[self.临时id1].数量 ~= nil and self.数据[self.临时id1].数量 >= 1 then
		if #玩家数据[id].召唤兽.数据 >= 5 then
			发送数据(玩家数据[id].连接id, 7, "#y/你当前可携带的召唤兽已达上限")

			return 0
		end
		self.随机神兽 = {
			"进阶小丫丫",
			"进阶小仙灵",
			"进阶小仙女",
			"进阶小精灵",
			"进阶小魔头",
		}

		玩家数据[id].角色:购买神兽1(id, self.随机神兽[取随机数(1, #self.随机神兽)], 0)

		self.数据[self.临时id1] = nil
		玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil

		发送数据(玩家数据[id].连接id, 3006, "66")
	else
		发送数据(玩家数据[id].连接id, 7, "#y/只有集齐99个神兜兜才可兑换神兽")
	end
end

function 道具处理类:海马处理(id, 格子, 类型)
	local 数据 = {
		格子 = 格子,
		类型 = 类型
	}
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]

	if self.数据[self.临时id1] == nil or self.数据[self.临时id1] == 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/你似乎并没有这样的道具")

		return 0
	end

	玩家数据[id].角色.数据.当前体力 = 玩家数据[id].角色.数据.当前体力 + 100

	if 玩家数据[id].角色.数据.当前体力 > 玩家数据[id].角色.数据.体力上限 then
		玩家数据[id].角色.数据.当前体力 =玩家数据[id].角色.数据.体力上限
	end
	玩家数据[id].角色.数据.当前活力 = 玩家数据[id].角色.数据.当前活力 + 100

	if 玩家数据[id].角色.数据.当前活力 > 玩家数据[id].角色.数据.活力上限 then
		玩家数据[id].角色.数据.当前活力 =玩家数据[id].角色.数据.活力上限
	end


	发送数据(玩家数据[id].连接id, 7, "#y/你的体力活力增加了100点")

	self.数据[self.临时id1] = nil
	玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil

	发送数据(玩家数据[id].连接id, 3006, "66")
end


function 道具处理类:蟠桃处理(id, 数据)
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]

	if self.数据[self.临时id1] == nil or self.数据[self.临时id1] == 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/你似乎并没有这样的道具")

		return 0
	end

	if 玩家数据[id].召唤兽.数据.参战 == 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/请先将要食用蟠桃的召唤兽设置为参战状态")

		return 0
	end

	self.召唤兽id = 玩家数据[id].召唤兽.数据.参战

	if 玩家数据[id].召唤兽.数据[self.召唤兽id].服用蟠桃 == nil then
		玩家数据[id].召唤兽.数据[self.召唤兽id].服用蟠桃 = 0
	end

	self.蟠桃上限 = 200

	if self.蟠桃上限 <= 玩家数据[id].召唤兽.数据[self.召唤兽id].服用蟠桃 then
		发送数据(玩家数据[id].连接id, 7, "#y/你的这只召唤兽已经无法服用更多的蟠桃了")

		return 0
	end

	玩家数据[id].召唤兽.数据[self.召唤兽id].服用蟠桃 = 玩家数据[id].召唤兽.数据[self.召唤兽id].服用蟠桃 + 1
	玩家数据[id].召唤兽.数据[self.召唤兽id].成长 = 玩家数据[id].召唤兽.数据[self.召唤兽id].成长 + 0.001

	玩家数据[id].召唤兽:刷新属性(self.召唤兽id)
	发送数据(玩家数据[id].连接id, 7, "#y/你的这只召唤兽成长提升了0.001点")
	发送数据(玩家数据[id].连接id, 7, "#y/你的这只召唤兽已经服用了" .. 玩家数据[id].召唤兽.数据[self.召唤兽id].服用蟠桃 .. "个蟠桃")

	self.数据[self.临时id1] = nil
	玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil

	发送数据(玩家数据[id].连接id, 3006, "66")
	发送数据(玩家数据[id].连接id, 3040, "66")
end

function 道具处理类:鬼谷子处理(id, 格子, 类型)
	local 数据 = {
		格子 = 格子,
		类型 = 类型
	}
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]

	if self.数据[self.临时id1] == nil or self.数据[self.临时id1] == 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/你似乎并没有这样的道具")

		return 0
	end

	self.临时阵法 = self.数据[self.临时id1].参数

	if 玩家数据[id].角色.数据.阵法[self.临时阵法] then
		发送数据(玩家数据[id].连接id, 7, "#y/你已经学习过此阵型，请勿重复学习")

		return 0
	else
		玩家数据[id].角色.数据.阵法[self.临时阵法] = true

		发送数据(玩家数据[id].连接id, 7, "#y/你学会了如何使用" .. self.临时阵法)
	end

	self.数据[self.临时id1] = nil
	玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil

	发送数据(玩家数据[id].连接id, 3006, "66")
end


function 道具处理类:玄天残卷处理(id,数据)

	local xtcj = {"玄天残卷·上卷","玄天残卷·中卷","玄天残卷·下卷"}
	local gz = {}
	for i = 1, #xtcj do
		local lskg = false
		for n = 1, 20 do
			if 玩家数据[id].角色.数据.道具[数据.类型][n] ~= nil and lskg == false and self.数据[玩家数据[id].角色.数据.道具[数据.类型][n]].名称 == xtcj[i] then
				lskg = true
				gz[#gz + 1] = n
			end
		end
	end
	if #gz ~= 3 then
		发送数据(玩家数据[id].连接id, 7, "#y/只有集齐玄天残卷·上卷、玄天残卷·中卷、玄天残卷·下卷才可合成")
	else
		for n = 1, 3 do
			self.数据[玩家数据[id].角色.数据.道具[数据.类型][gz[n]]] = 0
			玩家数据[id].角色.数据.道具[数据.类型][gz[n]] = nil
		end
		self:新加给予道具(id, "圣兽丹")
		self:索要道具(id, 数据.类型)
	end
end
function 道具处理类:五宝处理(id, 格子, 类型)
	local 数据 = {
		格子 = 格子,
		类型 = 类型
	}
	self.五宝名称 = {
		"夜光珠",
		"龙鳞",
		"定魂珠",
		"避水珠",
		"金刚石"
	}
	self.五宝格子 = {}

	for i = 1, #self.五宝名称 do
		self.五宝找到 = false

		for n = 1, 20 do
			if 玩家数据[id].角色.数据.道具[数据.类型][n] ~= nil and self.五宝找到 == false and self.数据[玩家数据[id].角色.数据.道具[数据.类型][n]].名称 == self.五宝名称[i] then
				self.五宝找到 = true
				self.五宝格子[#self.五宝格子 + 1] = n
			end
		end
	end

	if #self.五宝格子 ~= 5 then
		发送数据(玩家数据[id].连接id, 7, "#y/只有集齐龙鳞、夜光珠、避水珠、定魂珠、金刚石才可合成")
	else
		for n = 1, 5 do
			self.数据[玩家数据[id].角色.数据.道具[数据.类型][self.五宝格子[n]]] = 0
			玩家数据[id].角色.数据.道具[数据.类型][self.五宝格子[n]] = nil
		end

		发送数据(玩家数据[id].连接id, 7, "#y/你获得了一张高级藏宝图")
		self:给予道具(id, "高级藏宝图", "功能", 0, 1)
		self:索要道具(id, 数据.类型)
	end
end

function 道具处理类:索要玩家道具(id, 内容)
	self.发送信息 = {}

	for n = 1, 20 do
		if 玩家数据[id].角色.数据.道具[内容][n] ~= nil and self.数据[玩家数据[id].角色.数据.道具[内容][n]] ~= nil then
			self.发送信息[n] = self.数据[玩家数据[id].角色.数据.道具[内容][n]]
		end
	end

	for n = 21, 26 do
		if 玩家数据[id].角色.数据.装备数据[n] ~= nil and self.数据[玩家数据[id].角色.数据.装备数据[n]] ~= nil then
			self.发送信息[n] = self.数据[玩家数据[id].角色.数据.装备数据[n]]
		end
	end

	self.发送信息.类型 = 内容
	self.发送信息.银两 = 玩家数据[id].角色.数据.道具.货币.银子
	self.发送信息.存银 = 玩家数据[id].角色.数据.道具.货币.存银

	return self.发送信息
end

function 道具处理类:给予玩家物品处理(id, 参数, 序号, 文本)
	self.临时文本 = 分割文本(文本, "*-*")
	local 内容 = {
		id = 参数,
		银子 = 序号,
		格子 = {
			0,
			0,
			0
		}
	}

	for n = 1, #self.临时文本 do
		if self.临时文本[n] ~= "" then
			内容.格子[n] = self.临时文本[n] + 0
		end
	end

	if 玩家数据[内容.id] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/这个玩家并不在线")
	elseif 玩家数据[id].地图 ~= 玩家数据[内容.id].地图 then
		发送数据(玩家数据[id].连接id, 7, "#y/你离这个玩家太远了")
	elseif 地图处理类:取距离范围(id, 内容.id, 400) == false then
		发送数据(玩家数据[id].连接id, 7, "#y/你离这个玩家太远了")
	else
		for n = 1, 3 do
			if 内容.格子[n] ~= 0 then
				self:给予玩家物品处理1(id, 内容.id, 内容.格子[n])
			end
		end

		内容.银子 = 内容.银子 + 0
		内容.银子 = 0

		if 内容.银子 > 0 then
			if 玩家数据[id].角色.数据.道具.货币.银子 < 内容.银子 then
				发送数据(玩家数据[id].连接id, 7, "#y/你没有那么多的银子")
			else
				玩家数据[id].角色.数据.道具.货币.银子 = 玩家数据[id].角色.数据.道具.货币.银子 - 内容.银子
				玩家数据[内容.id].角色.数据.道具.货币.银子 = 玩家数据[内容.id].角色.数据.道具.货币.银子 + 内容.银子

				发送数据(玩家数据[id].连接id, 7, "#y/你给了" .. 玩家数据[内容.id].角色.数据.名称 .. 内容.银子 .. "两银子")
				发送数据(玩家数据[内容.id].连接id, 7, "#y/" .. 玩家数据[id].角色.数据.名称 .. "给了你" .. 内容.银子 .. "两银子")
				玩家数据[id].角色:添加消费日志("[给予]给予(" .. 玩家数据[内容.id].账号 .. ")银子：" .. 内容.银子)
				玩家数据[内容.id].角色:添加消费日志("[给予]获得(" .. 玩家数据[id].账号 .. ")银子：" .. 内容.银子)
				发送数据(玩家数据[id].连接id, 3006, "子")
			end
		end

		发送数据(id, 19005, "66")
		发送数据(内容.id, 19005, "66")
	end
end

function 道具处理类:交易条件判断(id, 内容)
	self.找到id = 内容 + 0

	if 玩家数据[self.找到id] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/这个玩家并不存在")

		return 0
	elseif 玩家数据[self.找到id].交易 ~= 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/对方正在与其他人交易中……")

		return 0
	elseif 玩家数据[id].交易 ~= 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/你的上一次交易还没有完成")

		return 0
	elseif 玩家数据[id].角色.数据.等级 < 50 then
		发送数据(玩家数据[id].连接id, 7, "#y/等级达到50级后才可使用交易功能")

		return 0
	elseif 玩家数据[self.找到id].角色.数据.等级 < 50 then
		发送数据(玩家数据[id].连接id, 7, "#y/对方等级未达到50级无法进行交易")

		return 0
	elseif 玩家数据[id].认证 == "0" or 玩家数据[self.找到id].认证 == "0" then
		发送数据(玩家数据[id].连接id, 7, "#y/认证模式下无法使用交易功能")

		return 0
	else
		if 玩家数据[id].角色.数据.道具.货币.银子 < 0 or 玩家数据[self.找到id].角色.数据.道具.货币.银子 < 0 then
			发送数据(玩家数据[id].连接id, 7, "#y/错误的交易数据")

			return 0
		end

		交易数据[玩家数据[id].数字id] = {
			发起方 = {
				银两 = 0,
				id = id,
				道具 = {},
				召唤兽 = {}
			},
			接受方 = {
				银两 = 0,
				id = self.找到id,
				道具 = {},
				召唤兽 = {}
			},
			确认 = {
				false,
				false
			},
			确定 = {
				false,
				false
			}
		}
		玩家数据[self.找到id].交易 = 玩家数据[id].数字id
		玩家数据[id].交易 = 玩家数据[id].数字id
		self.发送信息 = {
			id = 玩家数据[id].数字id,
			道具 = self:索要玩家道具(id, "包裹"),
			名称 = 玩家数据[self.找到id].角色.数据.名称,
			等级 = 玩家数据[self.找到id].角色.数据.等级
		}

		发送数据(玩家数据[id].连接id, 3013, self.发送信息)

		self.发送信息 = {
			id = 玩家数据[id].数字id,
			道具 = 玩家数据[self.找到id].道具:索要玩家道具(self.找到id, "包裹"),
			名称 = 玩家数据[id].角色.数据.名称,
			等级 = 玩家数据[id].角色.数据.等级
		}

		发送数据(玩家数据[self.找到id].连接id, 3013, self.发送信息)
		发送数据(玩家数据[id].连接id, 7, "#w/你正在与#y/[" .. 玩家数据[self.找到id].角色.数据.名称 .. "]#w/交易")
		发送数据(玩家数据[self.找到id].连接id, 7, "#w/你正在与#y/[" .. 玩家数据[id].角色.数据.名称 .. "]#w/交易")
	end
end

function 道具处理类:给予玩家物品处理1(id, 给予id, 格子)
	self.道具id = 玩家数据[id].角色.数据.道具.包裹[格子]

	if 玩家数据[id].角色.数据.道具.包裹[格子] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/你没有这样的道具")

		return 0
	elseif 玩家数据[id].道具.数据[self.道具id] == nil or 玩家数据[id].道具.数据[self.道具id] == 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/这个道具不存在")

		return 0
	else
		self.玩家格子 = 玩家数据[给予id].角色:取可用道具格子("包裹")

		if self.玩家格子 == 0 then
			发送数据(玩家数据[id].连接id, 7, "#y/对方身上没有多余的道具空间")

			return 0
		else
			self.临时道具id = 玩家数据[给予id].道具:取道具编号()
			玩家数据[给予id].道具.数据[self.临时道具id] = table.copy(玩家数据[id].道具.数据[self.道具id])
			玩家数据[给予id].角色.数据.道具.包裹[self.玩家格子] = self.临时道具id
			玩家数据[id].道具.数据[self.道具id] = nil
			玩家数据[id].角色.数据.道具.包裹[格子] = nil

			发送数据(玩家数据[id].连接id, 7, "#y/你给了" .. 玩家数据[给予id].角色.数据.名称 .. 玩家数据[给予id].道具.数据[self.临时道具id].名称)
			玩家数据[给予id].角色:添加消费日志("[给予]获得(" .. 玩家数据[id].账号 .. ")道具：" .. 玩家数据[给予id].道具.数据[self.临时道具id].名称)
			玩家数据[id].角色:添加消费日志("[给予]给予(" .. 玩家数据[给予id].账号 .. ")道具：" .. 玩家数据[给予id].道具.数据[self.临时道具id].名称)
			发送数据(玩家数据[给予id].连接id, 7, "#y/" .. 玩家数据[id].角色.数据.名称 .. "给了你" .. 玩家数据[给予id].道具.数据[self.临时道具id].名称)
		end
	end
end

function 道具处理类:使用烹饪处理(id, 格子, 类型)
	local 数据 = {
		格子 = 格子,
		类型 = 类型
	}
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]

	if 玩家数据[id].道具.数据[self.临时id1] == nil or 玩家数据[id].道具.数据[self.临时id1] == nil then
		return 0
	end

	if 玩家数据[id].道具.数据[self.临时id1].名称 == "包子" then
		玩家数据[id].角色:恢复血魔(id, 2, 100)

		玩家数据[id].道具.数据[self.临时id1].数量 = 玩家数据[id].道具.数据[self.临时id1].数量 - 1

		if 玩家数据[id].道具.数据[self.临时id1].数量 <= 0 then
			玩家数据[id].道具.数据[self.临时id1] = nil
			玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil
		end

		self:索要道具(id, 数据.类型)

		return 0
	elseif 玩家数据[id].道具.数据[self.临时id1].名称 == "烤鸭" then
		玩家数据[id].角色:恢复血魔(id, 2, 玩家数据[id].道具.数据[self.临时id1].品质 * 10 + 100)
	elseif 玩家数据[id].道具.数据[self.临时id1].名称 == "佛跳墙" then
		玩家数据[id].角色:恢复血魔(id, 3, 玩家数据[id].道具.数据[self.临时id1].品质 * 10 + 100)
	elseif 玩家数据[id].道具.数据[self.临时id1].名称 == "珍露酒" then
		玩家数据[id].角色.数据.愤怒 = math.floor(玩家数据[id].角色.数据.愤怒 + 玩家数据[id].道具.数据[self.临时id1].品质 * 0.4 + 10)

		if 玩家数据[id].角色.数据.愤怒 > 150 then
			玩家数据[id].角色.数据.愤怒 = 150
		end

		发送数据(玩家数据[id].连接id, 1003, 玩家数据[id].角色:获取角色气血数据())
	elseif 玩家数据[id].道具.数据[self.临时id1].名称 == "女儿红" then
		玩家数据[id].角色.数据.愤怒 = math.floor(玩家数据[id].角色.数据.愤怒 + 20)

		if 玩家数据[id].角色.数据.愤怒 > 150 then
			玩家数据[id].角色.数据.愤怒 = 150
		end

		发送数据(玩家数据[id].连接id, 1003, 玩家数据[id].角色:获取角色气血数据())
	elseif 玩家数据[id].道具.数据[self.临时id1].名称 == "醉生梦死" then
		玩家数据[id].角色.数据.愤怒 = math.floor(玩家数据[id].角色.数据.愤怒 + 玩家数据[id].道具.数据[self.临时id1].品质 * 1)

		if 玩家数据[id].角色.数据.愤怒 > 150 then
			玩家数据[id].角色.数据.愤怒 = 150
		end

		发送数据(玩家数据[id].连接id, 1003, 玩家数据[id].角色:获取角色气血数据())
	elseif 玩家数据[id].道具.数据[self.临时id1].名称 == "桂花丸" then
		if 玩家数据[id].召唤兽.数据.参战 == 0 then
			发送数据(玩家数据[id].连接id, 7, "#y/请将要添加寿命的召唤兽设置为参战状态")

			return 0
		elseif 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].神兽 then
			发送数据(玩家数据[id].连接id, 7, "#y/你的这只召唤兽无法增加寿命")

			return 0
		end

		玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].寿命 = 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].寿命 + math.floor(玩家数据[id].道具.数据[self.临时id1].品质 * 0.5)

		发送数据(玩家数据[id].连接id, 7, "#y/你的这只召唤兽寿命增加了" .. math.floor(玩家数据[id].道具.数据[self.临时id1].品质 * 0.5) .. "点")
	elseif 玩家数据[id].道具.数据[self.临时id1].名称 == "长寿面" then
		if 玩家数据[id].召唤兽.数据.参战 == 0 then
			发送数据(玩家数据[id].连接id, 7, "#y/请将要添加寿命的召唤兽设置为参战状态")

			return 0
		elseif 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].神兽 then
			发送数据(玩家数据[id].连接id, 7, "#y/你的这只召唤兽无法增加寿命")

			return 0
		end

		玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].寿命 = 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].寿命 + math.floor(玩家数据[id].道具.数据[self.临时id1].品质 * 2 + 50)

		发送数据(玩家数据[id].连接id, 7, "#y/你的这只召唤兽寿命增加了" .. math.floor(玩家数据[id].道具.数据[self.临时id1].品质 * 2 + 50) .. "点")
	end

	玩家数据[id].道具.数据[self.临时id1] = 0
	玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil

	self:索要道具(id, 数据.类型)
end

function 道具处理类:使用药品处理(id, 格子, 类型)
	local 数据 = {
		格子 = 格子,
		类型 = 类型
	}
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]

	if 玩家数据[id].道具.数据[self.临时id1] == nil or 玩家数据[id].道具.数据[self.临时id1] == nil then
		return 0
	end

	if 游戏装备.药品[玩家数据[id].道具.数据[self.临时id1].名称].等级 ~= 3 then
		if 游戏装备.药品[玩家数据[id].道具.数据[self.临时id1].名称].气血 ~= 0 then
			玩家数据[id].角色:恢复血魔(id, 2, 游戏装备.药品[玩家数据[id].道具.数据[self.临时id1].名称].气血)
		elseif 游戏装备.药品[玩家数据[id].道具.数据[self.临时id1].名称].魔法 ~= 0 then
			玩家数据[id].角色:恢复血魔(id, 3, 游戏装备.药品[玩家数据[id].道具.数据[self.临时id1].名称].魔法)
		end

		if 玩家数据[id].道具.数据[self.临时id1].数量 ~= nil then
			玩家数据[id].道具.数据[self.临时id1].数量 = 玩家数据[id].道具.数据[self.临时id1].数量 - 1

			if 玩家数据[id].道具.数据[self.临时id1].数量 <= 0 then
				玩家数据[id].道具.数据[self.临时id1] = nil
				玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil
			end
		else
			玩家数据[id].道具.数据[self.临时id1] = nil
			玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil
		end
	elseif 游戏装备.药品[玩家数据[id].道具.数据[self.临时id1].名称].等级 == 3 then
		self.名称 = 玩家数据[id].道具.数据[self.临时id1].名称
		self.品质 = 玩家数据[id].道具.数据[self.临时id1].品质

		if self.名称 == "金创药" then
			玩家数据[id].角色:恢复血魔(id, 2, 400)
			发送数据(玩家数据[id].连接id, 7, "#y/您使用了" .. self.名称)

			玩家数据[id].道具.数据[self.临时id1] = 0
			玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil
		elseif self.名称 == "千年保心丹" then
			玩家数据[id].角色:恢复血魔(id, 2, self.品质 * 4 + 200)
			发送数据(玩家数据[id].连接id, 7, "#y/您使用了" .. self.名称)

			玩家数据[id].道具.数据[self.临时id1] = 0
			玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil
		elseif self.名称 == "金香玉" then
			玩家数据[id].角色:恢复血魔(id, 2, self.品质 * 12 + 150)
			发送数据(玩家数据[id].连接id, 7, "#y/您使用了" .. self.名称)

			玩家数据[id].道具.数据[self.临时id1] = 0
			玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil
		elseif self.名称 == "小还丹" then
			玩家数据[id].角色:恢复血魔(id, 2, self.品质 * 8 + 100)
			发送数据(玩家数据[id].连接id, 7, "#y/您使用了" .. self.名称)

			玩家数据[id].道具.数据[self.临时id1] = 0
			玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil
		elseif self.名称 == "风水混元丹" then
			玩家数据[id].角色:恢复血魔(id, 3, self.品质 * 3 + 50)
			发送数据(玩家数据[id].连接id, 7, "#y/您使用了" .. self.名称)

			玩家数据[id].道具.数据[self.临时id1] = 0
			玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil
		elseif self.名称 == "蛇蝎美人" then
			玩家数据[id].角色:恢复血魔(id, 3, self.品质 * 5 + 100)
			发送数据(玩家数据[id].连接id, 7, "#y/您使用了" .. self.名称)

			玩家数据[id].道具.数据[self.临时id1] = 0
			玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil
		elseif self.名称 == "十香返生丸" then
			玩家数据[id].角色:恢复血魔(id, 3, self.品质 * 3 + 50)
			发送数据(玩家数据[id].连接id, 7, "#y/您使用了" .. self.名称)

			玩家数据[id].道具.数据[self.临时id1] = 0
			玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil
		else
			发送数据(玩家数据[id].连接id, 7, "#y/您无法使用这样的药品")

			return 0
		end
	end

	self:索要道具(id, 数据.类型)
end

function 道具处理类:召唤兽快捷加血(id)
	self.召唤兽id = 玩家数据[id].召唤兽.数据.参战

	if 玩家数据[id].战斗 ~= 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/战斗中无法使用快捷加血功能")

		return 0
	elseif self.召唤兽id == 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/请先设置一只召唤兽为参战状态")

		return 0
	elseif 玩家数据[id].召唤兽.数据[self.召唤兽id].当前气血 == 玩家数据[id].召唤兽.数据[self.召唤兽id].气血上限 then
		发送数据(玩家数据[id].连接id, 7, "#y/您的召唤兽气血值是满的")

		return 0
	else
		self.气血差 = 玩家数据[id].召唤兽.数据[self.召唤兽id].气血上限 - 玩家数据[id].召唤兽.数据[self.召唤兽id].当前气血
		self.扣减数量 = 0

		for n = 1, 20 do
			self.临时id1 = 玩家数据[id].角色.数据.道具.包裹[n]

			if self.临时id1 ~= nil and 玩家数据[id].道具.数据[self.临时id1].名称 == "包子" and self.气血差 > 0 then
				for i = 1, 玩家数据[id].道具.数据[self.临时id1].数量 do
					if self.气血差 > 0 then
						玩家数据[id].道具.数据[self.临时id1].数量 = 玩家数据[id].道具.数据[self.临时id1].数量 - 1
						self.气血差 = self.气血差 - 100
						self.扣减数量 = self.扣减数量 + 1

						if self.气血差 < 0 then
							self.气血差 = 0
						end
					end
				end

				if 玩家数据[id].道具.数据[self.临时id1].数量 <= 0 then
					玩家数据[id].道具.数据[self.临时id1] = nil
					玩家数据[id].角色.数据.道具.包裹[n] = nil
				end
			end
		end

		if self.扣减数量 ~= 0 then
			玩家数据[id].召唤兽.数据[self.召唤兽id].当前气血 = 玩家数据[id].召唤兽.数据[self.召唤兽id].气血上限 - self.气血差

			发送数据(玩家数据[id].连接id, 7, "#y/您消耗了" .. self.扣减数量 .. "个包子")
			发送数据(玩家数据[id].连接id, 2005, 玩家数据[id].召唤兽:取头像数据())
		else
			发送数据(玩家数据[id].连接id, 7, "#y/您身上没有包子，无法使用快捷加血功能")
		end
	end
end

function 道具处理类:召唤兽快捷加蓝(id)
	self.召唤兽id = 玩家数据[id].召唤兽.数据.参战

	if 玩家数据[id].战斗 ~= 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/战斗中无法使用快捷加蓝功能")

		return 0
	elseif self.召唤兽id == 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/请先设置一只召唤兽为参战状态")

		return 0
	elseif 玩家数据[id].召唤兽.数据[self.召唤兽id].当前魔法 == 玩家数据[id].召唤兽.数据[self.召唤兽id].魔法上限 then
		发送数据(玩家数据[id].连接id, 7, "#y/您的召唤兽魔法值是满的")

		return 0
	else
		self.气血差 = 玩家数据[id].召唤兽.数据[self.召唤兽id].魔法上限 - 玩家数据[id].召唤兽.数据[self.召唤兽id].当前魔法
		self.扣减数量 = 0

		for n = 1, 20 do
			self.临时id1 = 玩家数据[id].角色.数据.道具.包裹[n]

			if self.临时id1 ~= nil and 玩家数据[id].道具.数据[self.临时id1].名称 == "鬼切草" and self.气血差 > 0 then
				for i = 1, 玩家数据[id].道具.数据[self.临时id1].数量 do
					if self.气血差 > 0 then
						玩家数据[id].道具.数据[self.临时id1].数量 = 玩家数据[id].道具.数据[self.临时id1].数量 - 1
						self.气血差 = self.气血差 - 40
						self.扣减数量 = self.扣减数量 + 1

						if self.气血差 < 0 then
							self.气血差 = 0
						end
					end
				end

				if 玩家数据[id].道具.数据[self.临时id1].数量 <= 0 then
					玩家数据[id].道具.数据[self.临时id1] = nil
					玩家数据[id].角色.数据.道具.包裹[n] = nil
				end
			end
		end

		if self.扣减数量 ~= 0 then
			玩家数据[id].召唤兽.数据[self.召唤兽id].当前魔法 = 玩家数据[id].召唤兽.数据[self.召唤兽id].魔法上限 - self.气血差

			发送数据(玩家数据[id].连接id, 7, "#y/您消耗了" .. self.扣减数量 .. "个鬼切草")
			发送数据(玩家数据[id].连接id, 2005, 玩家数据[id].召唤兽:取头像数据())
		else
			发送数据(玩家数据[id].连接id, 7, "#y/您身上没有鬼切草，无法使用快捷加蓝功能")
		end
	end
end

function 道具处理类:角色快捷加血(id)
	if 玩家数据[id].战斗 ~= 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/战斗中无法使用快捷加血功能")

		return 0
	elseif 玩家数据[id].角色.数据.当前气血 == 玩家数据[id].角色.数据.气血上限 then
		发送数据(玩家数据[id].连接id, 7, "#y/您的气血值是满的")

		return 0
	else
		self.气血差 = 玩家数据[id].角色.数据.气血上限 - 玩家数据[id].角色.数据.当前气血
		self.扣减数量 = 0

		for n = 1, 20 do
			self.临时id1 = 玩家数据[id].角色.数据.道具.包裹[n]

			if self.临时id1 ~= nil and 玩家数据[id].道具.数据[self.临时id1].名称 == "包子" and self.气血差 > 0 then
				for i = 1, 玩家数据[id].道具.数据[self.临时id1].数量 do
					if self.气血差 > 0 then
						玩家数据[id].道具.数据[self.临时id1].数量 = 玩家数据[id].道具.数据[self.临时id1].数量 - 1
						self.气血差 = self.气血差 - 100
						self.扣减数量 = self.扣减数量 + 1

						if self.气血差 < 0 then
							self.气血差 = 0
						end
					end
				end

				if 玩家数据[id].道具.数据[self.临时id1].数量 <= 0 then
					玩家数据[id].道具.数据[self.临时id1] = nil
					玩家数据[id].角色.数据.道具.包裹[n] = nil
				end
			end
		end

		if self.扣减数量 ~= 0 then
			玩家数据[id].角色.数据.当前气血 = 玩家数据[id].角色.数据.气血上限 - self.气血差

			发送数据(玩家数据[id].连接id, 7, "#y/您消耗了" .. self.扣减数量 .. "个包子")
			发送数据(玩家数据[id].连接id, 1003, 玩家数据[id].角色:获取角色气血数据())
			发送数据(玩家数据[id].连接id, 2005, 玩家数据[id].召唤兽:取头像数据())
		else
			发送数据(玩家数据[id].连接id, 7, "#y/您身上没有包子，无法使用快捷加血功能")
		end
	end
end

function 道具处理类:角色快捷加蓝(id)
	if 玩家数据[id].战斗 ~= 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/战斗中无法使用快捷加蓝功能")

		return 0
	elseif 玩家数据[id].角色.数据.当前魔法 == 玩家数据[id].角色.数据.魔法上限 then
		发送数据(玩家数据[id].连接id, 7, "#y/您的魔法值是满的")

		return 0
	else
		self.气血差 = 玩家数据[id].角色.数据.魔法上限 - 玩家数据[id].角色.数据.当前魔法
		self.扣减数量 = 0

		for n = 1, 20 do
			self.临时id1 = 玩家数据[id].角色.数据.道具.包裹[n]

			if self.临时id1 ~= nil and 玩家数据[id].道具.数据[self.临时id1].名称 == "鬼切草" and self.气血差 > 0 then
				for i = 1, 玩家数据[id].道具.数据[self.临时id1].数量 do
					if self.气血差 > 0 then
						玩家数据[id].道具.数据[self.临时id1].数量 = 玩家数据[id].道具.数据[self.临时id1].数量 - 1
						self.气血差 = self.气血差 - 40
						self.扣减数量 = self.扣减数量 + 1

						if self.气血差 < 0 then
							self.气血差 = 0
						end
					end
				end

				if 玩家数据[id].道具.数据[self.临时id1].数量 <= 0 then
					玩家数据[id].道具.数据[self.临时id1] = nil
					玩家数据[id].角色.数据.道具.包裹[n] = nil
				end
			end
		end

		if self.扣减数量 ~= 0 then
			玩家数据[id].角色.数据.当前魔法 = 玩家数据[id].角色.数据.魔法上限 - self.气血差

			发送数据(玩家数据[id].连接id, 7, "#y/您消耗了" .. self.扣减数量 .. "个鬼切草")
			发送数据(玩家数据[id].连接id, 2017, 玩家数据[id].角色:获取角色数据())
			发送数据(玩家数据[id].连接id, 2005, 玩家数据[id].召唤兽:取头像数据())
		else
			发送数据(玩家数据[id].连接id, 7, "#y/您身上没有鬼切草，无法使用快捷加蓝功能")
		end
	end
end

function 道具处理类:给予小铲子(id)
	self.道具重复 = false

	for n = 1, 20 do
		if 玩家数据[id].角色.数据.道具.包裹[n] ~= nil and self.数据[玩家数据[id].角色.数据.道具.包裹[n]].名称 == "小铲子" then
			self.道具重复 = true
		end
	end

	for n = 1, 20 do
		if 玩家数据[id].角色.数据.道具.行囊[n] ~= nil and self.数据[玩家数据[id].角色.数据.道具.行囊[n]].名称 == "小铲子" then
			self.道具重复 = true
		end
	end

	if self.道具重复 == false then
		self:给予道具(id, "小铲子", "功能", 1)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了一把小铲子")
	end
end

function 道具处理类:天眼处理(id, 数据)
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]

	if self.数据[self.临时id1] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/你没有这样的道具")

		return 0
	end

	if 玩家数据[id].角色:取任务id(4) ~= 0 then
		local 任务id = 玩家数据[id].角色:取任务id(4)

		地图处理类:指定跳转1(id, 任务数据[任务id].地图编号, math.floor(任务数据[任务id].坐标.x / 20), math.floor(任务数据[任务id].坐标.y / 20))
	else
		发送数据(玩家数据[id].连接id, 7, "#y/请先领取抓鬼任务")

		return 0
	end

	发送数据(玩家数据[id].连接id, 7, "#y/你消耗了一张天眼通符")

	if self.数据[self.临时id1].数量 == nil then
		self.数据[self.临时id1].数量 = 1
	end

	self.数据[self.临时id1].数量 = self.数据[self.临时id1].数量 - 1

	if self.数据[self.临时id1].数量 <= 0 then
		self.数据[self.临时id1] = nil
		玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil
	end

	发送数据(玩家数据[id].连接id, 3006, "66")
end

function 道具处理类:导标旗传送(id, 格子, 类型)
	local 数据 = {
		格子 = 格子,
		类型 = 类型
	}
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]

	if 玩家数据[id].队伍 ~= 0 and 玩家数据[id].队长 == false then
		发送数据(玩家数据[id].连接id, 7, "#y/只有队长才可使用此道具")
	elseif 玩家数据[id].角色:取任务id(206) ~= 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/游泳大赛过程中不可使用此功能")

		return 0
	else
		self.允许使用 = true

		if 玩家数据[id].角色:取飞行限制() == false then
			self.允许使用 = false
			self.提示信息 = "#y/您当前无法使用飞行道具"
		end

		if 玩家数据[id].队伍 ~= 0 then
			for n = 1, #队伍数据[玩家数据[id].队伍].队员数据 do
				self.临时id = 队伍数据[玩家数据[id].队伍].队员数据[n]

				if 玩家数据[self.临时id].角色:取飞行限制() == false then
					self.允许使用 = false
					self.提示信息 = "#y/" .. 玩家数据[self.临时id].角色.数据.名称 .. "无法使用飞行道具"
				end
			end
		end

		if self.允许使用 == false then
			发送数据(玩家数据[id].连接id, 7, self.提示信息)
		else
			地图处理类:指定跳转1(id, 玩家数据[id].道具.数据[self.临时id1].地图编号, 玩家数据[id].道具.数据[self.临时id1].x, 玩家数据[id].道具.数据[self.临时id1].y)

			玩家数据[id].道具.数据[self.临时id1].次数 = 玩家数据[id].道具.数据[self.临时id1].次数 - 1

			if 玩家数据[id].道具.数据[self.临时id1].次数 <= 0 then
				玩家数据[id].道具.数据[self.临时id1] = nil
				玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil

				发送数据(玩家数据[id].连接id, 7, "#y/您的导标旗次数已经用尽")
			end

			发送数据(玩家数据[id].连接id, 3006, "#y/您的导标旗次数已经用尽")
		end
	end
end

function 道具处理类:导标旗定标(id, 格子, 类型)
	local 数据 = {
		格子 = 格子,
		类型 = 类型
	}
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
	self.地图编号 = 玩家数据[id].地图

	if self.地图编号 ~= 1501 and self.地图编号 ~= 1001 and self.地图编号 ~= 1092 and self.地图编号 ~= 1070 and self.地图编号 ~= 1226 and self.地图编号 ~= 1040 then
		发送数据(玩家数据[id].连接id, 7, "#y/导标旗只能在长安城、建邺城、傲来国、长寿村、宝象国、西凉女国定标")
	else
		玩家数据[id].道具.数据[self.临时id1].地图编号 = self.地图编号
		玩家数据[id].道具.数据[self.临时id1].地图名称 = 地图处理类.地图数据[self.地图编号].名称
		玩家数据[id].道具.数据[self.临时id1].x = math.floor(玩家数据[id].角色.数据.地图数据.x / 20)
		玩家数据[id].道具.数据[self.临时id1].y = math.floor(玩家数据[id].角色.数据.地图数据.y / 20)

		发送数据(玩家数据[id].连接id, 7, "#y/定标成功")
		self:索要道具(id, 数据.类型)
	end
end

function 道具处理类:藏宝图处理(id, 格子, 类型)
	local 数据 = {
		格子 = 格子,
		类型 = 类型
	}
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]

	if 玩家数据[id].道具.数据[self.临时id1].名称 == "藏宝图" then
		self:低级藏宝图奖励(id, 数据)
	elseif 玩家数据[id].道具.数据[self.临时id1].名称 == "高级藏宝图" then
		self:高级藏宝图奖励(id, 数据)
	end
end

function 道具处理类:出售道具处理(id, 格子, 类型)
	local 数据 = {
		格子 = 格子,
		类型 = 类型
	}
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]

	if 玩家数据[id].角色.数据.道具[数据.类型][数据.格子] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/你没有这样的道具")

		return 0
	end

	self.收购价格 = 0
	self.临时类型 = 玩家数据[id].道具.数据[self.临时id1].类型
	self.临时名称 = 玩家数据[id].道具.数据[self.临时id1].名称
	local 临时参数 = 玩家数据[id].道具.数据[self.临时id1].参数 or 1
	local 临时等级 = 玩家数据[id].道具.数据[self.临时id1].等级 or 1

	if self.临时类型 == "武器" or self.临时类型 == "衣服" or self.临时类型 == "头盔" or self.临时类型 == "项链" or self.临时类型 == "腰带" or self.临时类型 == "鞋子" then
		self.临时差价 = 玩家数据[id].道具.数据[self.临时id1].等级 * 100 * 2

		if self.临时类型 == "武器" then
			self.收购价格 = (玩家数据[id].道具.数据[self.临时id1].命中 + 玩家数据[id].道具.数据[self.临时id1].伤害) * 5
		elseif self.临时类型 == "衣服" then
			self.收购价格 = 玩家数据[id].道具.数据[self.临时id1].防御 * 5
		elseif self.临时类型 == "项链" then
			self.收购价格 = 玩家数据[id].道具.数据[self.临时id1].灵力 * 30
		elseif self.临时类型 == "腰带" then
			self.收购价格 = (玩家数据[id].道具.数据[self.临时id1].防御 + 玩家数据[id].道具.数据[self.临时id1].气血) * 12
		elseif self.临时类型 == "鞋子" then
			self.收购价格 = (玩家数据[id].道具.数据[self.临时id1].敏捷 + 玩家数据[id].道具.数据[self.临时id1].防御) * 29
		elseif self.临时类型 == "头盔" then
			self.收购价格 = (玩家数据[id].道具.数据[self.临时id1].魔法 + 玩家数据[id].道具.数据[self.临时id1].防御) * 22
		end

		if 玩家数据[id].道具.数据[self.临时id1].等级 == 10 then
			self.临时差价 = 500
		end

		self.收购价格 = math.floor(self.收购价格 + self.临时差价)

		if 玩家数据[id].道具.数据[self.临时id1].特效 == "珍宝" then
			self.收购价格 = self.收购价格 * 10
		end
	elseif self.临时名称 == "魔兽要诀"  then
		self.收购价格 = 200000
	elseif self.临时名称 == "高级魔兽要诀" then
		self.收购价格 = 2000000
	elseif self.临时名称 == "定魂珠" or self.临时名称 == "避水珠" or self.临时名称 == "龙鳞" or self.临时名称 == "夜光珠" or self.临时名称 == "金刚石" then
		self.收购价格 = 300000
	elseif self.临时名称 == "超级金柳露" then
		self.收购价格 = 300000
	-- elseif self.临时名称 == "藏宝图"  then
	-- 	self.收购价格 = 20000
	elseif self.临时名称 == "月华露" then
		self.收购价格 = 80000
	elseif self.临时名称 == "灵饰指南书" then
		self.收购价格 = 100000 * 临时等级
	elseif self.临时名称 == "珍珠" then
		self.收购价格 = 5000 * 临时等级
	elseif self.临时名称 == "元灵晶石" then
		self.收购价格 = 300000
	elseif self.临时名称 == "青龙石" or self.临时名称 == "白虎石"or self.临时名称 == "朱雀石"or self.临时名称 == "玄武石" then
		self.收购价格 = 80000
	elseif self.临时名称 == "高级藏宝图"then
		self.收购价格 = 20000
	-- elseif self.临时名称 == "星辉石" then
	-- 	self.收购价格 = 50000
	elseif  self.临时名称 == "太阳石" or self.临时名称 == "红玛瑙" or self.临时名称 == "月亮石" or self.临时名称 == "舍利子" or self.临时名称 == "黑宝石" or self.临时名称 == "光芒石" or self.临时名称 == "星辉石" then
		self.收购价格 = 50000
		for k = 临时参数 - 1, 1, -1 do self.收购价格 = self.收购价格 * 2 end
	elseif  self.临时名称 == "白色合成旗" or self.临时名称 == "绿色合成旗" or self.临时名称 == "黄色合成旗" or self.临时名称 == "蓝色合成旗" or self.临时名称 == "红色合成旗" then
		self.收购价格 = 50000
	elseif self.临时名称 == "鬼谷子" then
		self.收购价格 = 100000
	elseif self.临时名称 == "彩果" then
		self.收购价格 = 800000
	elseif self.临时类型 == "符石" or self.临时类型 == "未激活符石" then
		self.收购价格 = 100000 * 临时等级
	elseif self.临时名称 == "钨金" then
		self.收购价格 = 200000
	elseif self.临时名称 == "制造指南书" or self.临时名称 == "百炼精铁" then
		self.收购价格 = 2000 * 临时参数
	elseif self.临时名称 == "上古锻造图策" then
		self.收购价格 = 10000 * 临时等级
	elseif self.临时类型 == "法宝" then
		self.收购价格 = 30000000
	elseif self.临时名称 == "法宝碎片" then
		self.收购价格 = 300000
	elseif string.find(self.临时名称, '元身') ~= nil then
		self.收购价格 = 20000000
	elseif self.临时名称 == "战魄" then
		self.收购价格 = 10000000
	elseif self.临时名称 == "召唤兽内丹" then
		self.收购价格 = 200000
	elseif self.临时名称 == "高级召唤兽内丹" then
		self.收购价格 = 2000000
	elseif self.临时名称 == "玉葫灵髓" then
		self.收购价格 = 2000000
	elseif self.临时名称 == "易经丹" then
		self.收购价格 = 2000000
	elseif string.find(self.临时名称, '玄天残卷') ~= nil then
		self.收购价格 = 20000000
	elseif self.临时名称 == "怪物卡片" then
		self.收购价格 = 100000
	elseif self.临时类型 == "锦衣" then
		self.收购价格 = 100000000
	elseif self.临时名称 == "九转金丹" then
		self.收购价格 = 10000 * (玩家数据[id].道具.数据[self.临时id1].品质 or 1)
	elseif self.临时名称 == "修炼果" then
		self.收购价格 = 1500000
	elseif self.临时名称 == "神兜兜" then
		self.收购价格 = 1000000
	elseif self.临时名称 == "海马" then
		self.收购价格 = 50000
	elseif self.临时名称 == "附魔宝珠" then -- 没用?
		self.收购价格 = 100000
	elseif self.临时名称 == "金银宝盒" then
		self.收购价格 = 50000
	elseif self.临时名称 == "吸附石" then
		self.收购价格 = 200000
	elseif self.临时名称 == "点化石" then
		self.收购价格 = 500000
	else
		self.收购价格 = 100
	end

	self.收购价格 = self.收购价格 * (玩家数据[id].道具.数据[self.临时id1].数量 or 1)

	if self.收购价格 == 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/这样的道具无法被出售")

		return 0
	else
		玩家数据[id].角色:添加银子(id, self.收购价格, 6)

		玩家数据[id].道具.数据[self.临时id1] = nil
		玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil

		发送数据(玩家数据[id].连接id, 3006, "66")
	end
end

function 道具处理类:宝石升级处理(id, 格子, 参数)
	local 数据 = {
		类型 = 参数,
		格子 = 格子
	}
	self.宝石id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]
	self.宝石id2 = 0

	for n = 1, 20 do
		if 数据.格子 ~= n and 玩家数据[id].角色.数据.道具[数据.类型][n] ~= nil and 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具[数据.类型][n]].名称 == 玩家数据[id].道具.数据[self.宝石id1].名称 and 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具[数据.类型][n]].参数 == 玩家数据[id].道具.数据[self.宝石id1].参数 then
			self.宝石id2 = n
		end
	end

	if self.宝石id2 == 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/未找到可合成的" .. 玩家数据[id].道具.数据[self.宝石id1].名称)
	else
		玩家数据[id].道具.数据[self.宝石id1].参数 = 玩家数据[id].道具.数据[self.宝石id1].参数 + 1
		玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具[数据.类型][self.宝石id2]] = nil
		玩家数据[id].角色.数据.道具[数据.类型][self.宝石id2] = nil
		local 合成几率 = 100 - 玩家数据[id].道具.数据[self.宝石id1].参数 * 5

		if 玩家数据[id].道具.数据[self.宝石id1].参数 <= 5 then
			合成几率 = 100
		end

		if 合成几率 <= 70 then
			合成几率 = 70
		end

		if 合成几率 <= 取随机数() then
			玩家数据[id].道具.数据[self.宝石id1].参数 = 玩家数据[id].道具.数据[self.宝石id1].参数 - 1

			发送数据(玩家数据[id].连接id, 7, "#y/合成失败，你损失了一颗" .. 玩家数据[id].道具.数据[self.宝石id1].名称)
		end

		if 玩家数据[id].道具.数据[self.宝石id1].名称 == "无字天书" then
			发送数据(玩家数据[id].连接id, 7, "#y/你获得了一本#r/" .. 玩家数据[id].道具.数据[self.宝石id1].参数 .. "#y/级" .. 玩家数据[id].道具.数据[self.宝石id1].名称)
		else
			发送数据(玩家数据[id].连接id, 7, "#y/你获得了一颗#r/" .. 玩家数据[id].道具.数据[self.宝石id1].参数 .. "#y/级" .. 玩家数据[id].道具.数据[self.宝石id1].名称)
		end

		发送数据(玩家数据[id].连接id, 3006, "66")
	end
end

function 道具处理类:师门兑换奖励(id)
	if 玩家数据[id].角色.数据.门贡 < 50 then
		发送数据(玩家数据[id].连接id, 7, "#y/你的门贡低于50点，无法兑换奖励")

		return 0
	end

	self.奖励参数 = 取随机数()
	玩家数据[id].角色.数据.门贡 = 玩家数据[id].角色.数据.门贡 - 50

	if self.奖励参数 <= 5 then
		self:给予道具(id, "魔兽要诀", "普通", 取低级兽诀名称())
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了一本妖怪遗留下来的秘籍")
	elseif self.奖励参数 <= 7 then
		self:给予道具(id, "神兜兜", "普通", 1)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了神兽最喜爱的神兜兜")
	elseif self.奖励参数 <= 30 then
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了" .. self:给予随机五宝(id))
	elseif self.奖励参数 <= 50 then
		self:给予道具(id, "金柳露", "普通", 1)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了金柳露")
	elseif self.奖励参数 <= 75 then
		self.生成等级 = 取随机数(8, 14) * 10

		玩家数据[id].道具:给予道具(id, "上古锻造图策", "功能", self.生成等级, 取上古锻造图策())
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了上古锻造图策")
	else
		self.临时银子 = 取随机数(100000, 500000)

		玩家数据[id].角色:添加银子(id, self.临时银子, 16)
	end
end

function 道具处理类:突破奖励(id)
	if 玩家数据[id].角色.数据.突破 ~= 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/你没有这样的奖励可领取")

		return 0
	elseif 玩家数据[id].角色.数据.突破 == 0 then
		self.佩戴装备1 = false

		for n = 21, 26 do
			if 玩家数据[id].角色.数据.装备数据[n] ~= nil then
				self.佩戴装备1 = true
			end
		end

		if self.佩戴装备1 then
			发送数据(玩家数据[id].连接id, 7, "#y/请先卸下所有已经佩戴的装备")

			return 0
		end

		if 玩家数据[id].角色:取可用格子数量("包裹") < 3 then
			发送数据(玩家数据[id].连接id, 7, "#y/请先预留出3个道具空间")

			return 0
		end

		玩家数据[id].角色.数据.突破 = 1

			local gh ={"烈焰澜翻","水墨游龙","星光熠熠","双鲤寄情","凌波微步","浩瀚星河"}
			local gh1=gh[取随机数(1,#gh)]
			玩家数据[id].角色.数据.光环=gh1
			发送数据(玩家数据[id].连接id, 7, "#y/你获得了突破奖励#r/"..gh1.."#y/光环（水墨游龙能增加人物10%伤害）")
		玩家数据[id].角色:添加储备(id, 200000000, 6)
		玩家数据[id].角色:添加经验(id, 200000000,  11)
	发送数据(玩家数据[id].连接id, 7, "#y/你的等级上限已经提升至89级")
	end
end

function 道具处理类:比武奖励(id)
	if 比武大会数据==nil or 比武大会数据[id] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/你没有这样的奖励可以领取")

		return 0
	elseif 比武大会数据[id].奖励 == false then
		发送数据(玩家数据[id].连接id, 7, "#y/你没有这样的奖励可以领取")

		return 0
	else
		比武大会数据[id].奖励 = false
		self.经验奖励 = math.floor(玩家数据[id].角色.数据.等级 * 玩家数据[id].角色.数据.等级 * 玩家数据[id].角色.数据.等级 * 2.2)
		self.银子奖励 = 比武大会数据[id].积分 * 50000

		玩家数据[id].角色:添加经验(self.经验奖励, id, 16)
		玩家数据[id].角色:添加银子(id, self.银子奖励, 21)
		发送数据(玩家数据[id].连接id, 7, "#y/你领取了比武大会奖励")
	end
end

function 道具处理类:国庆在线奖励(id)
	self.奖励参数 = 取随机数()

	if self.奖励参数 <= 5 then
		self:给予道具(id, "高级魔兽要诀", "普通", 取高级兽诀名称())
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了一本高级魔兽要诀")
	elseif self.奖励参数 <= 10 then
		self:给予道具(id, "神兜兜", "普通", 1)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了一个神兜兜")
	elseif self.奖励参数 <= 15 then
		self:给予道具(id, "元宵", "功能", 1)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了元宵")
	elseif self.奖励参数 <= 18 then
		self:给予道具(id, "蟠桃", "功能", 1)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了蟠桃")
	elseif self.奖励参数 <= 38 then
		self.宝石名称 = Q_宝图宝石[取随机数(1, #Q_宝图宝石)]

		玩家数据[id].道具:给予道具(id, self.宝石名称, "宝石", 取随机数(2, 5))
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了" .. self.宝石名称)
	elseif self.奖励参数 <= 58 then
		self.临时银子 = 取随机数(1000000, 5000000)

		玩家数据[id].角色:添加银子(id, self.临时银子, 4)
	elseif self.奖励参数 <= 78 then
		self:给予道具(id, "海马", "功能", 20)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了一只海马")
	else
		self:给予道具(id, "魔兽要诀", "普通", 取低级兽诀名称())
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了一本魔兽要诀")
	end

	发送数据(玩家数据[id].连接id, 3008, "66")
end

function 道具处理类:高级藏宝图奖励(id, 数据)
	self.奖励参数 = 取随机数(1, 120)
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]

	if 玩家数据[id].角色.数据.道具[数据.类型][数据.格子] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/你没有这样的道具")

		return 0
	end

	玩家数据[id].道具.数据[self.临时id1] = nil
	玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil

     if  self.奖励参数 <=5 then
      self:新加给予道具(id,"召唤兽内丹",取内丹("高级"))
     发送数据(玩家数据[id].连接id,7,"#y/你获得了一颗召唤兽内丹")
   elseif self.奖励参数 <= 10 then
       self:新加给予道具(id,"钨金",取随机数(12,16))
     发送数据(玩家数据[id].连接id,7,"#y/你获得了一颗钨金")
	elseif self.奖励参数 <= 20 then
		self:给予道具(id, "高级魔兽要诀", "普通", 取高级兽诀名称())
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了一本妖怪遗留下来的秘籍")
	elseif self.奖励参数 <= 25 then
		self:给予道具(id, "神兜兜", "普通", 1)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了神兽最喜爱的神兜兜")
	elseif self.奖励参数 <= 35 then
		self:给予道具(id, "上古锻造图策", "功能", 取随机数(6, 14) * 10, 取上古锻造图策())
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了上古锻造图策")
	elseif self.奖励参数 <= 45 then
		self:给予道具(id, "彩果", "普通", 1)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了彩果")
	elseif self.奖励参数 <= 50 then
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了星辉石")
		self:给予道具(id, "星辉石", "宝石", 1)
	elseif self.奖励参数 <= 20 then
		self.随机名称 = 取随机书铁()
		self.随机等级 = 取随机数(10, 15)

		玩家数据[id].道具:给予道具(id, self.随机名称, "打造", self.随机等级, self.随机等级, 取随机制造())
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了" .. self.随机名称)
	elseif self.奖励参数 <= 70 then
		self.临时银子 = 取随机数(100000, 500000)

		玩家数据[id].角色:添加银子(id, self.临时银子, 4)
	elseif self.奖励参数 <= 80 then
		self:给予道具(id, "海马", "功能", 20)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了一只海马")
	elseif self.奖励参数 <= 85 then
		self:给予道具(id, "高级魔兽要诀", "普通", 取高级兽诀名称())
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了一本妖怪遗留下来的秘籍")
	elseif self.奖励参数 <= 100 then
		self:给予道具(id, "珍珠", "普通", 取随机数(10, 15) * 10)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了珍珠")
	else
		玩家数据[id].装备:取随机装备(id, 取随机数(6, 8), true)
	end

	发送数据(玩家数据[id].连接id, 3006, "66")
end

function 道具处理类:低级藏宝图奖励(id, 数据)
	self.奖励参数 = 取随机数()
	self.临时id1 = 玩家数据[id].角色.数据.道具[数据.类型][数据.格子]

	if 玩家数据[id].角色.数据.道具[数据.类型][数据.格子] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/你没有这样的道具")

		return 0
	end

	玩家数据[id].道具.数据[self.临时id1] = nil
	玩家数据[id].角色.数据.道具[数据.类型][数据.格子] = nil
    if  self.奖励参数 <=5 then
      self:新加给予道具(id,"召唤兽内丹",取内丹("低级"))
     发送数据(玩家数据[id].连接id,7,"#y/你获得了一颗召唤兽内丹")
    elseif  self.奖励参数 <=8 then
      self:新加给予道具(id,"钨金",取随机数(12,16))
     发送数据(玩家数据[id].连接id,7,"#y/你获得了一颗钨金")
	elseif self.奖励参数 <=10 then
		self:给予道具(id, "魔兽要诀", "普通", 取低级兽诀名称())
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了一本妖怪遗留下来的秘籍")
	elseif self.奖励参数 <= 15 then
		发送数据(玩家数据[id].连接id, 7, "#y/你好像得到了点什么")
		self:给予随机五宝(id)
	elseif self.奖励参数 <= 18 then
		self.临时名称 = 取随机书铁()
		self:给予道具(id, self.临时名称, "打造", 3, 8, 取随机制造())
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了" .. self.临时名称)
	elseif self.奖励参数 <= 28 then
		self:给予道具(id, "金柳露", "普通", 1)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了传说中的金柳露")
	elseif self.奖励参数 <= 38 then
		self:给予道具(id, "花豆", "普通", 1)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了花豆")
	elseif self.奖励参数 <= 30 then
		self:给予道具(id, "彩果", "普通", 1)
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了彩果")
	elseif self.奖励参数 <= 58 then
		self.临时银子 = 取随机数(10000, 20000)

		玩家数据[id].角色:添加银子(id, self.临时银子, 4)
	elseif self.奖励参数 <= 80 then
		战斗准备类:进入处理(id, 100004, "66", 0)
	else
		发送数据(玩家数据[id].连接id, 7, "#y/你一锄头挖下去，似乎挖到了什么……")
		任务处理类:设置封妖任务(id)
	end

	发送数据(玩家数据[id].连接id, 3006, "66")
end

function 道具处理类:给予随机五宝(id)
	self.临时参数 = 取随机数(1, 5)

	if self.临时参数 == 1 then
		self:给予道具(id, "夜光珠", "普通", 0, 1)

		return "夜光珠"
	elseif self.临时参数 == 2 then
		self:给予道具(id, "避水珠", "普通", 0, 1)

		return "避水珠"
	elseif self.临时参数 == 3 then
		self:给予道具(id, "龙鳞", "普通", 0, 1)

		return "龙鳞"
	elseif self.临时参数 == 4 then
		self:给予道具(id, "定魂珠", "普通", 0, 1)

		return "定魂珠"
	elseif self.临时参数 == 5 then
		self:给予道具(id, "金刚石", "普通", 0, 1)

		return "金刚石"
	end
end

function 道具处理类:飞行符传送(id, 参数, 格子, 类型)------------修复
  local 数据={类型=类型,格子=格子,参数=参数}
 self.临时id1=玩家数据[id].角色.数据.道具[数据.类型][数据.格子]

 if 玩家数据[id].队伍~=0 and 玩家数据[id].队长==false then

   发送数据(玩家数据[id].连接id,7,"#y/只有队长才可使用此道具")
   elseif 玩家数据[id].角色:取任务id(206)~=0 then

    发送数据(玩家数据[id].连接id,7,"#y/游泳大赛过程中不可使用此功能")
    return 0

  else

   self.允许使用=true

   if 玩家数据[id].角色:取飞行限制()==false then

     self.允许使用=false
     self.提示信息="#y/您当前无法使用飞行道具"

     end

   if 玩家数据[id].队伍~=0 then

      for n=1,#队伍数据[玩家数据[id].队伍].队员数据 do

        self.临时id=队伍数据[玩家数据[id].队伍].队员数据[n]

        if 玩家数据[self.临时id].角色:取飞行限制()==false then

          self.允许使用=false
          self.提示信息="#y/"..玩家数据[self.临时id].角色.数据.名称.."无法使用飞行道具"

          end
       end
     end
   if self.允许使用==false then
     发送数据(玩家数据[id].连接id,7,self.提示信息)
    else
	   local 跳转坐标={
		 {x=208,y=112,z=1001},
		 {x=65,y=113,z=1501},
		 {x=121,y=50,z=1092},
		 {x=117,y=148,z=1070},
		 {x=110,y=88,z=1040},
		 {x=115,y=50,z=1226},
		 {x=108,y=50,z=1208},}
     地图处理类:指定跳转1(id,跳转坐标[数据.参数].z,跳转坐标[数据.参数].x,跳转坐标[数据.参数].y)
     if 玩家数据[id].道具.数据[self.临时id1].数量==nil then 玩家数据[id].道具.数据[self.临时id1].数量=0 end
        玩家数据[id].道具.数据[self.临时id1].数量=玩家数据[id].道具.数据[self.临时id1].数量-1
     if 玩家数据[id].道具.数据[self.临时id1].数量<=0 then
       玩家数据[id].道具.数据[self.临时id1]=nil
       玩家数据[id].角色.数据.道具[数据.类型][数据.格子]=nil
       end
     self:索要道具(id,数据.类型)


     end


   end


 end

function 道具处理类:佩戴灵饰(id, 格子, 类型)
	self.临时数据 = {
		格子 = 格子,
		当前类型 = 包裹转换(类型)
	}

	if self.临时数据.格子 >= 31 then
		self.临时格子 = 玩家数据[id].角色:取可用道具格子(self.临时数据.当前类型)

		if self.临时格子 == 0 then
			发送数据(玩家数据[id].连接id, 7, "#y/您的" .. self.临时数据.当前类型 .. "已经无法存储更多的道具了")

			return 0
		else
			玩家数据[id].角色.数据.道具[self.临时数据.当前类型][self.临时格子] = 玩家数据[id].角色.数据.灵饰[self.临时数据.格子]
			玩家数据[id].角色.数据.灵饰[self.临时数据.格子] = 0

			玩家数据[id].角色:刷新装备属性(id)
			self:索要灵饰(id)
			发送数据(玩家数据[id].连接id, 3006, "66")
			发送数据(玩家数据[id].连接id, 3030, "66")

			return 0
		end
	else
		self.道具编号 = 玩家数据[id].角色.数据.道具[self.临时数据.当前类型][self.临时数据.格子]
		self.道具类型 = 玩家数据[id].道具.数据[self.道具编号].类型

		if self.道具类型 == "耳饰" then
			self.佩戴格子 = 31
		elseif self.道具类型 == "佩饰" then
			self.佩戴格子 = 32
		elseif self.道具类型 == "戒指" then
			self.佩戴格子 = 33
		elseif self.道具类型 == "手镯" then
			self.佩戴格子 = 34
		end


		if 玩家数据[id].道具.数据[self.道具编号].特效 == nil and 玩家数据[id].角色.数据.等级 < 玩家数据[id].道具.数据[self.道具编号].等级 then
			发送数据(玩家数据[id].连接id, 7, "#y/您的等级不足以佩戴这种灵饰")

			return 0
		elseif 玩家数据[id].道具.数据[self.道具编号].特效 == "超级简易" and 玩家数据[id].角色.数据.等级 < 玩家数据[id].道具.数据[self.道具编号].等级 - 40 then
			发送数据(玩家数据[id].连接id, 7, "#y/您的等级不足以佩戴这种灵饰")

			return 0
		end

		if 玩家数据[id].角色.数据.灵饰[self.佩戴格子] ~= 0 then
			玩家数据[id].角色.数据.道具[self.临时数据.当前类型][self.临时数据.格子] = 玩家数据[id].角色.数据.灵饰[self.佩戴格子]
		else
			玩家数据[id].角色.数据.道具[self.临时数据.当前类型][self.临时数据.格子] = nil
		end

		玩家数据[id].角色.数据.灵饰[self.佩戴格子] = self.道具编号

		玩家数据[id].角色:刷新装备属性(id)
		self:索要灵饰(id)
		发送数据(玩家数据[id].连接id, 3006, "66")
		发送数据(玩家数据[id].连接id, 3030, "66")
	end
end

function 道具处理类:佩戴装备(id, 格子, 类型)
	self.临时数据 = {
		格子 = 格子,
		当前类型 = 包裹转换(类型)
	}

	if self.临时数据.格子 >= 21 then
		self.临时格子 = 玩家数据[id].角色:取可用道具格子(self.临时数据.当前类型)

		if self.临时格子 == 0 then
			发送数据(玩家数据[id].连接id, 7, "#y/您的" .. self.临时数据.当前类型 .. "已经无法存储更多的道具了")

			return 0
		else
			玩家数据[id].角色.数据.道具[self.临时数据.当前类型][self.临时格子] = 玩家数据[id].角色.数据.装备数据[self.临时数据.格子]
			玩家数据[id].角色.数据.装备数据[self.临时数据.格子] = nil

			玩家数据[id].角色:刷新装备属性(id)
			self:索要道具(id, 类型)
			发送数据(玩家数据[id].连接id, 3006, "66")
			发送数据(玩家数据[id].连接id, 3030, "66")

			if self.临时数据.格子 == 21 then
				发送数据(玩家数据[id].连接id, 2010, 玩家数据[id].角色:获取角色数据())

				self.发送信息 = 玩家数据[id].角色.数据.武器数据
				self.发送信息.id = id

				地图处理类:发送数据(id, 1016, 玩家数据[id].角色:获取角色数据(), 玩家数据[id].地图)
			end
		end
	else
		self.道具编号 = 玩家数据[id].角色.数据.道具[self.临时数据.当前类型][self.临时数据.格子]
		self.道具类型 = 玩家数据[id].道具.数据[self.道具编号].类型
		self.道具等级 = self.数据[self.道具编号].等级
		self.道具耐久 = self.数据[self.道具编号].耐久度
		self.道具特效 = self.数据[self.道具编号].特效
		self.名称 = self.数据[self.道具编号].名称
		self.格子序号 = 0

		if self.道具类型 == "武器" then
			self.格子序号 = 21
		elseif self.道具类型 == "头盔" then
			self.格子序号 = 22
		elseif self.道具类型 == "腰带" then
			self.格子序号 = 23
		elseif self.道具类型 == "项链" then
			self.格子序号 = 24
		elseif self.道具类型 == "鞋子" then
			self.格子序号 = 25
		elseif self.道具类型 == "衣服" then
			self.格子序号 = 26
		end

		if self.格子序号 == 0 then
			发送数据(玩家数据[id].连接id, 7, "#y/此道具无法佩戴")

			return 0
		elseif self.道具耐久 <= 0 then
			发送数据(玩家数据[id].连接id, 7, "#y/此道具的耐久度为0，已经无法佩戴了")

			return 0
		elseif self.数据[self.道具编号].专用 ~= nil and self.数据[self.道具编号].专用 ~= id then
			发送数据(玩家数据[id].连接id, 7, "#y/你无法佩戴他人的专用装备")

			return 0
		elseif self:取等级要求(id, self.道具等级, self.道具特效) == false then
			发送数据(玩家数据[id].连接id, 7, "#y/你当前的等级无法佩戴这样的装备")

			return 0
		else
			if self.道具类型 == "武器" then
				self.角色 = 游戏装备.武器[self.名称].装备角色
			else
				self.角色 = 游戏装备.防具[self.名称].装备角色
			end

			if self.道具类型 == "武器" and string.find(self.角色, 玩家数据[id].角色.数据.造型) == nil then
				发送数据(玩家数据[id].连接id, 7, "#y/你的角色无法佩戴此种武器")

				return 0
			elseif self.道具类型 ~= "武器" and self.角色 ~= "无" and string.find(self.角色, 玩家数据[id].角色.数据.性别) == nil then
				发送数据(玩家数据[id].连接id, 7, "#y/你的角色不适合佩戴此种装备")

				return 0
			else
				self.临时装备 = 玩家数据[id].角色.数据.装备数据[self.格子序号]
				玩家数据[id].角色.数据.装备数据[self.格子序号] = self.道具编号
				玩家数据[id].角色.数据.道具[self.临时数据.当前类型][self.临时数据.格子] = self.临时装备

				玩家数据[id].角色:刷新装备属性(id)
				self:索要道具(id, 类型)
				发送数据(玩家数据[id].连接id, 3006, "66")
				发送数据(玩家数据[id].连接id, 3030, "66")
			end
		end
	end
end

function 道具处理类:取等级要求(id, 等级, 特效)
	self.无级别特效 = false

	if 特效 ~= nil then
		for n = 1, #特效 do
			if 特效[n] == "无级别限制" then
				self.无级别特效 = true
			end
		end
	end

	self.简易特效 = false

	if 特效 ~= nil then
		for n = 1, #特效 do
			if 特效[n] == "简易" then
				self.简易特效 = true
			end
		end
	end

	if self.无级别特效 then
		return true
	elseif self.简易特效 and 等级 <= 玩家数据[id].角色.数据.等级 + 5 then
		return true
	elseif 等级 <= 玩家数据[id].角色.数据.等级 then
		return true
	else
		return false
	end
end

function 道具处理类:专用装备(id)
	self.临时格子 = 玩家数据[id].道具id.格子
	self.临时类型 = 玩家数据[id].道具id.类型
	self.道具编号 = 玩家数据[id].角色.数据.道具[self.临时类型][self.临时格子]

	if 玩家数据[id].道具.数据[self.道具编号].制造者 == nil then
		self.属性下限 = 1.5
		self.属性上限 = 1.8
		self.制造者 = 0
	elseif 玩家数据[id].道具.数据[self.道具编号].强化 ~= 1 then
		self.属性下限 = 打造属性.普通.下限 + 0.1
		self.属性上限 = 打造属性.普通.上限 + 0.1
		self.制造者 = 玩家数据[id].道具.数据[self.道具编号].制造玩家
	elseif 玩家数据[id].道具.数据[self.道具编号].强化 == 1 then
		self.属性下限 = 打造属性.强化.下限 + 0.1
		self.属性上限 = 打造属性.强化.上限 + 0.1
		self.制造者 = 玩家数据[id].道具.数据[self.道具编号].制造玩家
	end

	玩家数据[id].装备:生成装备(玩家数据[id].道具.数据[self.道具编号].类型, 玩家数据[id].道具.数据[self.道具编号].名称, 玩家数据[id].道具.数据[self.道具编号].等级, self.属性上限, self.属性下限, false, 玩家数据[id].道具.数据[self.道具编号].强化, self.制造者, 1, self.道具编号)

	玩家数据[id].道具.数据[self.道具编号].专用 = id

	self:鉴定道具(id, self.临时格子, self.临时类型, 1)
end

function 道具处理类:鉴定道具(id, 格子, 类型, 专用)
	self.临时数据 = {
		格子 = 格子,
		当前类型 = 类型
	}
	self.道具编号 = 玩家数据[id].角色.数据.道具[self.临时数据.当前类型][self.临时数据.格子]
	self.道具类型 = 玩家数据[id].道具.数据[self.道具编号].类型

	if self.道具类型 ~= "武器" and self.道具类型 ~= "项链" and self.道具类型 ~= "头盔" and self.道具类型 ~= "发钗" and self.道具类型 ~= "鞋子" and self.道具类型 ~= "腰带" and self.道具类型 ~= "女衣" and self.道具类型 ~= "男衣" and self.道具类型 ~= "衣服" then
		发送数据(玩家数据[id].连接id, 7, "#y/只有装备才可以使用鉴定功能")
	else
		self.临时等级 = 玩家数据[id].道具.数据[self.道具编号].等级
		self.鉴定活力 = self.临时等级
		self.鉴定费用 = math.floor(self.临时等级 * self.临时等级 * 10)

		if 玩家数据[id].角色.数据.当前活力 < self.鉴定活力 then
			发送数据(玩家数据[id].连接id, 7, "#y/本次鉴定需要消耗" .. self.鉴定活力 .. "点活力")

			return 0
		elseif 银子检查(id, self.鉴定费用) == false then
			发送数据(玩家数据[id].连接id, 7, "#y/本次鉴定需要花费" .. self.鉴定费用 .. "两银子")

			return 0
		elseif 玩家数据[id].道具.数据[self.道具编号].鉴定 then
			发送数据(玩家数据[id].连接id, 7, "#y/你的这件装备已经鉴定过了")

			return 0
		else
			if 取随机数(1, 1000) <= 1 and 专用 == nil then
				玩家数据[id].道具id = {
					格子 = 格子,
					类型 = 类型
				}
				self.连接id内容 = [[
 你的这件装备鉴定成功后将会成为仅限于你本人使用的专用装备。专用装备将会提升10%的装备属性。请确认您是否需要将此装备鉴定为专用装备？

  ]] .. "#R/ht|" .. "66" .. [[
/确认鉴定装备

  #R/ht|0/取消

       ]]
				self.发送信息 = {
					名称 = "???",
					连接id = self.连接id内容
				}

				发送数据(玩家数据[id].连接id, 20, self.发送信息)

				return 0
			end

			玩家数据[id].角色.数据.当前活力 = 玩家数据[id].角色.数据.当前活力 - self.鉴定活力
			玩家数据[id].道具.数据[self.道具编号].鉴定 = true

			玩家数据[id].角色:扣除银子(id, self.鉴定费用, 10)
			self:索要道具(id, self.临时数据.当前类型)
			发送数据(玩家数据[id].连接id, 7, "#y/鉴定成功,你消耗了" .. self.鉴定活力 .. "点活力、" .. self.鉴定费用 .. "两银子")
		end
	end
end

function 道具处理类:丢弃道具(id, 数据)
	self.临时数据 = 数据
	self.数据[玩家数据[id].角色.数据.道具[self.临时数据.类型][self.临时数据.格子]] = nil
	玩家数据[id].角色.数据.道具[self.临时数据.类型][self.临时数据.格子] = nil

	self:索要道具(id, self.临时数据.类型)
	发送数据(玩家数据[id].连接id, 7, "#y/丢弃道具成功")
end

function 道具处理类:空间互换(id, 当前编号, 放置类型, 格子)
	self.临时数据 = {
		放置类型 = 放置类型,
		当前类型 = 包裹转换(当前编号),
		格子 = 格子
	}
	self.临时格子 = 玩家数据[id].角色:取可用道具格子(self.临时数据.放置类型)

	if self.临时格子 == 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/您的" .. self.临时数据.放置类型 .. "已经无法存储更多的道具了")

		return 0
	else
		玩家数据[id].角色.数据.道具[self.临时数据.放置类型][self.临时格子] = 玩家数据[id].角色.数据.道具[self.临时数据.当前类型][self.临时数据.格子]
		玩家数据[id].角色.数据.道具[self.临时数据.当前类型][self.临时数据.格子] = nil

		self:索要道具(id, self.临时数据.当前类型)
	end
end

function 道具处理类:交换格子(id, 对象, 类型, 原始)

 self.临时数据=table.loadstring(数据)
 self.临时数据={原始格子=原始,交换格子=对象,类型=类型}

 if self.临时数据.原始格子>=21 or self.临时数据.交换格子>=21  then

    return 0

  else

   if 玩家数据[id].角色.数据.道具[self.临时数据.类型][self.临时数据.交换格子]==nil  then


     玩家数据[id].角色.数据.道具[self.临时数据.类型][self.临时数据.交换格子]=玩家数据[id].角色.数据.道具[self.临时数据.类型][self.临时数据.原始格子]
     玩家数据[id].角色.数据.道具[self.临时数据.类型][self.临时数据.原始格子]=nil
     self:索要道具(id,self.临时数据.类型)
     return 0

    else
     --print(玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具[self.临时数据.类型][self.临时数据.交换格子]].数量)

     if self.临时数据.原始格子==self.临时数据.交换格子 then
       self:索要道具(id,self.临时数据.类型)
       return 0

       end
     if 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具[self.临时数据.类型][self.临时数据.原始格子]].名称~=玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具[self.临时数据.类型][self.临时数据.交换格子]].名称 or 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具[self.临时数据.类型][self.临时数据.交换格子]].数量==nil then
        self.临时格子=玩家数据[id].角色.数据.道具[self.临时数据.类型][self.临时数据.交换格子]
        玩家数据[id].角色.数据.道具[self.临时数据.类型][self.临时数据.交换格子]=玩家数据[id].角色.数据.道具[self.临时数据.类型][self.临时数据.原始格子]
        玩家数据[id].角色.数据.道具[self.临时数据.类型][self.临时数据.原始格子]=self.临时格子
      else
        if (玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具[self.临时数据.类型][self.临时数据.原始格子]].名称 == "高级清灵仙露"
                	or 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具[self.临时数据.类型][self.临时数据.原始格子]].名称 == "中级清灵仙露"
                	or 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具[self.临时数据.类型][self.临时数据.原始格子]].名称 == "初级清灵仙露" ) and 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具[self.临时数据.类型][self.临时数据.原始格子]].灵气 ~= 玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具[self.临时数据.类型][self.临时数据.交换格子]].灵气 then
        	发送数据(玩家数据[id].连接id, 7, "#y/只有相同灵气的清灵仙露才可以叠加")
        return
            --do
        end

        self.叠加数量=玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具[self.临时数据.类型][self.临时数据.原始格子]].数量+玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具[self.临时数据.类型][self.临时数据.交换格子]].数量
        if self.叠加数量>999 then
          self.减少数量=999-玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具[self.临时数据.类型][self.临时数据.交换格子]].数量
          玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具[self.临时数据.类型][self.临时数据.原始格子]].数量=玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具[self.临时数据.类型][self.临时数据.原始格子]].数量-self.减少数量
          玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具[self.临时数据.类型][self.临时数据.交换格子]].数量=999

        else
         玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具[self.临时数据.类型][self.临时数据.交换格子]].数量=玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具[self.临时数据.类型][self.临时数据.交换格子]].数量+玩家数据[id].道具.数据[玩家数据[id].角色.数据.道具[self.临时数据.类型][self.临时数据.原始格子]].数量
         玩家数据[id].角色.数据.道具[self.临时数据.类型][self.临时数据.原始格子]=nil
         end
       end
     end

    self:索要道具(id,self.临时数据.类型)

   end


 end

function 道具处理类:删除道具(id, 格子, 类型)
	if self.数据[格子] == nil then
		return 0
	else
		self.数据[格子] = nil
		玩家数据[id].角色.数据.道具[类型][格子] = nil
	end
end
function 道具处理类:取低级兽诀名称()
	self.临时名称 = {
		"防御",
		"反击",
		"必杀",
		"吸血",
		"强力",
		"偷袭",
		"反震",
		"法术暴击",
		"冥思",
		"法术波动",
		"夜战",
		"隐身",
		"感知",
		"再生",
		"法术连击",
		"毒",
		"幸运",
		"连击",
		"永恒",
		"敏捷",
		"神佑复生",
		"驱鬼",
		"鬼魂术",
		"魔之心",
		"水攻",
		"落岩",
		"雷击",
		"烈火"
	}

	return self.临时名称[取随机数(1, #self.临时名称)]
end

function 道具处理类:取高级兽诀名称()
	self.临时名称 = {
		"高级防御",
		"高级反击",
		"高级必杀",
		"高级吸血",
		"高级强力",
		"高级偷袭",
		"高级反震",
		"高级法术暴击",
		"高级冥思",
		"高级法术波动",
		"高级夜战",
		"高级隐身",
		"高级感知",
		"高级再生",
		"高级法术连击",
		"高级毒",
		"高级幸运",
		"高级连击",
		"高级永恒",
		"高级敏捷",
		"高级神佑复生",
		"高级驱鬼",
		"高级鬼魂术",
		"高级魔之心",
		"水漫金山",
		"泰山压顶",
		"地狱烈火",
		"奔雷咒"
	}

	return self.临时名称[取随机数(1, #self.临时名称)]
end

function 道具处理类:索要炼妖数据(id, 内容)
	内容 = "包裹"

	发送数据(玩家数据[id].连接id, 3002, 玩家数据[id].召唤兽:获取数据())

	self.发送信息 = {}

	for n = 1, 20 do
		if 玩家数据[id].角色.数据.道具[内容][n] ~= nil and self.数据[玩家数据[id].角色.数据.道具[内容][n]] ~= nil then
			self.发送信息[n] = self.数据[玩家数据[id].角色.数据.道具[内容][n]]
		end
	end

	self.发送信息.类型 = 内容
	self.发送信息.银两 = 玩家数据[id].角色.数据.道具.货币.银子
	self.发送信息.存银 = 玩家数据[id].角色.数据.道具.货币.存银

	发送数据(玩家数据[id].连接id, 3003, self.发送信息)
	发送数据(玩家数据[id].连接id, 3004, "66")
end

function 道具处理类:给予书铁(id, 下限, 上限, 提示)
	self.随机名称 = 取随机书铁()

	玩家数据[id].道具:给予道具(id, self.随机名称, "打造", 下限, 上限, 取随机制造())
	发送数据(玩家数据[id].连接id, 7, "#y/你获得了" .. self.随机名称)

	return self.随机名称
end

function 道具处理类:给予宝石(id, 等级)
	self.宝石名称 = Q_宝图宝石[取随机数(1, #Q_宝图宝石)]

	self:给予道具(id, self.宝石名称, "宝石", 等级)

	return self.宝石名称
end



function 道具处理类:取道具编号()
	for n, v in pairs(self.数据) do
		if self.数据[n] == nil then
			return n
		end
	end

	return #self.数据 + 1
end

function 道具处理类:索要打造道具(id, 内容, 类型)
	self.发送信息 = {}

	for n = 1, 20 do
		if 玩家数据[id].角色.数据.道具[内容][n] ~= nil and self.数据[玩家数据[id].角色.数据.道具[内容][n]] ~= nil then
			self.发送信息[n] = self.数据[玩家数据[id].角色.数据.道具[内容][n]]
		end
	end

	self.发送信息.类型 = 内容
	self.发送信息.类别 = 类型
	self.发送信息.银两 = 玩家数据[id].角色.数据.道具.货币.银子
	self.发送信息.体力 = 玩家数据[id].角色.数据.当前体力

	发送数据(玩家数据[id].连接id, 3005, self.发送信息)
end

function 道具处理类:索要道具(id, 内容)
	if 内容 == 1 then
		内容 = "包裹"
	elseif 内容 == 2 then
		内容 = "行囊"
	elseif 内容 == 3 then
		内容 = "法宝"
	end
	if 玩家数据[id].角色.数据.法宝 == nil then
		玩家数据[id].角色.数据.法宝 = {}
	end
	if 玩家数据[id].角色.数据.道具[内容] == nil then
		玩家数据[id].角色.数据.道具[内容] = {}
	end
	self.发送信息 = {}

	for n = 1, 20 do
		if 玩家数据[id].角色.数据.道具[内容][n] ~= nil and self.数据[玩家数据[id].角色.数据.道具[内容][n]] ~= nil then
			self.发送信息[n] = self.数据[玩家数据[id].角色.数据.道具[内容][n]]
		end
	end

	if 内容 ~= "法宝" then
		for n = 21, 26 do
			if 玩家数据[id].角色.数据.装备数据[n] ~= nil and self.数据[玩家数据[id].角色.数据.装备数据[n]] ~= nil then
				self.发送信息[n] = self.数据[玩家数据[id].角色.数据.装备数据[n]]
			end
		end
	else
		for n = 35,38 do
			if 玩家数据[id].角色.数据.法宝[n] ~= nil and self.数据[玩家数据[id].角色.数据.法宝[n]] ~= nil then
				self.发送信息[n] = self.数据[玩家数据[id].角色.数据.法宝[n]]
			end
		end
	end

	self.发送信息.类型 = 内容
	self.发送信息.银两 = 玩家数据[id].角色.数据.道具.货币.银子
	self.发送信息.存银 = 玩家数据[id].角色.数据.道具.货币.存银
	self.发送信息.储备 = 玩家数据[id].角色.数据.道具.货币.储备

	发送数据(玩家数据[id].连接id, 3001, self.发送信息)
end

function 道具处理类:仓库拿走事件(id, 格子, 仓库, 类型)
	local 内容 = {
		当前类型 = 类型,
		格子 = 格子,
		仓库 = 仓库
	}
	self.可用格子 = 玩家数据[id].角色:取可用道具格子(内容.当前类型)

	if self.可用格子 == 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/您身上没有足够的空间")

		return 0
	else
		玩家数据[id].角色.数据.道具[内容.当前类型][self.可用格子] = 玩家数据[id].角色.数据.仓库[内容.仓库][内容.格子]
		玩家数据[id].角色.数据.仓库[内容.仓库][内容.格子] = nil

		发送数据(玩家数据[id].连接id, 3009, self:索要道具1(id, 内容.当前类型))
		发送数据(玩家数据[id].连接id, 3010, self:索要仓库道具(id, 内容.仓库))
		发送数据(玩家数据[id].连接id, 3006, "66")
	end
end
function 道具处理类:索要法宝道具(id, 内容)
	--if 玩家数据[id].角色.数据.道具[内容] == nil then return end
	self.发送信息 = {}

	for n = 1, 20 do
		if 玩家数据[id].角色.数据.道具[内容][n] ~= nil and self.数据[玩家数据[id].角色.数据.道具[内容][n]] ~= nil then
			self.发送信息[n] = self.数据[玩家数据[id].角色.数据.道具[内容][n]]
			self.发送信息[n].编号 = n
		end
	end

	self.发送信息.类型 = 内容
	self.发送信息.银两 = 玩家数据[id].角色.数据.道具.货币.银子
	self.发送信息.存银 = 玩家数据[id].角色.数据.道具.货币.存银

	return self.发送信息
end
function 道具处理类:仓库存放事件(id, 格子, 仓库, 类型)
	local 内容 = {
		当前类型 = 类型,
		格子 = 格子,
		仓库 = 仓库
	}
	self.可用格子 = 0

	for n = 1, 20 do
		if 玩家数据[id].角色.数据.仓库[内容.仓库][n] == 玩家数据[id].角色.数据.道具[内容.当前类型][内容.格子] then
			玩家数据[id].角色.数据.道具[内容.当前类型][内容.格子] = nil

			发送数据(玩家数据[id].连接id, 3009, self:索要道具1(id, 内容.当前类型))
			发送数据(玩家数据[id].连接id, 3010, self:索要仓库道具(id, 内容.仓库))
			发送数据(玩家数据[id].连接id, 3006, "66")

			return 0
		end

		if self.可用格子 == 0 and 玩家数据[id].角色.数据.仓库[内容.仓库][n] == nil then
			self.可用格子 = n
		end
	end

	if self.可用格子 == 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/您当前的仓库已经无法存放更多的道具了")

		return 0
	else
		玩家数据[id].角色.数据.仓库[内容.仓库][self.可用格子] = 玩家数据[id].角色.数据.道具[内容.当前类型][内容.格子]
		玩家数据[id].角色.数据.道具[内容.当前类型][内容.格子] = nil

		发送数据(玩家数据[id].连接id, 3009, self:索要道具1(id, 内容.当前类型))
		发送数据(玩家数据[id].连接id, 3010, self:索要仓库道具(id, 内容.仓库))
		发送数据(玩家数据[id].连接id, 3006, "66")
	end
end

function 道具处理类:索要灵饰(id)
	self.发送信息 = {}

	for n = 31, 34 do
		if 玩家数据[id].角色.数据.灵饰[n] ~= 0 then
			self.发送信息[n] = table.copy(玩家数据[id].道具.数据[玩家数据[id].角色.数据.灵饰[n]])
		else
			self.发送信息[n] = 0
		end
	end

	发送数据(玩家数据[id].连接id, 20027, self.发送信息)
end
function 道具处理类:佩戴锦衣(id, 格子, 类型)
	self.临时数据 = {
		格子 = 格子,
		当前类型 = 包裹转换(类型)
	}

	if self.临时数据.格子 >= 35 then
		self.临时格子 = 玩家数据[id].角色:取可用道具格子(self.临时数据.当前类型)

		if self.临时格子 == 0 then
			发送数据(玩家数据[id].连接id, 7, "#y/您的" .. self.临时数据.当前类型 .. "已经无法存储更多的道具了")

			return 0
		else
			玩家数据[id].角色.数据.道具[self.临时数据.当前类型][self.临时格子] = 玩家数据[id].角色.数据.锦衣[self.临时数据.格子]
			玩家数据[id].角色.数据.锦衣[self.临时数据.格子] = 0

			玩家数据[id].角色:刷新装备属性(id)
			self:索要锦衣(id)
			发送数据(玩家数据[id].连接id, 2010, 玩家数据[id].角色:获取角色数据())
			发送数据(玩家数据[id].连接id, 3006, "66")
			发送数据(玩家数据[id].连接id, 3030, "66")

			return 0
		end
	else
		self.道具编号 = 玩家数据[id].角色.数据.道具[self.临时数据.当前类型][self.临时数据.格子]
		self.道具类型 = 玩家数据[id].道具.数据[self.道具编号].部位

		if self.道具类型 == "衣服" then
			self.佩戴格子 = 35
		elseif self.道具类型 == "光环" then
			self.佩戴格子 = 36
		elseif self.道具类型 == "足迹" then
			self.佩戴格子 = 37
		elseif self.道具类型 == "翅膀" then
			self.佩戴格子 = 38
		end



		if 玩家数据[id].角色.数据.锦衣[self.佩戴格子] ~= 0 then
			玩家数据[id].角色.数据.道具[self.临时数据.当前类型][self.临时数据.格子] = 玩家数据[id].角色.数据.锦衣[self.佩戴格子]
		else
			玩家数据[id].角色.数据.道具[self.临时数据.当前类型][self.临时数据.格子] = nil
		end

		玩家数据[id].角色.数据.锦衣[self.佩戴格子] = self.道具编号

		玩家数据[id].角色:刷新装备属性(id)
		self:索要锦衣(id)
		发送数据(玩家数据[id].连接id, 2010, 玩家数据[id].角色:获取角色数据())
		发送数据(玩家数据[id].连接id, 3006, "66")
		发送数据(玩家数据[id].连接id, 3030, "66")
	end
end

function 道具处理类:索要锦衣(id)
	self.发送信息 = {}

	for n = 35, 38 do
		if 玩家数据[id].角色.数据.锦衣[n] ~= 0 then
			self.发送信息[n] = table.copy(玩家数据[id].道具.数据[玩家数据[id].角色.数据.锦衣[n]])
		else
			self.发送信息[n] = 0
		end
	end

	发送数据(玩家数据[id].连接id, 20039, self.发送信息)
end
function 道具处理类:索要仓库道具(id, 内容)
	self.发送信息 = {}
	内容 = 内容 + 0

	for n = 1, 20 do
		if 玩家数据[id].角色.数据.仓库[内容][n] ~= nil then
			self.发送信息[n] = {
				道具 = self.数据[玩家数据[id].角色.数据.仓库[内容][n]],
				编号 = n
			}
		end
	end

	self.发送信息.类型 = 内容
	self.发送信息.总数 = #玩家数据[id].角色.数据.仓库

	return self.发送信息
end
function 道具处理类:索要道具1(id, 内容)
	self.发送信息 = {}

	for n = 1, 20 do
		if 玩家数据[id].角色.数据.道具[内容][n] ~= nil and self.数据[玩家数据[id].角色.数据.道具[内容][n]] ~= nil then
			self.发送信息[n] = self.数据[玩家数据[id].角色.数据.道具[内容][n]]
			self.发送信息[n].编号 = n
		end
	end

	self.发送信息.类型 = 内容
	self.发送信息.银两 = 玩家数据[id].角色.数据.道具.货币.银子
	self.发送信息.存银 = 玩家数据[id].角色.数据.道具.货币.存银

	return self.发送信息
end
function 道具处理类:索要道具3(id, 内容)
	self.发送信息 = {}
	for n = 1, 20 do
		if 玩家数据[id].角色.数据.道具[内容][n] ~= nil and self.数据[玩家数据[id].角色.数据.道具[内容][n]] ~= nil then
			self.发送信息[n] = self.数据[玩家数据[id].角色.数据.道具[内容][n]]
			self.发送信息[n].编号 = n
		end
	end
    self.发送信息.体力=玩家数据[id].角色.数据.当前体力
	self.发送信息.类型 = 内容
	self.发送信息.银子 = 玩家数据[id].角色.数据.道具.货币.银子
	self.发送信息.存银 = 玩家数据[id].角色.数据.道具.货币.存银

	return self.发送信息
end
function 道具处理类:索要道具2(id, 内容)
	self.发送信息 = {}

	for n = 1, 20 do
		if 玩家数据[id].角色.数据.道具[内容][n] ~= nil and self.数据[玩家数据[id].角色.数据.道具[内容][n]] ~= nil then
			self.发送信息[n] = self.数据[玩家数据[id].角色.数据.道具[内容][n]]
		end
	end

	self.发送信息.类型 = 内容
	self.发送信息.银两 = 玩家数据[id].角色.数据.道具.货币.银子
	self.发送信息.存银 = 玩家数据[id].角色.数据.道具.货币.存银

	return self.发送信息
end

function 道具处理类:存档(id)
	写出文件(data目录 .. 玩家数据[id].账号 .. _道具txt, table.tostring(self.数据))
end



return 道具处理类















