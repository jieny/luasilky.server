local 地图坐标类 = class()

function 地图坐标类:初始化(编号)
	self.编号 = 编号
	-- self.char = ffi.new("char[2097152]")
	-- self.临时文件 = 程序目录 .. "map1/" .. 编号 .. ".map"
	-- self.临时文件 = 服务端参数.目录 .. 编号 .. ".map"
	-- self.p = tonumber(ffi.cast("intptr_t", self.char))
	-- self.map = require("glow/MAP类")(self.临时文件, self.p, 2097152)
	-- self.map = 地图类.创建(self.临时文件, self.p, 2097152)
	self.宽度 = ccc_getMapw(编号) -- self.map.宽度
	self.高度 = ccc_getMaph(编号) --self.map.高度
	-- self.行数 = self.map.行数
	-- self.列数 = self.map.列数
	-- local 障碍宽度 = self.列数 * 16
	-- local 障碍高度 = self.行数 * 12
	-- self.寻路 = require("Astart类")(障碍宽度, 障碍高度, self.map:取障碍())
	-- self.可用坐标 = {}
	-- self.操作序列 = 1
	-- self.格子数据 = {
	-- 	x = self.宽度 / 20,
	-- 	y = self.高度 / 20
	-- }
    -- self.p ={}
	-- self.map ={}
	-- self.char={}
	-- for n = 1, self.格子数据.x, 1 do
	-- 	for i = 1, self.格子数据.y, 1 do
	-- 		if self.寻路:检查点(n, i) then
	-- 			self.可用坐标[#self.可用坐标 + 1] = {
	-- 				x = n,
	-- 				y = i
	-- 			}
	-- 		end
	-- 	end
	-- end
end

function 地图坐标类:取随机格子()
	self.临时格子 = {
		x = math.floor(取随机数(1, self.宽度)),
		y = math.floor(取随机数(1, self.高度))
	}

	while ccc_getObstacles(self.编号, self.临时格子.x, self.临时格子.y) == 1 do --self.寻路:检查点(self.临时格子.x, self.临时格子.y) == false do
		self.临时格子 = {
			x = math.floor(取随机数(1, self.宽度)),
			y = math.floor(取随机数(1, self.高度))
		}
	end

	return self.临时格子
end

function 地图坐标类:取随机格子1()
	self.随机坐标 = 取随机数(1, #self.可用坐标)

	return {
		x = self.可用坐标[self.随机坐标].x * 20,
		y = self.可用坐标[self.随机坐标].y * 20
	}
end

function 地图坐标类:置随机格子()
	self.可用坐标 = {}

	for n = 1, 666, 1 do
		self.可用坐标[n] = self:取随机格子1()
	end
end

function 地图坐标类:更新(dt)
end

function 地图坐标类:显示(x, y)
end

return 地图坐标类
