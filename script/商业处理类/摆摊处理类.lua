local 摆摊处理类 = class()

function 摆摊处理类:初始化()
end

function 摆摊处理类:更新(dt)
end

function 摆摊处理类:数据处理(id, 序号, 参数, 内容)
	if 序号 == 1 then
		self:发送摊位信息(id)
	elseif 序号 == 2 then
		self:商品上架(id, 参数 + 0, 内容)
	elseif 序号 == 3 then
		self:更改招牌(id, 内容)
	elseif 序号 == 4 then
		发送数据(玩家数据[id].连接id, 20002, 玩家数据[id].道具:索要道具1(id, "包裹"))
	elseif 序号 == 5 then
		发送数据(玩家数据[id].连接id, 20005, 玩家数据[id].召唤兽:获取数据())
	elseif 序号 == 6 then
		self:bb上架(id, 参数 + 0, 内容)
	elseif 序号 == 7 then
		self:发送玩家摊位信息(id, 参数)
	elseif 序号 == 8 then
		self:购买道具(id, 参数, 内容)
	elseif 序号 == 9 then
		if 玩家数据[id].摊位id == nil then
			发送数据(玩家数据[id].连接id, 7, "#y/这样的摊位不存在")

			return 0
		else
			self.摊位id = 玩家数据[id].摊位id

			if 摊位数据[self.摊位id] == nil or 摊位数据[self.摊位id].状态 == false or 玩家数据[self.摊位id] == nil then
				发送数据(玩家数据[id].连接id, 7, "#y/这样的摊位不存在")

				return 0
			end

			发送数据(玩家数据[id].连接id, 20011, self:索取摊位道具(self.摊位id))

			玩家数据[id].查看摊位 = os.time()
		end
	elseif 序号 == 10 then
		if 玩家数据[id].摊位id == nil then
			发送数据(玩家数据[id].连接id, 7, "#y/这样的摊位不存在")

			return 0
		else
			self.摊位id = 玩家数据[id].摊位id

			if 摊位数据[self.摊位id] == nil or 摊位数据[self.摊位id].状态 == false or 玩家数据[self.摊位id] == nil then
				发送数据(玩家数据[id].连接id, 7, "#y/这样的摊位不存在")

				return 0
			end

			发送数据(玩家数据[id].连接id, 20013, self:索取摊位bb(self.摊位id))

			玩家数据[id].查看摊位 = os.time()
		end
	elseif 序号 == 11 then
		self:购买bb(id, 参数, 内容)
	elseif 序号 == 12 then
		摊位数据[id].状态 = false
		玩家数据[id].摆摊 = nil

		发送数据(玩家数据[id].连接id, 7, "#y/收摊回家玩老婆咯！")
		发送数据(玩家数据[id].连接id, 20016, "77")
		发送数据(玩家数据[id].连接id, 20008,"")
		地图处理类:更新摊位(id, "", 玩家数据[id].地图)
	end
end

function 摆摊处理类:更改招牌(id, 名称)
	摊位数据[id].名称 = 名称
	摊位数据[id].摆摊 = id

	发送数据(玩家数据[id].连接id, 7, "#y/更改招牌成功")
	发送数据(玩家数据[id].连接id, 20008, 摊位数据[id].名称)
	地图处理类:更新摊位(id, 名称, 玩家数据[id].地图)
end

function 摆摊处理类:bb上架(id, 编号, 价格)
	if 玩家数据[id].召唤兽.数据[编号] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/你没有这样的召唤兽")

		return 0
	else
		self.操作编号 = self:取bb状态(id, 编号)

		if self.操作编号 == 0 then
			if 价格 + 0 < 1 then
				发送数据(玩家数据[id].连接id, 7, "#y/商品的最低价格不能低于1两银子")

				return 0
			end

			摊位数据[id].bb[编号] = {
				id = 编号,
				价格 = 价格
			}

			发送数据(玩家数据[id].连接id, 20006, 摊位数据[id].bb[编号])
		else
			摊位数据[id].bb[编号] = nil

			发送数据(玩家数据[id].连接id, 20007, 编号)
		end

		摊位数据[id].刷新 = os.time()
	end
end

function 摆摊处理类:购买bb(id, 编号, 数量)
	if 玩家数据[id].摊位id == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/这样的摊位不存在")

		return 0
	else
		self.摊位id = 玩家数据[id].摊位id

		if 摊位数据[self.摊位id] == nil or 摊位数据[self.摊位id].状态 == false or 玩家数据[self.摊位id] == nil then
			发送数据(玩家数据[id].连接id, 7, "#y/这样的摊位不存在")

			return 0
		elseif 玩家数据[id].查看摊位 <= 摊位数据[self.摊位id].刷新 then
			发送数据(玩家数据[id].连接id, 7, "#y/该摊位商品发生了变化，请稍后再试")
			发送数据(玩家数据[id].连接id, 20013, self:索取摊位bb(self.摊位id))

			玩家数据[id].查看摊位 = os.time()

			return 0
		end

		if 摊位数据[self.摊位id].bb[编号] == nil then
			发送数据(玩家数据[id].连接id, 7, "#y/这样的商品不存在")
			发送数据(玩家数据[id].连接id, 20013, self:索取摊位bb(self.摊位id))

			return 0
		end

		self.bbid = 摊位数据[self.摊位id].bb[编号].id
		self.购买价格 = 摊位数据[self.摊位id].bb[编号].价格 + 0

		if 玩家数据[self.摊位id].召唤兽.数据[self.bbid] == nil or 玩家数据[self.摊位id].召唤兽.数据[self.bbid] == 0 then
			发送数据(玩家数据[id].连接id, 7, "#y/这样的商品不存在")
			发送数据(玩家数据[id].连接id, 20013, self:索取摊位bb(self.摊位id))

			return 0
		elseif #玩家数据[id].召唤兽.数据 >= 5 then
			发送数据(玩家数据[id].连接id, 7, "#y/您当前无法携带更多的召唤兽了")

			return 0
		elseif 银子检查(id, self.购买价格) == false then
			发送数据(玩家数据[id].连接id, 7, "#y/您没有那么多的银子")

			return 0
		end

		玩家数据[id].角色:扣除银子(id, self.购买价格, 29)

		玩家数据[id].召唤兽.数据[#玩家数据[id].召唤兽.数据 + 1] = table.loadstring(玩家数据[self.摊位id].召唤兽:获取指定数据(self.bbid))

		发送数据(玩家数据[id].连接id, 7, "#w/你花费了#r/" .. self.购买价格 .. "#w/两银子购买了#g/" .. 玩家数据[id].召唤兽.数据[#玩家数据[id].召唤兽.数据].名称)
		玩家数据[id].角色:添加消费日志("[摊位系统-购买]花费" .. self.购买价格 .. "两银子购买了" .. 玩家数据[id].召唤兽.数据[#玩家数据[id].召唤兽.数据].名称 .. "，类型为" .. 玩家数据[id].召唤兽.数据[#玩家数据[id].召唤兽.数据].造型 .. "，出售者为" .. 玩家数据[self.摊位id].账号)
		玩家数据[self.摊位id].角色:添加消费日志("[摊位系统-出售]获得" .. self.购买价格 .. "两银子购买了" .. 玩家数据[id].召唤兽.数据[#玩家数据[id].召唤兽.数据].名称 .. "，类型为" .. 玩家数据[id].召唤兽.数据[#玩家数据[id].召唤兽.数据].造型 .. "，购买者为" .. 玩家数据[id].账号)
		玩家数据[self.摊位id].角色:添加银子(self.摊位id, self.购买价格, 29)
		发送数据(玩家数据[self.摊位id].连接id, 7, "#w/出售#g/" .. 玩家数据[id].召唤兽.数据[#玩家数据[id].召唤兽.数据].名称 .. "#w/获得了#r/" .. self.购买价格 .. "#w/两银子")

		玩家数据[self.摊位id].召唤兽.数据[self.bbid] = nil
		摊位数据[self.摊位id].bb[编号] = nil
		self.参战数据 = 玩家数据[self.摊位id].召唤兽.数据.参战
		self.原始数据 = {}

		for n = 1, 8 do
			if 玩家数据[self.摊位id].召唤兽.数据[n] ~= nil and 玩家数据[self.摊位id].召唤兽.数据[n] ~= 0 then
				self.原始数据[#self.原始数据 + 1] = {
					bb = table.loadstring(玩家数据[self.摊位id].召唤兽:获取指定数据(n)),
					编号 = n
				}
			end
		end

		玩家数据[self.摊位id].召唤兽.数据 = {
			参战 = 0
		}
		self.记录数据 = {}

		for n = 1, #self.原始数据 do
			玩家数据[self.摊位id].召唤兽.数据[n] = table.copy(self.原始数据[n].bb)

			if self.原始数据[n].编号 == self.参战数据 then
				玩家数据[self.摊位id].召唤兽.数据.参战 = n
			end

			for i = 1, 10 do
				if 摊位数据[self.摊位id].bb[i] ~= nil and 摊位数据[self.摊位id].bb[i].id == self.原始数据[n].编号 then
					self.记录数据[n] = {
						id = n,
						价格 = 摊位数据[self.摊位id].bb[i].价格
					}
				end
			end
		end

		摊位数据[self.摊位id].bb = {}

		for n = 1, 10 do
			if self.记录数据[n] ~= nil then
				摊位数据[self.摊位id].bb[n] = {
					id = self.记录数据[n].id,
					价格 = self.记录数据[n].价格
				}
			end
		end

		发送数据(玩家数据[self.摊位id].连接id, 20014, "11")
		发送数据(玩家数据[id].连接id, 20013, self:索取摊位bb(self.摊位id))
		发送数据(玩家数据[id].连接id, 3040, "6")
		发送数据(玩家数据[self.摊位id].连接id, 3040, "6")
		发送数据(玩家数据[self.摊位id].连接id, 20015, 摊位数据[self.摊位id].bb)

		摊位数据[self.摊位id].刷新 = os.time()
	end
end

function 摆摊处理类:购买道具(id, 编号, 数量)
	if 玩家数据[id].摊位id == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/这样的摊位不存在")

		return 0
	else
		self.摊位id = 玩家数据[id].摊位id

		if 摊位数据[self.摊位id] == nil or 摊位数据[self.摊位id].状态 == false or 玩家数据[self.摊位id] == nil then
			发送数据(玩家数据[id].连接id, 7, "#y/这样的摊位不存在")

			return 0
		elseif 玩家数据[id].查看摊位 <= 摊位数据[self.摊位id].刷新 then
			发送数据(玩家数据[id].连接id, 7, "#y/该摊位商品发生了变化，请稍后再试")
			发送数据(玩家数据[id].连接id, 20011, self:索取摊位道具(self.摊位id))

			玩家数据[id].查看摊位 = os.time()

			return 0
		end

		if 摊位数据[self.摊位id].道具[编号] == nil then
			发送数据(玩家数据[id].连接id, 7, "#y/这样的商品不存在")
			发送数据(玩家数据[id].连接id, 20011, self:索取摊位道具(self.摊位id))

			玩家数据[id].查看摊位 = os.time()

			return 0
		else
			self.包裹id1 = 摊位数据[self.摊位id].道具[编号].id

			if 玩家数据[self.摊位id].角色.数据.道具.包裹[self.包裹id1] == nil then
				发送数据(玩家数据[id].连接id, 7, "#y/这样的商品不存在")
				发送数据(玩家数据[id].连接id, 20011, self:索取摊位道具(self.摊位id))

				return 0
			end

			self.道具id1 = 玩家数据[self.摊位id].角色.数据.道具.包裹[self.包裹id1]

			if 玩家数据[self.摊位id].道具.数据[self.道具id1] == nil or 玩家数据[self.摊位id].道具.数据[self.道具id1] == 0 then
				发送数据(玩家数据[id].连接id, 7, "#y/这样的商品不存在")
				发送数据(玩家数据[id].连接id, 20011, self:索取摊位道具(self.摊位id))

				玩家数据[id].查看摊位 = os.time()

				return 0
			end

			self.购买数量 = 数量 + 0

			if self.购买数量 <= 0 or self.购买数量 >= 99 then
				发送数据(玩家数据[id].连接id, 7, "#y/请输入正确的购买数量")

				return 0
			end

			if 玩家数据[self.摊位id].道具.数据[self.道具id1].数量 == nil then
				self.购买数量 = 1
			elseif self.购买数量 > 1 and 玩家数据[self.摊位id].道具.数据[self.道具id1].数量 <= self.购买数量 then
				self.购买数量 = 玩家数据[self.摊位id].道具.数据[self.道具id1].数量
			end

			self.扣除价格 = 摊位数据[self.摊位id].道具[编号].价格 * self.购买数量

			if 银子检查(id, self.扣除价格) == false then
				发送数据(玩家数据[id].连接id, 7, "#y/你没有那么多的银子")

				return 0
			elseif 玩家数据[id].角色:取可用道具格子("包裹") == 0 then
				发送数据(玩家数据[id].连接id, 7, "#y/您当前的包裹空间不足")
				return 0
			end

			玩家数据[id].角色:扣除银子(id, self.扣除价格, 24)

			self.可用id = 玩家数据[id].角色:取可用道具格子("包裹")
			self.道具id = 玩家数据[id].道具:取道具编号()
			玩家数据[id].道具.数据[self.道具id] = table.copy(玩家数据[self.摊位id].道具.数据[self.道具id1])

			if 玩家数据[self.摊位id].道具.数据[self.道具id1].数量 ~= nil and 玩家数据[self.摊位id].道具.数据[self.道具id1].数量 >= 1 then
				玩家数据[id].道具.数据[self.道具id].数量 = self.购买数量
				玩家数据[self.摊位id].道具.数据[self.道具id1].数量 = 玩家数据[self.摊位id].道具.数据[self.道具id1].数量 - self.购买数量

				if 玩家数据[self.摊位id].道具.数据[self.道具id1].数量 <= 0 then
					玩家数据[self.摊位id].角色.数据.道具.包裹[self.包裹id1] = nil
					玩家数据[self.摊位id].道具.数据[self.道具id1] = 0
					摊位数据[self.摊位id].道具[编号] = nil
				end
			else
				玩家数据[self.摊位id].角色.数据.道具.包裹[self.包裹id1] = nil
				玩家数据[self.摊位id].道具.数据[self.道具id1] = 0
				摊位数据[self.摊位id].道具[编号] = nil
			end
			玩家数据[id].角色.数据.道具.包裹[self.可用id] = self.道具id
			玩家数据[id].角色:添加消费日志("[摊位系统-购买]花费" .. self.扣除价格 .. "两银子购买" .. 玩家数据[id].道具.数据[self.道具id].名称 .. "，出售者=" .. 玩家数据[self.摊位id].账号)
			玩家数据[self.摊位id].角色:添加消费日志("[摊位系统-出售]获得" .. self.扣除价格 .. "两银子，出售了" .. 玩家数据[id].道具.数据[self.道具id].名称 .. "，购买者=" .. 玩家数据[id].账号)
			玩家数据[self.摊位id].角色:添加银子(self.摊位id, self.扣除价格, 29)
			发送数据(玩家数据[id].连接id, 7, "#w/您花费了#r/" .. self.扣除价格 .. "#w/两银子购买了#g/" .. 玩家数据[id].道具.数据[self.道具id].名称 .. "*" .. self.购买数量)
			发送数据(玩家数据[self.摊位id].连接id, 7, "#w/出售#g/" .. 玩家数据[id].道具.数据[self.道具id].名称 .. "*" .. self.购买数量 .. "#w/获得#r/" .. self.扣除价格 .. "#w/两银子")
			摊位数据[self.摊位id].记录 = 摊位数据[self.摊位id].记录 .. "[" .. 测试时间(os.time()) .. "]#h/出售#g/" .. 玩家数据[id].道具.数据[self.道具id].名称 .. "*" .. self.购买数量 .. "#h/获得#r/" .. self.扣除价格 .. "#w/两银子" .. "\n"
			发送数据(玩家数据[id].连接id, 3006, "66")
			发送数据(玩家数据[self.摊位id].连接id, 3006, "66")
			发送数据(玩家数据[id].连接id, 20011, self:索取摊位道具(self.摊位id))
			发送数据(玩家数据[self.摊位id].连接id, 20012, "66")
			摊位数据[self.摊位id].刷新 = os.time()
			玩家数据[id].查看摊位 = os.time() + 1
		end
	end
end

function 摆摊处理类:商品上架(id, 编号, 价格)
	if 玩家数据[id].角色.数据.道具.包裹[编号] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/你没有这样的道具")

		return 0
	else
		self.操作编号 = self:取道具状态(id, 编号)

		if self.操作编号 == 0 then
			if 价格 + 0 < 1 then
				发送数据(玩家数据[id].连接id, 7, "#y/商品的最低价格不能低于1两银子")

				return 0
			end

			摊位数据[id].道具[编号] = {
				id = 编号,
				价格 = 价格
			}

			发送数据(玩家数据[id].连接id, 20003, 摊位数据[id].道具[编号])
		else
			摊位数据[id].道具[self.操作编号] = nil

			发送数据(玩家数据[id].连接id, 20004, self.操作编号)
		end

		摊位数据[id].刷新 = os.time()
	end
end

function 摆摊处理类:取道具状态(id, 编号)
	if 摊位数据[id] == nil then
		return 0
	else
		for n = 1, 20 do
			if 摊位数据[id].道具[n] ~= nil and 摊位数据[id].道具[n].id == 编号 then
				return n
			end
		end
	end

	return 0
end

function 摆摊处理类:取bb状态(id, 编号)
	if 摊位数据[id] == nil then
		return 0
	else
		for n = 1, 10 do
			if 摊位数据[id].bb[n] ~= nil and n == 编号 then
				return n
			end
		end
	end

	return 0
end

function 摆摊处理类:发送玩家摊位信息(id, 玩家编号)
	if 摊位数据[玩家编号] == nil or 摊位数据[玩家编号].状态 == false or 玩家数据[玩家编号] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/这个摊位不存在")

		return 0
	end

	摊位数据[玩家编号].次数 = 摊位数据[玩家编号].次数 + 1

	发送数据(玩家数据[id].连接id, 20010, {
		id = 玩家编号,
		名称 = 摊位数据[玩家编号].名称,
		银子 = 玩家数据[id].角色.数据.道具.货币.银子,
		角色 = 玩家数据[玩家编号].角色.数据.名称
	})
	发送数据(玩家数据[id].连接id, 20011, self:索取摊位道具(玩家编号))

	玩家数据[id].查看摊位 = os.time()
	玩家数据[id].摊位id = 玩家编号
end

function 摆摊处理类:索取摊位bb(玩家编号)
	if 摊位数据[玩家编号] == nil or 摊位数据[玩家编号].状态 == false or 玩家数据[玩家编号] == nil then
		return {}
	end

	self.发送数据 = {}

	for n = 1, 10 do
		if 摊位数据[玩家编号].bb[n] ~= nil then
			self.发送数据[n] = {
				bb = 玩家数据[玩家编号].召唤兽:获取指定数据1(摊位数据[玩家编号].bb[n].id),
				单价 = 摊位数据[玩家编号].bb[n].价格
			}
		end
	end

	return self.发送数据
end

function 摆摊处理类:索取摊位道具(玩家编号)
	if 摊位数据[玩家编号] == nil or 摊位数据[玩家编号].状态 == false or 玩家数据[玩家编号] == nil then
		return {}
	end

	self.发送数据 = {}

	for n = 1, 20 do
		if 摊位数据[玩家编号].道具[n] ~= nil then
			self.包裹id = 摊位数据[玩家编号].道具[n].id

			if 玩家数据[玩家编号].角色.数据.道具.包裹[self.包裹id] ~= nil then
				self.道具id = 玩家数据[玩家编号].角色.数据.道具.包裹[self.包裹id]

				if 玩家数据[玩家编号].道具.数据[self.道具id] ~= nil and 玩家数据[玩家编号].道具.数据[self.道具id] ~= 0 then
					self.发送数据[n] = {
						道具 = 玩家数据[玩家编号].道具.数据[self.道具id],
						单价 = 摊位数据[玩家编号].道具[n].价格
					}

					-- if  then
					-- end
				end
			end
		end
	end

	return self.发送数据
end

function 摆摊处理类:发送摊位信息(id)
	if 地图处理类:摊位重复(id, 玩家数据[id].地图) then
		发送数据(玩家数据[id].连接id, 7, "#y/这里已经有人在摆摊了，请换个地方吧")

		return 0
	elseif 玩家数据[id].地图 ~= 1001 and 玩家数据[id].地图 ~= 1070 and 玩家数据[id].地图 ~= 1092 and 玩家数据[id].地图 ~= 1501 then
		发送数据(玩家数据[id].连接id, 7, "#y/只有长安城、长寿村、傲来国、建邺城才可以摆摊")

		return 0
	elseif 玩家数据[id].队伍 ~= 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/组队状态下无法摆摊")

		return 0
	end

	if 摊位数据[id] == nil then
		摊位数据[id] = {
			状态 = true,
			记录 = "",
			名称 = "杂货摊位",
			次数 = 0,
			道具 = {},
			bb = {},
			刷新 = os.time()
		}
	end

	摊位数据[id].状态 = true

	发送数据(玩家数据[id].连接id, 20001, 摊位数据[id])
	发送数据(玩家数据[id].连接id, 20002, 玩家数据[id].道具:索要道具1(id, "包裹"))
	发送数据(玩家数据[id].连接id, 20008, 摊位数据[id].名称)
	地图处理类:更新摊位(id, 摊位数据[id].名称, 玩家数据[id].地图)

	玩家数据[id].摆摊 = id
end

function 摆摊处理类:显示(x, y)
end

return 摆摊处理类
